SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `I_Wish_DB` ;
CREATE SCHEMA IF NOT EXISTS `I_Wish_DB` DEFAULT CHARACTER SET latin1 ;
USE `I_Wish_DB` ;

-- -----------------------------------------------------
-- Table `I_Wish_DB`.`Category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `I_Wish_DB`.`Category` ;

CREATE TABLE IF NOT EXISTS `I_Wish_DB`.`Category` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `image` LONGBLOB NULL DEFAULT NULL,
  `img` LONGBLOB NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `I_Wish_DB`.`Company`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `I_Wish_DB`.`Company` ;

CREATE TABLE IF NOT EXISTS `I_Wish_DB`.`Company` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `image` LONGBLOB NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 34
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `I_Wish_DB`.`Place`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `I_Wish_DB`.`Place` ;

CREATE TABLE IF NOT EXISTS `I_Wish_DB`.`Place` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `description` VARCHAR(500) NULL DEFAULT NULL,
  `address` VARCHAR(50) NOT NULL,
  `tel_no` VARCHAR(50) NULL DEFAULT NULL,
  `webSite` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `I_Wish_DB`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `I_Wish_DB`.`User` ;

CREATE TABLE IF NOT EXISTS `I_Wish_DB`.`User` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `firstName` VARCHAR(50) NULL DEFAULT NULL,
  `lastName` VARCHAR(50) NULL DEFAULT NULL,
  `password` VARCHAR(500) NOT NULL,
  `birthday` DATETIME NULL DEFAULT NULL,
  `facebookUserId` VARCHAR(500) NULL DEFAULT NULL,
  `facebookUserAccessToken` VARCHAR(500) NULL DEFAULT NULL,
  `picture` LONGBLOB NULL DEFAULT NULL,
  `registerationId` VARCHAR(1000) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 68
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `I_Wish_DB`.`Event_Type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `I_Wish_DB`.`Event_Type` ;

CREATE TABLE IF NOT EXISTS `I_Wish_DB`.`Event_Type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `I_Wish_DB`.`User_Event`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `I_Wish_DB`.`User_Event` ;

CREATE TABLE IF NOT EXISTS `I_Wish_DB`.`User_Event` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `eventType_id` INT(11) NOT NULL,
  `User_id` INT(11) NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `description` VARCHAR(500) NULL DEFAULT NULL,
  `place_id` INT(11) NULL DEFAULT NULL,
  `date` DATE NULL DEFAULT NULL,
  `time` TIME NULL DEFAULT NULL,
  `img` LONGBLOB NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `User-event_FK1_idx` (`User_id` ASC),
  INDEX `User-event_FK2_idx` (`eventType_id` ASC),
  INDEX `fk_place_fk_idx` (`place_id` ASC),
  CONSTRAINT `fk_place_fk`
    FOREIGN KEY (`place_id`)
    REFERENCES `I_Wish_DB`.`Place` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `User-event_FK1`
    FOREIGN KEY (`User_id`)
    REFERENCES `I_Wish_DB`.`User` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `User-event_FK2`
    FOREIGN KEY (`eventType_id`)
    REFERENCES `I_Wish_DB`.`Event_Type` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 105
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `I_Wish_DB`.`Event_Comments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `I_Wish_DB`.`Event_Comments` ;

CREATE TABLE IF NOT EXISTS `I_Wish_DB`.`Event_Comments` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `content` VARCHAR(500) NULL DEFAULT NULL,
  `isReply` INT(11) NULL DEFAULT '0',
  `event_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `comment_FK1_idx` (`event_id` ASC),
  INDEX `fk_user_id_evetn_comment_idx` (`user_id` ASC),
  CONSTRAINT `comment_FK1`
    FOREIGN KEY (`event_id`)
    REFERENCES `I_Wish_DB`.`User_Event` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK6B261A39C65524FB`
    FOREIGN KEY (`event_id`)
    REFERENCES `I_Wish_DB`.`User_Event` (`id`),
  CONSTRAINT `fk_user_id_evetn_comment`
    FOREIGN KEY (`user_id`)
    REFERENCES `I_Wish_DB`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 30
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `I_Wish_DB`.`Event_Place`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `I_Wish_DB`.`Event_Place` ;

CREATE TABLE IF NOT EXISTS `I_Wish_DB`.`Event_Place` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `Place_id` INT(11) NOT NULL,
  `event_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `event-Place_FK1_idx` (`event_id` ASC),
  INDEX `event-Place_FK2` (`Place_id` ASC),
  CONSTRAINT `event-Place_FK1`
    FOREIGN KEY (`event_id`)
    REFERENCES `I_Wish_DB`.`Event_Type` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `event-Place_FK2`
    FOREIGN KEY (`Place_id`)
    REFERENCES `I_Wish_DB`.`Place` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `I_Wish_DB`.`Friend_List`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `I_Wish_DB`.`Friend_List` ;

CREATE TABLE IF NOT EXISTS `I_Wish_DB`.`Friend_List` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `User_id` INT(11) NOT NULL,
  `friend_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `unique_User_friend_constraint` (`User_id` ASC, `friend_id` ASC),
  INDEX `friend_FK2` (`friend_id` ASC),
  CONSTRAINT `friend_FK1`
    FOREIGN KEY (`User_id`)
    REFERENCES `I_Wish_DB`.`User` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `friend_FK2`
    FOREIGN KEY (`friend_id`)
    REFERENCES `I_Wish_DB`.`User` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 160
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `I_Wish_DB`.`Greeting`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `I_Wish_DB`.`Greeting` ;

CREATE TABLE IF NOT EXISTS `I_Wish_DB`.`Greeting` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `event_id` INT(11) NOT NULL,
  `description` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `Greeting_FK1_idx` (`event_id` ASC),
  CONSTRAINT `FK1018E459B5658060`
    FOREIGN KEY (`event_id`)
    REFERENCES `I_Wish_DB`.`Event_Type` (`id`),
  CONSTRAINT `Greeting_FK1`
    FOREIGN KEY (`event_id`)
    REFERENCES `I_Wish_DB`.`Event_Type` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `I_Wish_DB`.`IWish_Config`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `I_Wish_DB`.`IWish_Config` ;

CREATE TABLE IF NOT EXISTS `I_Wish_DB`.`IWish_Config` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `config_key` VARCHAR(100) NOT NULL,
  `config_value` VARCHAR(250) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `I_Wish_DB`.`Notification`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `I_Wish_DB`.`Notification` ;

CREATE TABLE IF NOT EXISTS `I_Wish_DB`.`Notification` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `is_deliverd` BIT(1) NULL DEFAULT NULL,
  `detail` VARCHAR(500) NULL DEFAULT NULL,
  `picture` LONGBLOB NULL DEFAULT NULL,
  `receive_time` DATETIME NULL DEFAULT NULL,
  `title` VARCHAR(50) NULL DEFAULT NULL,
  `user_id` INT(11) NULL DEFAULT NULL,
  `userEvent_id` INT(11) NULL DEFAULT NULL,
  `url` VARCHAR(500) NULL DEFAULT NULL,
  `eventComment_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK2D45DD0BA50CCBCE` (`user_id` ASC),
  INDEX `FK_ra5ayj04a2s6meciq3b7jpfvw` (`userEvent_id` ASC),
  INDEX `FK_eventCommentNotification_idx` (`eventComment_id` ASC),
  CONSTRAINT `FK2D45DD0BA50CCBCE`
    FOREIGN KEY (`user_id`)
    REFERENCES `I_Wish_DB`.`User` (`id`),
  CONSTRAINT `FK_q9cgls04cdymagxkstkcmq7qb`
    FOREIGN KEY (`user_id`)
    REFERENCES `I_Wish_DB`.`User` (`id`),
  CONSTRAINT `FK_ra5ayj04a2s6meciq3b7jpfvw`
    FOREIGN KEY (`userEvent_id`)
    REFERENCES `I_Wish_DB`.`User_Event` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 211
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `I_Wish_DB`.`Store_Owner`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `I_Wish_DB`.`Store_Owner` ;

CREATE TABLE IF NOT EXISTS `I_Wish_DB`.`Store_Owner` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `firstName` VARCHAR(50) NULL DEFAULT NULL,
  `lastName` VARCHAR(50) NULL DEFAULT NULL,
  `password` VARCHAR(100) NOT NULL,
  `birthday` DATE NOT NULL,
  `picture` LONGBLOB NULL DEFAULT NULL,
  `is_admin` TINYINT(1) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `I_Wish_DB`.`Product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `I_Wish_DB`.`Product` ;

CREATE TABLE IF NOT EXISTS `I_Wish_DB`.`Product` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(5000) NOT NULL,
  `image` LONGBLOB NULL DEFAULT NULL,
  `price` INT(11) NOT NULL,
  `description` MEDIUMTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `Company_id` INT(11) NULL DEFAULT NULL,
  `Category_id` INT(11) NOT NULL,
  `Store_Owner_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `Product_FK_idx` (`Category_id` ASC),
  INDEX `Product_FK2_idx` (`Company_id` ASC),
  INDEX `fk_Product_Store_Owner1_idx` (`Store_Owner_id` ASC),
  CONSTRAINT `fk_Product_Store_Owner1`
    FOREIGN KEY (`Store_Owner_id`)
    REFERENCES `I_Wish_DB`.`Store_Owner` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Product_FK1`
    FOREIGN KEY (`Category_id`)
    REFERENCES `I_Wish_DB`.`Category` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `Product_FK2`
    FOREIGN KEY (`Company_id`)
    REFERENCES `I_Wish_DB`.`Company` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 1468
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `I_Wish_DB`.`User_Product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `I_Wish_DB`.`User_Product` ;

CREATE TABLE IF NOT EXISTS `I_Wish_DB`.`User_Product` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `User_id` INT(11) NOT NULL,
  `Product_id` INT(11) NOT NULL,
  `received` INT(11) NULL DEFAULT '0',
  `deleted` INT(11) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `FKB581507BA50CCBCE` (`User_id` ASC),
  INDEX `FKB581507BF82CF0E6` (`Product_id` ASC),
  CONSTRAINT `FKB581507BA50CCBCE`
    FOREIGN KEY (`User_id`)
    REFERENCES `I_Wish_DB`.`User` (`id`),
  CONSTRAINT `FKB581507BF82CF0E6`
    FOREIGN KEY (`Product_id`)
    REFERENCES `I_Wish_DB`.`Product` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 13
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `I_Wish_DB`.`Product_Reserve`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `I_Wish_DB`.`Product_Reserve` ;

CREATE TABLE IF NOT EXISTS `I_Wish_DB`.`Product_Reserve` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `comment` VARCHAR(50) NOT NULL,
  `participate` DOUBLE NULL DEFAULT NULL,
  `friend_id` INT(11) NOT NULL,
  `user_product_id` INT(11) NOT NULL,
  `deleted` TINYINT(1) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `reserve_FK3_idx` (`friend_id` ASC),
  INDEX `user_product_FK_idx` (`user_product_id` ASC),
  CONSTRAINT `reserve_FK3`
    FOREIGN KEY (`friend_id`)
    REFERENCES `I_Wish_DB`.`User` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `user_product_FK`
    FOREIGN KEY (`user_product_id`)
    REFERENCES `I_Wish_DB`.`User_Product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
