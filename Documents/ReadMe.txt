/************************************************************/
/************************************************************/
/************************************************************/
/******************                              ************/
/******************      IWish Enterprise        ************/
/******************      IWish  Co.  2015        ************/
/******************                              ************/
/************************************************************/
/************************************************************/
/************************************************************/

/Documentation				// IWish Documentation
/ERD - Class Diagram		// ERD & Class Diagram
/Lib 						// Requied libs for web & mobile
/Presentation				// IWish Presentation
/Server 					// WildFlay Server
/Source Code 				// IWish Web & Mobile Project Source Code



HOW-TO Setup Enviroment 

Tools : 
		- IDE
				Netbeans 8.2
				Intellij 11.0
		- Server
				Glassfish 4.0
				WildFly 8.1

Server Configuration
// wildfly-8.2.0.Final.zip - file contains server with configured datasource


Nead some configuration to switch between Glassfish and WildFly 8.1
in : 
	- Datasource 
				persistence.xml // change datasource name and server JTA platform
				eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil.java // change ServiceLocator Lookup for desired server


