package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.NotificationDTO;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserProductDTO;

import java.util.ArrayList;
import java.util.List;


public interface UserDelegate {
	
    public List<UserProductDTO> getUserProductsDTOsList(UserDTO user);
    
	public List<ProductDTO> getUserProductsList(String  email) ;

	public ArrayList<UserDTO> getFriends(UserDTO user);

	public List<NotificationDTO> getAllNotification(UserDTO userDTO);
	
	public List<NotificationDTO> getUnReadedUserNotification(UserDTO userDTO);        

    public UserDTO login(String email, String password);
        
    public void saveRegId(UserDTO userDTO , String regId);
        
    public UserDTO findById (int id);

	void updateUserProfile(UserDTO user);
	
	/**
	 * @author Hossam ElDeen
	 * 
	 * @param facebookUserId
	 * @return
	 */
	public UserDTO findUserByFacebookId(String facebookUserId);
}
