package eg.gov.iti.jets.iwish.pojo;

// Generated May 8, 2015 10:25:02 PM by Hibernate Tools 4.0.0

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * EventComments generated by hbm2java
 */
@Entity
@Table(name = "Event_Comments", catalog = "I_Wish_DB")
@XmlRootElement
public class EventComments  extends AbstractPojo implements java.io.Serializable {

	private Integer id;
        private User user;
	private UserEvent userEvent;
	private String content;
	private Integer isReply;

	public EventComments() {
	}

	public EventComments(UserEvent userEvent) {
		this.userEvent = userEvent;
	}

	public EventComments(UserEvent userEvent, String content, Integer isReply,User user) {
		this.userEvent = userEvent;
		this.content = content;
		this.isReply = isReply;
                this.user= user;
	}
        
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
        
        @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        
        
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "event_id", nullable = false)
	public UserEvent getUserEvent() {
		return this.userEvent;
	}

	public void setUserEvent(UserEvent userEvent) {
		this.userEvent = userEvent;
	}

	@Column(name = "content", length = 300)
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "isReply")
	public Integer getIsReply() {
		return this.isReply;
	}

	public void setIsReply(Integer isReply) {
		this.isReply = isReply;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		EventComments that = (EventComments) o;

		if (id != null ? !id.equals(that.id) : that.id != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}
}
