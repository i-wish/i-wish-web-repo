package eg.gov.iti.jets.iwish.interceptor;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggerInterceptor {

    @AroundInvoke
    public Object log(InvocationContext invocationContext) throws Exception {

        Logger logger = Logger.getLogger("myLogger");
        logger.log(Level.INFO, "Class Name : " + invocationContext.getMethod().getDeclaringClass().getName());
        logger.log(Level.INFO, "Method Name : " + invocationContext.getMethod().getName());
        logger.log(Level.INFO, "Start at : " + new Date());
        Object[] parameters = invocationContext.getParameters();
        if(parameters != null)
            for (int i = 0; i < parameters.length; i++) {
                logger.log(Level.INFO, parameters[i] != null ? parameters[i].toString(): "null");
            }
        Object result = invocationContext.proceed();

        logger.log(Level.INFO, "return : " + invocationContext.getMethod().getReturnType());
        logger.log(Level.INFO, "End at : " + new Date());
        return result;
    }

}
