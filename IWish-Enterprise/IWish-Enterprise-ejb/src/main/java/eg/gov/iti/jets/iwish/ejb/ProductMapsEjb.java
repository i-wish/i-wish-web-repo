package eg.gov.iti.jets.iwish.ejb;

import eg.gov.iti.jets.iwish.dao.CategoryDAO;
import eg.gov.iti.jets.iwish.dao.CompanyDAO;
import eg.gov.iti.jets.iwish.dao.UserEventDAO;
import eg.gov.iti.jets.iwish.delegate.ProductDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.CategoryDTO;
import eg.gov.iti.jets.iwish.dto.CompanyDTO;
import eg.gov.iti.jets.iwish.dto.ProductDTO;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by ZamZam on 4/28/2015.
 */

@Startup
@Singleton
@Lock(LockType.READ)
public class ProductMapsEjb {

    Map<CategoryDTO,Map<CompanyDTO, ArrayList<ProductDTO>>> cateCompProdMap = new HashMap<>();

//    Map<CategoryDTO, ArrayList<CompanyDTO>> categoryCompanyMap = new HashMap<>();
//    Map<CompanyDTO, ArrayList<ProductDTO>> companyProductMap = new HashMap<>();

    List<CategoryDTO> categories=new ArrayList<>();
    
    ProductDelegate productDelegate;

    CategoryDTO currentSelectedCategory = new CategoryDTO();

    @EJB
    CompanyDAO companyDAO ;

    @EJB
    CategoryDAO categoryDAO;
    
    @EJB
    SendGreetingEjb sendGreetingEjb;
    
    @EJB
    UserEventDAO userEventDAO;

    public ProductMapsEjb() {
        
    }
    
    @PostConstruct
    public void postConstract() {
    	productDelegate = DelegateFactory.getProductDelegateInstance();
        fillAllProducts();
    }

    public void setCateCompProdMap(Map<CategoryDTO, Map<CompanyDTO, ArrayList<ProductDTO>>> cateCompProdMap) {
        this.cateCompProdMap = cateCompProdMap;
    }

    public Map<CategoryDTO, Map<CompanyDTO, ArrayList<ProductDTO>>> getCateCompProdMap() {
        return cateCompProdMap;
    }

    public void fillAllProducts(){
        cateCompProdMap.clear();
        categories.clear();
        List<ProductDTO> allProducts = productDelegate.getAllProducts();

        for (int i = 0; i < allProducts.size(); i++) {
            //category already exist
            if (cateCompProdMap.containsKey(allProducts.get(i).getCategoryDTO())) {
                //company already exist
                if(cateCompProdMap.get(allProducts.get(i).getCategoryDTO()).containsKey(allProducts.get(i).getCompanyDTO())){
                    cateCompProdMap.get(allProducts.get(i).getCategoryDTO()).get(allProducts.get(i).getCompanyDTO()).add(allProducts.get(i));
                }
                //company does not exist
                else{
                    //create hashmap which works as value of this category with curren company as key and current product as value
                    HashMap<CompanyDTO,ArrayList<ProductDTO>> secondMap = new HashMap<>();
                    //create list to be the value of the key
                    ArrayList<ProductDTO> products = new ArrayList<>();
                    products.add(allProducts.get(i));
                    secondMap.put(allProducts.get(i).getCompanyDTO(), products);
                    //put the object we made in the big map
                    cateCompProdMap.get(allProducts.get(i).getCategoryDTO()).put(allProducts.get(i).getCompanyDTO(),products);
                }
            }
            //category does not exist, it is new
            else {
                //add it to categories
                categories.add(allProducts.get(i).getCategoryDTO());
                //create hashmap which works as value of this category with curren company as key and current product as value
                HashMap<CompanyDTO,ArrayList<ProductDTO>> secondMap = new HashMap<>();
                //create list to be the value of the key
                ArrayList<ProductDTO> products = new ArrayList<>();
                products.add(allProducts.get(i));
                secondMap.put(allProducts.get(i).getCompanyDTO(), products);
                //put the object we made in the big map
                cateCompProdMap.put(allProducts.get(i).getCategoryDTO(),secondMap);
            }
        }

    }

    public List<CategoryDTO> getCategories(){
        return categories;
    }

    public List<CategoryDTO> getAllCategories(){
        List<CategoryDTO> cats = categoryDAO.getAll();
        return cats;
    }

    public List<CompanyDTO> getCompanies(){
        return getAllCompanies();
//        for(int i=0; i< cateCompProdMap.size(); i++){
//            Set<CompanyDTO> allCompaniesSet = new HashSet<>();
//            allCompaniesSet.addAll(cateCompProdMap.);
//        }
//        Set<CompanyDTO> allCompaniesSet = companyProductMap.keySet();
//        ArrayList<CompanyDTO> allCompaniesList = new ArrayList<>();
//        for(CompanyDTO companyDTO: allCompaniesSet){
//            allCompaniesList.add(companyDTO);
//        }
//        return allCompaniesList;
    }

    public List<CompanyDTO> getAllCompanies(){
        List<CompanyDTO> comps = companyDAO.getAll();
        return comps;
    }

    public List<CompanyDTO> getCompanies(CategoryDTO category){
        currentSelectedCategory = category;
        List<CompanyDTO> companiess = new ArrayList<>();
        if(currentSelectedCategory != null) {
            if(!cateCompProdMap.isEmpty()){
                Map<CompanyDTO, ArrayList<ProductDTO>> companymap = cateCompProdMap.get(category);
                if (companymap != null)
                    companiess.addAll(companymap.keySet());

            return companiess;
            }
        }
        return new ArrayList<>();

    }

    public List<ProductDTO> getProducts(CompanyDTO company){
        if(currentSelectedCategory != null && cateCompProdMap !=null  ) {
            Map<CompanyDTO, ArrayList<ProductDTO>> cate = cateCompProdMap.get(currentSelectedCategory);
            if(cate != null) {
                return cate.get(company);
            }
        }

        return new ArrayList<ProductDTO>();
    }


    public int getAllProductsCount(CompanyDTO company, CategoryDTO categoryDTO){
        if(cateCompProdMap !=null  ) {

            Map<CompanyDTO, ArrayList<ProductDTO>> cate = cateCompProdMap.get(categoryDTO);
            if(cate != null) {

                List<ProductDTO> products = cate.get(company);
                if(products != null) {
                    return products.size();
                }
            }
        }

        return 0;
    }

}
