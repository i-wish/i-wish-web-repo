package eg.gov.iti.jets.iwish.dao;

import java.util.ArrayList;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.hibernate.Query;

// Generated Mar 31, 2015 5:37:08 PM by Hibernate Tools 4.0.0

import eg.gov.iti.jets.iwish.dto.EventCommentsDTO;
import eg.gov.iti.jets.iwish.pojo.EventComments;
import eg.gov.iti.jets.iwish.pojo.UserEvent;
import eg.gov.iti.jets.iwish.util.PojoUtil;

/**
 * Home object for domain model class EventComments.
 * 
 * @see eg.gov.iti.jets.iwish.pojo.EventComments
 * @author Hibernate Tools
 */
@Stateless
@LocalBean
public class EventCommentsDAO extends AbstractDAO<EventComments, EventCommentsDTO> {
	private static final Logger log = Logger.getLogger(EventCommentsDAO.class.getName());

	public EventCommentsDAO() {
		super(EventComments.class);
	}

	@Override
	public EventComments getEntity(EventComments t, EventCommentsDTO e) {
		t.setIsReply(e.getIsReply());
		t.setUser(PojoUtil.getPojo(e.getUser()));
		t.setUserEvent(PojoUtil.getPojo(e.getUserEventDTO()));
		t.setContent(e.getContent());
		t.setId(e.getId());
		return t;
	}
	
	//FIXME
	//converting list to dto
	public ArrayList<EventCommentsDTO> getCommentsOnEvent(UserEvent userEvent) {
		String hql = "From EventComments s where s.userEvent = :userEventt";
		Query query = getHibernateSession().createQuery(hql);
		ArrayList<EventComments> eventCommentses = (ArrayList<EventComments>) query.setParameter("userEventt", userEvent).list();
		
		
		ArrayList<EventCommentsDTO> eventCommentsDTOs = new ArrayList<>();
		for (EventComments eventComment : eventCommentses) {
			eventCommentsDTOs.add(PojoUtil.getDTO(eventComment));
		}
		return eventCommentsDTOs;
	}

}
