package eg.gov.iti.jets.iwish.ejb;

import eg.gov.iti.jets.iwish.dao.GreetingDAO;
import eg.gov.iti.jets.iwish.dao.UserEventDAO;
import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.GreetingDTO;
import eg.gov.iti.jets.iwish.interceptor.LoggerInterceptor;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.ArrayList;

/**
 * Created by Hossam Abdallah on 5/15/2015.
 */
@Stateless(mappedName="SuggestGreetingEjb")
@LocalBean
@Interceptors(LoggerInterceptor.class)
public class SuggestGreetingEjb {

    @EJB
    UserEventDAO userEventDAO;

    @EJB
    GreetingDAO greetingDAO;

    @EJB
    UserEjb userEjb;

    public ArrayList<GreetingDTO> getGreetings(EventTypeDTO eventTypeDTO) {
        return greetingDAO.getGreetingOfType(eventTypeDTO);
    }
    
}
