/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.ejb;

import eg.gov.iti.jets.iwish.dao.EventPlaceDAO;
import eg.gov.iti.jets.iwish.dao.EventTypeDAO;
import eg.gov.iti.jets.iwish.dto.EventPlaceDTO;
import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.PlaceDTO;
import eg.gov.iti.jets.iwish.interceptor.LoggerInterceptor;
import eg.gov.iti.jets.iwish.pojo.EventPlace;
import eg.gov.iti.jets.iwish.pojo.EventType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Radwa Manssour
 */
@Stateless(mappedName = "placeEjb")
@LocalBean
@Interceptors(LoggerInterceptor.class)
public class PlaceEjb {
    
    @Inject
    EventPlaceDAO eventPlaceDAO;
    
    @Inject
    EventTypeDAO eventTypeDAO;
    
   public List<PlaceDTO> getPlaces(EventTypeDTO eventType){
       
       EventType eventTypePojo=eventTypeDAO.findEntityById(eventType.getId());
       EventPlace eventPlace=new EventPlace();
       eventPlace.setEventType(eventTypePojo);
      
       List<EventPlaceDTO> eventPlaces=eventPlaceDAO.findByExample(eventPlace);
       List<PlaceDTO> placesDTO=new ArrayList<PlaceDTO>();
       for(EventPlaceDTO p:eventPlaces){
           placesDTO.add(p.getPlaceDTO());
       }
       
       return placesDTO;       
       
   }
    
    
}
