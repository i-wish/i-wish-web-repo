package eg.gov.iti.jets.iwish.observer;

import eg.gov.iti.jets.iwish.notification.NotificationWebService;

/**
 * Created by eltntawy on 11/06/15.
 */
public class NotificationObserver {


    private static NotificationWebService notificationWebService;


    public static NotificationWebService getNotificationWebService() {
        return notificationWebService;
    }

    private static void setNotificationWebService(NotificationWebService notificationWebService) {
        NotificationObserver.notificationWebService = notificationWebService;
    }

    public static void registerNotificationWebService(NotificationWebService notificationWebService) {
        if(NotificationObserver.getNotificationWebService() == null){
            setNotificationWebService(notificationWebService);
        }
    }

}
