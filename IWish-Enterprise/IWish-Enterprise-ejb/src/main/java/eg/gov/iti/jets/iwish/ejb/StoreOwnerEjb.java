package eg.gov.iti.jets.iwish.ejb;

import eg.gov.iti.jets.iwish.dao.ProductDAO;
import eg.gov.iti.jets.iwish.dao.StoreOwnerDAO;
import eg.gov.iti.jets.iwish.dto.CategoryDTO;
import eg.gov.iti.jets.iwish.dto.CompanyDTO;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.StoreOwnerDTO;
import eg.gov.iti.jets.iwish.interceptor.LoggerInterceptor;
import eg.gov.iti.jets.iwish.pojo.Product;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.List;

@Stateless(mappedName="storeOwnerEjb")
@LocalBean
@Interceptors(LoggerInterceptor.class) 
public class StoreOwnerEjb {

	@EJB
	StoreOwnerDAO storeOwnerDAO ;
	
	@EJB
	ProductDAO productDAO ;

	@EJB
	ProductMapsEjb productMapsEjb;

	public StoreOwnerDTO findStoreOwner(String email,String password) {
		
		StoreOwnerDTO storeOwnerDTO = storeOwnerDAO.findStoreOwnerByUsernameAndPassword(email,password);
		return storeOwnerDTO;
//		return null;
	}
	
	

	


	public void updateUser(StoreOwnerDTO user) {
		storeOwnerDAO.saveOrUpdate(user);
	}
}
