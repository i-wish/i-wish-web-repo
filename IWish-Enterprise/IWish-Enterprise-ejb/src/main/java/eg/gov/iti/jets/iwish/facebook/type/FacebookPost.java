/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.facebook.type;

/**
 *
 * @author eltntawy
 */
public class FacebookPost {
    
    public static enum FacebookPostType {
    LINK("link"),PICUTRE("picture"),MESSAGE("message"),DESCRIPTION("description");
        String value;
        FacebookPostType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
        
        @Override
        public String toString() {
            return this.value;
        }
    
    }
    
    private byte [] picture;
    private String link;
    private String description;
    private String message;

    public byte[] getPicture() {
        return picture;
        
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
    
    
    
}
