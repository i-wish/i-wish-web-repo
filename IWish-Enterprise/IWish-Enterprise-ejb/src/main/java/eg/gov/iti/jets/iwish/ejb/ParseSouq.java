package eg.gov.iti.jets.iwish.ejb;

import eg.gov.iti.jets.iwish.dao.CategoryDAO;
import eg.gov.iti.jets.iwish.dao.CompanyDAO;
import eg.gov.iti.jets.iwish.dao.ProductDAO;
import eg.gov.iti.jets.iwish.dao.StoreOwnerDAO;
import eg.gov.iti.jets.iwish.dto.StoreOwnerDTO;
import eg.gov.iti.jets.iwish.pojo.Category;
import eg.gov.iti.jets.iwish.pojo.Company;
import eg.gov.iti.jets.iwish.pojo.Product;
import eg.gov.iti.jets.iwish.pojo.StoreOwner;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
public class ParseSouq {

    @Inject
    CategoryDAO categoryDao;
    @Inject
    CompanyDAO companyDAO;
    @Inject
    ProductDAO productDAO;

    @Inject
    StoreOwnerDAO storeOwnerDAO;

    @Resource
    UserTransaction trx;

    public byte[] saveImage(String imageUrl) throws IOException {
        URL url = new URL(imageUrl);
        String fileName = url.getFile();
        String destName = fileName.substring(fileName.lastIndexOf("/"));
        // System.out.println(destName);

        InputStream is = url.openStream();
        // OutputStream os = new FileOutputStream("G:\\"+destName);

        BufferedImage originalImage = ImageIO.read(url);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(originalImage, "jpg", baos);
        byte[] imageInByte = baos.toByteArray();

        return imageInByte;
    }

    public HashMap<Category, String> getCategories() throws SystemException {

        Document doc;
        HashMap<Category, String> categories = new HashMap<Category, String>();
        try {
            doc = Jsoup.connect("http://egypt.souq.com").get();
            Elements list = doc.getElementsByAttributeValue("class",
                    "grouped-list show-for-large-up");


            for (Element i : list) {

                Elements links = i.select("a[href");

                for (Element l : links) {
                    String categoryName = l.text().toLowerCase();
                    System.out.println(categoryName);

                    Category category = new Category(categoryName);

                    List<Category> categoryList = categoryDao.findEntityByExample(category);
                    if (categoryList.size() > 0) {
                        category = categoryList.get(0);
                    } else {


                        try {
                            trx.begin();


                            categoryDao.persist(category);
                            categoryDao.getHibernateSession().refresh(category);

                            trx.commit();

                        } catch (Exception e) {
                            e.printStackTrace();
                            trx.rollback();
                        }

                    }
                    categories.put(category, l.attr("abs:href"));

                }

            }
        } catch (IOException ex) {
            Logger.getLogger(ParseSouq.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("category !!");
        return categories;

    }

    public void getProducts() throws SystemException {

        HashMap<Category, String> categories = getCategories();

        StoreOwnerDTO storeOwnerDTO = new StoreOwnerDTO();
        storeOwnerDTO.setEmail("souq@souq.com");
        StoreOwner storeowner = storeOwnerDAO.findByEmail(storeOwnerDTO);
        if (storeowner == null) {
            storeowner = new StoreOwner("Souq", "souq@souq.com", "", new Date());

            try {
                trx.begin();
                storeOwnerDAO.persist(storeowner);
                trx.commit();

            } catch (Exception e) {
                e.printStackTrace();
                trx.rollback();
            }

        }


        for (Map.Entry<Category, String> entry : categories.entrySet()) {

            int c = 0;

            try {


                Category category = entry.getKey();

                String url = entry.getValue();

                Document doc;



                    doc = Jsoup.connect(url).get();
                    Elements list = doc.getElementsByAttributeValue("class",
                            "mini-placard");
                    for (Element i : list) {
                        Elements links = i.select("a[href]");
                        for (Element l : links) {

                            Document productDoc = null;
                            try {
                                productDoc = Jsoup.connect(l.attr("abs:href")).get();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }

                            String productName = null;
                            Elements names = null;

                            if(productDoc != null) {
                                 names = productDoc.getElementsByAttributeValue("itemprop", "name");
                            }

                            if (names != null && names.size() > 0) {
                                productName = names.get(0).text();

                                Elements images = productDoc
                                        .getElementsByAttributeValue("class",
                                                "img-bucket");
                                byte[] arr = null;
                                if (images.size() > 0) {
                                    Elements imgUrls = images.get(0).select(
                                            "img[src]");
                                    final String imgUrl = imgUrls.attr("abs:src");

                                    arr = saveImage(imgUrl);

                                }
                                double price = 0;

                                Elements prices = productDoc
                                        .getElementsByAttributeValue("class",
                                                "price");
                                if (prices.size() > 0) {
                                    String price_str = prices.get(0).text();

                                    price_str = price_str
                                            .replaceAll("[a-zA-z]", "");
                                    price_str = price_str.replaceAll(",", "");
                                    price_str = price_str.substring(0,
                                            price_str.indexOf("."));

                                    price = Double.parseDouble(price_str);
                                }
                                Elements fullDesc = productDoc
                                        .getElementsByAttributeValue("id",
                                                "desc-full");

                                String desc = null;
                                if (fullDesc.size() > 0) {
                                    desc = fullDesc.get(0).text();
                                    System.out.println(desc);
                                } else {
                                    Elements shortDesc = productDoc
                                            .getElementsByAttributeValue("id",
                                                    "desc-short");
                                    if (shortDesc.size() > 0) {
                                        desc = shortDesc.get(0).text();

                                    }
                                }


                                String companyName = null;
                                Company company = null;
                                Elements companies = productDoc
                                        .getElementsByAttributeValue("class",
                                                "byline");
                                if (companies.size() > 0) {
                                    companyName = companies.get(0).text();
                                    companyName = companyName.substring(4);
                                    companyName = companyName.substring(0,
                                            companyName.indexOf(','));
                                    if (companyName.equalsIgnoreCase("other") || companyName.equalsIgnoreCase("����") || companyName.equalsIgnoreCase("����")) {
                                        companyName = "other";
                                    }

                                    company = new Company(companyName.toLowerCase());

                                    trx.begin();
                                    List<Company> Companies = companyDAO.findEntityByExample(company);
                                    if (Companies.size() > 0) {
                                        company = Companies.get(0);
                                    } else {

                                        companyDAO.persist(company);
                                        System.out.println(company.getId() + "second one");

                                    }
                                    trx.commit();

                                    Product product = new Product();
                                    product.setName(productName);
                                    product.setStoreOwner(storeowner);
                                    List<Product> products = productDAO.findEntityByExample(product);

                                    trx.begin();
                                    if (!products.isEmpty()) {
                                        product = products.get(0);
                                        product.setDescription(desc);
                                        product.setImage(arr);
                                        product.setPrice((int) price);
                                        productDAO.saveOrUpdate(product);
                                    } else {
                                        product = new Product(null, company, category,
                                                productName, arr, new Integer((int) price), desc, null);

                                        product.setStoreOwner(storeowner);
                                        productDAO.persist(product);
                                    }
                                    trx.commit();
                                }
                                c++;

                                if (c > 20) {
                                    c = 0;
                                    break;
                                }
                            }

                        }

                    }


            } catch (Exception e) {
                e.printStackTrace();
                trx.rollback();
            }
        }
    }
}
