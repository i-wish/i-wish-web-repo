package eg.gov.iti.jets.iwish.dao;

import eg.gov.iti.jets.iwish.dto.NotificationDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.pojo.Notification;
import eg.gov.iti.jets.iwish.pojo.UserEvent;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import org.hibernate.Query;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;
import java.util.logging.Logger;

@Stateless
@LocalBean
public class NotificationDAO extends AbstractDAO<Notification, NotificationDTO> {
	private static final Logger log = Logger.getLogger(NotificationDAO.class.getName());
	
	public NotificationDAO() {
		super(Notification.class);
	}

	@Override
	public Notification getEntity(Notification t, NotificationDTO e) {
		t.setDelivered(e.isDelivered());
		t.setDetail(e.getDetail());
		t.setReceiveTime(e.getReceiveTime());
		t.setTitle(e.getTitle());
		t.setPicture(e.getPicture());
		t.setUrl(e.getUrl());
		t.setId(e.getId());
		t.setUser(PojoUtil.getPojo(e.getUserDTO()));
		t.setUserEvent(PojoUtil.getPojo(e.getUserEvent()));
		return t;
	}

	public List<NotificationDTO> getNotification(UserDTO userDTO, Boolean isDelivered) {
		String queryString = "Select new eg.gov.iti.jets.iwish.dto.NotificationDTO(notif) "
				+ " From Notification notif where notif.user.id= :userId ";
		if (isDelivered != null) {
			queryString += "and notif.delivered = :isDelivered";
		}
		Query query = getHibernateSession().createQuery(queryString);
		query.setParameter("userId", userDTO.getId());
		if (isDelivered != null) {
			query.setParameter("isDelivered", isDelivered);
		}
		List<NotificationDTO> ret = query.list();
		return ret;
	}

	public List<NotificationDTO> getAllNotification(UserDTO userDTO) {
		return getNotification(userDTO, null);
	}

	public List<NotificationDTO> getUnReadedNotification(UserDTO userDTO) {
		return getNotification(userDTO, false);
	}

	public List<NotificationDTO> getReadedNotification(UserDTO userDTO) {
		return getNotification(userDTO, true);
	}

	//FIXME
	//why to use persist instead of saveOrUpdate
	public void confirmReceivingNotification(NotificationDTO notificationDTO) {
		Notification notification = findEntityById(notificationDTO.getId());
		notification.setDelivered(true);
		this.persist(notification);
	}

	public List<Notification> getNotificationsOfEvent(UserEvent eventPojo) {
		Query query = getHibernateSession().createQuery("From Notification n where n.userEvent = :userEvent");
		query.setParameter("userEvent", eventPojo);
		List<Notification> notifications = query.list();
		return notifications;
	}
}
