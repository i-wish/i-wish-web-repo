package eg.gov.iti.jets.iwish.pojo;

// Generated May 8, 2015 10:25:02 PM by Hibernate Tools 4.0.0

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * UserEvent generated by hbm2java
 */
@Entity
@Table(name = "User_Event", catalog = "I_Wish_DB")
@XmlRootElement
public class UserEvent extends AbstractPojo implements java.io.Serializable {

	private Integer id;
	private User user;
	private EventType eventType;
	private String name;
	private String description;
	private Place place;
	private Date date;
	private Date time;
	private Set<EventComments> eventCommentses = new HashSet<EventComments>(0);
    private byte[] img;

	public UserEvent() {
	}

	public UserEvent(User user, EventType eventType, String name) {
		this.user = user;
		this.eventType = eventType;
		this.name = name;
	}

	public UserEvent(User user, EventType eventType, String name,
			String description, Place place, Date date, Date time,
			Set<EventComments> eventCommentses) {
		this.user = user;
		this.eventType = eventType;
		this.name = name;
		this.description = description;
		this.place = place;
		this.date = date;
		this.time = time;
		this.eventCommentses = eventCommentses;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "User_id", nullable = false)
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "eventType_id", nullable = false)
	public EventType getEventType() {
		return this.eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	@Column(name = "name", nullable = false, length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description", length = 300)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "place_id", nullable = false)
	public Place getPlace() {
		return this.place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date", length = 10)
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "time", length = 8)
	public Date getTime() {
		return this.time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "userEvent")
	public Set<EventComments> getEventCommentses() {
		return this.eventCommentses;
	}

	public void setEventCommentses(Set<EventComments> eventCommentses) {
		this.eventCommentses = eventCommentses;
	}
        
        @Column(name = "img",columnDefinition="longblob")
	public byte[] getPicture() {
		return this.img;
	}

	public void setPicture(byte[] img) {
		this.img =img;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		UserEvent userEvent = (UserEvent) o;

		if (id != null ? !id.equals(userEvent.id) : userEvent.id != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}
}
