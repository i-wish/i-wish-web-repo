/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.CategoryDTO;
import eg.gov.iti.jets.iwish.dto.CompanyDTO;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.StoreOwnerDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserProductDTO;
import eg.gov.iti.jets.iwish.ejb.ProductEjb;
import eg.gov.iti.jets.iwish.ejb.UserEjb;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil.ServiceLocatorType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Hossam Abdallah
 */

public class ProductDelegateImpl implements ProductDelegate {
	ProductEjb productEjb = (ProductEjb) ServiceLocatorUtil.getInistance().lookup(ServiceLocatorType.PRODUCT_SERVICE);

	@Override
	public ArrayList<ProductDTO> getAllProducts() {
		return productEjb.getProducts();
	}
    
	@Override
	public List<ProductDTO> getProductsByUser(UserDTO userDTO) {
		return productEjb.getUserProductsList(userDTO);
	}

	@Override
	public CategoryDTO getCategoryById(int id) {
		return productEjb.getCategoryById(id);
	}

	@Override
	public void updateProduct(ProductDTO product) {
		productEjb.updateProduct(product);
	}

	@Override
	public void synchProudct() {
		productEjb.synchProduct();
	}

	@Override
	public ProductDTO getProductImage(ProductDTO productDTO) {
		return productEjb.getProductImage(productDTO);
	}

	@Override
	public Long getProductsCount() {
		return productEjb.getProductCount();
	}

	@Override
	public Map<String, Integer> getCompanyProductsMap() {
		return productEjb.getCompanyProductsMap();
	}

	@Override
	public CompanyDTO getCompanyById(int id) {
		return productEjb.getCompanyById(id);
	}

	
	@Override
	/**
	 * @author Hossam ElDeen
	 * 
	 * @param productDTO
	 * @param companyDTO
	 * @param categoryDTO
	 */
	public void addProduct(ProductDTO productDTO, CompanyDTO companyDTO, CategoryDTO categoryDTO) {
		if(productDTO==null)
			throw  new IllegalArgumentException("productDTO can not be null");
		if(companyDTO == null)
			throw new IllegalArgumentException("companyDTO can not be null");
		if(categoryDTO == null)
			throw new IllegalArgumentException("categoryDTO can not be null");
			
        productEjb.addProduct(productDTO,companyDTO,categoryDTO);
    }
	
	@Override
	/**
	 * @author Hossam ElDeen
	 */
	public List<ProductDTO> getProducts(StoreOwnerDTO storeOwnerDTO) {
		if(storeOwnerDTO == null)
			throw new IllegalArgumentException("input parameter can not be null");
		return productEjb.getProducts(storeOwnerDTO);
	}
	
	@Override
	/**
	 * @author Hossam ElDeen
	 */
	public void deleteProduct(ProductDTO productDTO) {
		if(productDTO == null)
			throw new IllegalArgumentException("input parameter can not be null");
		productEjb.remove(productDTO);
	}
	
	@Override
	/**
	 * @author Hossam ElDeen
	 */
	public List<UserProductDTO> getUserProductsDTOsList(UserDTO user) {
		return productEjb.getUserProductsDTOsList(user);
	}
	
}
