package eg.gov.iti.jets.iwish.util;

/**
 * Created by eltntawy on 23/04/15.
 */
import eg.gov.iti.jets.iwish.dto.*;
import eg.gov.iti.jets.iwish.pojo.*;

import javax.naming.OperationNotSupportedException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//FIXME
//every dto have to have getPojo() and getPojo(List) and constructor(Pojo), and the same for every pojo
public class PojoUtil {

    public static <T extends AbstractPojo> AbstractDTO getDTO(T t) throws OperationNotSupportedException {
        AbstractDTO dto = null;
        if (t != null) {
            if (t instanceof Category) {
                dto = getDTO((Category) t);
            } else if (t instanceof Company) {
                dto = getDTO((Company) t);
            } else if (t instanceof EventComments) {
                dto = getDTO((EventComments) t);
            } else if (t instanceof EventPlace) {
                dto = getDTO((EventPlace) t);
            } else if (t instanceof EventType) {
                dto = getDTO((EventType) t);
            } else if (t instanceof FriendList) {
                dto = getDTO((FriendList) t);
            } else if (t instanceof Greeting) {
                dto = getDTO((Greeting) t);
            } else if (t instanceof Place) {
                dto = getDTO((Place) t);
            } else if (t instanceof Product) {
                dto = getDTO((Product) t);
            } else if (t instanceof ProductReserve) {
                dto = getDTO((ProductReserve) t);
            } else if (t instanceof User) {
                dto = getDTO((User) t);
            } else if (t instanceof UserEvent) {
                dto = getDTO((UserEvent) t);
            } else if (t instanceof UserProduct) {
                dto = getDTO((UserProduct) t);
            } else if (t instanceof StoreOwner) {
                dto = getDTO((StoreOwner) t);
            } else if(t instanceof IWishConfig){
                dto= getDTO((IWishConfig) t);
            } else if(t instanceof Notification) {
                dto = getDTO((Notification) t);
            }
        }

        return dto;
    }

    public static UserProductDTO getDTO(UserProduct userProduct) {
        UserProductDTO userProductDTO = null;
        if (userProduct != null) {
            userProductDTO = new UserProductDTO();
            userProductDTO.setId(userProduct.getId());
            userProductDTO.setProductDTO(getDTO(userProduct.getProduct()));
//            userProductDTO.setProductReserves(getDTOListt(userProduct.getProductReserves()));
            userProductDTO.setReceived(userProduct.getReceived());
            userProductDTO.setUserDTO(getDTO(userProduct.getUser()));
            userProductDTO.setDeleted(userProduct.isDeleted());
        }
        return userProductDTO;
    }

    public static ProductReserveDTO getDTO(ProductReserve productReserve) {
        ProductReserveDTO productReserveDTO = null;
        if (productReserve != null) {
            productReserveDTO = new ProductReserveDTO();
            productReserveDTO.setComment(productReserve.getComment());
            productReserveDTO.setId(productReserve.getId());
            productReserveDTO.setParticipate(productReserve.getParticipate());
            productReserveDTO.setUserDTO(getDTO(productReserve.getUser()));
            productReserveDTO.setUserProductDto(getDTO(productReserve.getUserProduct()));
            productReserveDTO.setDeleted(productReserve.isDeleted());
        }
        return productReserveDTO;
    }

    public static Set<ProductReserveDTO> getDTOListt(Set<ProductReserve> productReserveList) {
        if (productReserveList == null) {
            return null;
        }
        Set<ProductReserveDTO> productReserveDTO = new HashSet<>();
        for (ProductReserve productReserve : productReserveList) {
            productReserveDTO.add(getDTO(productReserve));
        }
        return productReserveDTO;
    }

    public static ArrayList<UserProductDTO> getDTOList(ArrayList<UserProduct> userProductsList) {
        if (userProductsList == null) {
            return null;
        }
        ArrayList<UserProductDTO> userProductsDTO = new ArrayList<>();
        for (UserProduct userProduct : userProductsList) {
            userProductsDTO.add(getDTO(userProduct));
        }
        return userProductsDTO;
    }

    public static List<UserEventDTO> getDTOListForUserEvent(List<UserEvent> userEvents) {
        ArrayList<UserEventDTO> userEventDTOs = null;

        if (userEvents != null) {
            userEventDTOs = new ArrayList<>();

            for (UserEvent userEvent : userEvents) {
                UserEventDTO userEventDTO = getDTO(userEvent);
                userEventDTOs.add(userEventDTO);
            }
        }

        return userEventDTOs;
    }

    public static EventCommentsDTO getDTO(EventComments eventCommentsPojo) {
        EventCommentsDTO dto = null;
        if (eventCommentsPojo != null) {
            dto = new EventCommentsDTO();

            dto.setContent(eventCommentsPojo.getContent());
            dto.setId(eventCommentsPojo.getId());
            dto.setIsReply(eventCommentsPojo.getIsReply());
//            UserEventDTO userEventDTO = getDTO(eventCommentsPojo.getUserEvent());
//            dto.setUserEventDTO(userEventDTO);
            dto.setUserEventDTO(null);

            dto.setUser(getDTO(eventCommentsPojo.getUser()));
        }

        return dto;
    }

    public static Set<EventCommentsDTO> getDTO(Set<EventComments> eventCommentsSet) {
        Set<EventCommentsDTO> eventCommentsDTOSet = null;
        if (eventCommentsSet != null) {
            eventCommentsDTOSet = new HashSet<EventCommentsDTO>();

            for (EventComments eventComments : eventCommentsSet) {
                EventCommentsDTO eventCommentsDTO = getDTO(eventComments);
                eventCommentsDTOSet.add(eventCommentsDTO);
            }
        }
        return eventCommentsDTOSet;
    }

    public static Set<EventComments> getDTOList(Set<EventCommentsDTO> eventCommentsSet) {
        Set<EventComments> eventCommentsDTOSet = null;
        if (eventCommentsSet != null) {
            eventCommentsDTOSet = new HashSet<EventComments>();

            for (EventCommentsDTO eventComments : eventCommentsSet) {
                EventComments eventCommentsDTO = getPojo(eventComments);
                eventCommentsDTOSet.add(eventCommentsDTO);
            }
        }
        return eventCommentsDTOSet;
    }

    public static EventPlaceDTO getDTO(EventPlace eventPlacePojo) {
        EventPlaceDTO dto = null;
        if (eventPlacePojo != null) {
            dto = new EventPlaceDTO();
            dto.setId(eventPlacePojo.getId());

            EventTypeDTO eventTypeDTO = getDTO(eventPlacePojo.getEventType());
            dto.setEventTypeDTO(eventTypeDTO);

            PlaceDTO placeDTO = getDTO(eventPlacePojo.getPlace());
            dto.setPlaceDTO(placeDTO);
        }
        return dto;
    }

    public static GreetingDTO getDTO(Greeting greetingPojo) {
        GreetingDTO greetingDto = null;
        if (greetingPojo != null) {
            greetingDto = new GreetingDTO();
            greetingDto.setId(greetingPojo.getId());
            greetingDto.setDescription(greetingPojo.getDescription());
            EventTypeDTO eventTypeDto = getDTO(greetingPojo.getEventType());
            greetingDto.setEventTypeDTO(eventTypeDto);
        }
        return greetingDto;
    }

    public static EventComments getPojo(EventCommentsDTO eventCommentsDto) {
        EventComments pojo = null;
        if (eventCommentsDto != null) {
            pojo = new EventComments();
            pojo.setContent(eventCommentsDto.getContent());
            pojo.setId(eventCommentsDto.getId());
            pojo.setIsReply(eventCommentsDto.getIsReply());

            UserEvent usrEventPojo = getPojo(eventCommentsDto.getUserEventDTO());
            pojo.setUserEvent(usrEventPojo);
            pojo.setUser(getPojo(eventCommentsDto.getUser()));
        }
        return pojo;
    }

    public static EventPlace getPojo(EventPlaceDTO eventPlaceDTO) {
        EventPlace pojo = null;
        if (eventPlaceDTO != null) {
            pojo.setId(eventPlaceDTO.getId());

            EventType eventTypePojo = getPojo(eventPlaceDTO.getEventTypeDTO());
            pojo.setEventType(null);

            Place placePojo = getPojo(eventPlaceDTO.getPlaceDTO());
            pojo.setPlace(placePojo);
        }
        return pojo;
    }

    public static Greeting getPojo(GreetingDTO greetingDTO) {
        Greeting greetingPojo = new Greeting();
        if (greetingDTO != null) {
            greetingPojo.setId(greetingDTO.getId());
            greetingPojo.setDescription(greetingDTO.getDescription());
            EventType eventTypePojo = getPojo(greetingDTO.getEventTypeDTO());
            greetingPojo.setEventType(eventTypePojo);
        }
        return greetingPojo;
    }

    public static List<ProductDTO> getDTOList(List<Product> products) {
        List<ProductDTO> productDTOs = new ArrayList<ProductDTO>();

        for (Product product : products) {
            productDTOs.add(PojoUtil.getDTO(product));
        }
        return productDTOs;
    }

    public static UserDTO getDTO(User user) {
        if (user == null) {
            return null;
        }
        UserDTO userDto = new UserDTO(user.getName(), user.getEmail(), user.getFirstName(),
                user.getLastName(), user.getPassword(), user.getBirthday(), null, null, null, null, null);

        userDto.setId(user.getId());
        userDto.setPicture(user.getPicture());

        userDto.setFacebookUserAccessToken(user.getFacebookUserAccessToken());
        userDto.setFacebookUserId(user.getFacebookUserId());
        userDto.setRegisterationId(user.getRegisterationId());

        return userDto;
    }

    public static List<UserDTO> getDTO(List<User> results) {
        ArrayList<UserDTO> userDtos = new ArrayList<UserDTO>();
        for (User user : results) {
            UserDTO userDTO = new UserDTO(user.getName(), user.getEmail(), user.getFirstName(),
                    user.getLastName(), user.getPassword(), user.getBirthday(), user.getUserEvents(),
                    user.getUserProducts(), user.getProductReserves(), user.getFriendListsForUserId(),
                    user.getFriendListsForFriendId());
            userDTO.setId(user.getId());
            userDTO.setPicture(user.getPicture());
            userDtos.add(userDTO);
        }
        return userDtos;
    }

    public static ProductDTO getDTO(Product product) {
        ProductDTO productDTO = new ProductDTO();
        if (product != null) {
            productDTO.setCategoryDTO(PojoUtil.getDTO(product.getCategory()));
            productDTO.setCompanyDTO(PojoUtil.getDTO(product.getCompany()));
            productDTO.setDescription(product.getDescription());
            productDTO.setId(product.getId());
            productDTO.setImage(product.getImage());
            productDTO.setName(product.getName());
            productDTO.setPrice(product.getPrice());
            productDTO.setUserProductDTO(null);
            return productDTO;
        }
        return null;
    }

    public static Product getPojo(ProductDTO productDTO) {
        Product product = new Product();
        if (productDTO != null) {
            product.setCategory(PojoUtil.getPojo(productDTO.getCategoryDTO()));
            product.setCompany(PojoUtil.getPojo(productDTO.getCompanyDTO()));
            product.setDescription(productDTO.getDescription());
            product.setId(productDTO.getId());
            product.setImage(productDTO.getImage());
            product.setName(productDTO.getName());
            product.setPrice(productDTO.getPrice());
            product.setStoreOwner(PojoUtil.getPojo(productDTO.getStoreOwnerDTO()));

            return product;
        }
        return null;
    }

    public static StoreOwner getPojo(StoreOwnerDTO storeOwnerDTO) {

        StoreOwner storeOwner = new StoreOwner();
        if (storeOwnerDTO != null) {
            storeOwner.setBirthday(storeOwnerDTO.getBirthday());
            storeOwner.setEmail(storeOwnerDTO.getEmail());
            storeOwner.setFirstName(storeOwnerDTO.getFirstName());

            storeOwner.setId(storeOwnerDTO.getId());
            storeOwner.setLastName(storeOwnerDTO.getLastName());
            storeOwner.setName(storeOwnerDTO.getName());
            storeOwner.setPassword(storeOwnerDTO.getPassword());
            storeOwner.setPicture(storeOwnerDTO.getPicture());
            storeOwner.setAdmin(storeOwnerDTO.isAdmin());
            return storeOwner;
        }
        return null;
    }

    public static CategoryDTO getDTO(Category category) {
        CategoryDTO categoryDTO = new CategoryDTO();
        if (category != null) {
            categoryDTO.setId(category.getId());
            categoryDTO.setImage(category.getImage());
            categoryDTO.setName(category.getName());
            categoryDTO.setProducts(null);
            return categoryDTO;
        }
        return null;
    }

    public static Category getPojo(CategoryDTO categoryDTO) {
        Category category = new Category();
        if (categoryDTO != null) {
            category.setId(categoryDTO.getId());
            category.setImage(categoryDTO.getImage());
            category.setName(categoryDTO.getName());
            category.setProducts(null);
            return category;
        }
        return null;
    }

    public static CompanyDTO getDTO(Company company) {
        CompanyDTO companyDTO = new CompanyDTO();
        if (company != null) {
            companyDTO.setId(company.getId());
            companyDTO.setName(company.getName());
            companyDTO.setProductses(null);
            return companyDTO;
        }
        return null;
    }

    public static Company getPojo(CompanyDTO companyDTO) {
        Company company = new Company();
        if (companyDTO != null) {
            company.setId(companyDTO.getId());
            company.setName(companyDTO.getName());
            company.setProducts(companyDTO.getProductses());
            return company;
        }
        return null;
    }

    public static User getPojo(UserDTO userDTO) {
        User user = new User();
        if (userDTO != null) {
            user.setBirthday(userDTO.getBirthday());
            user.setEmail(userDTO.getEmail());
            user.setFacebookUserAccessToken(userDTO.getFacebookUserAccessToken());
            user.setFacebookUserId(userDTO.getFacebookUserId());
            user.setFirstName(userDTO.getFirstName());
            user.setFriendListsForFriendId(userDTO.getFriendListsForFriendId());
            user.setFriendListsForUserId(userDTO.getFriendListsForUserId());
            user.setId(userDTO.getId());
            user.setLastName(userDTO.getLastName());
            user.setName(userDTO.getName());
            user.setPassword(userDTO.getPassword());
            user.setPicture(userDTO.getPicture());
            user.setProductReserves(userDTO.getProductReserves());
            user.setUserEvents(userDTO.getUserEvents());
            user.setUserProducts(userDTO.getUserProducts());
            return user;
        }
        return null;
    }

    public static StoreOwnerDTO getDTO(StoreOwner results) {
        StoreOwnerDTO storeOwnerDTO = new StoreOwnerDTO();
        if (storeOwnerDTO != null) {
            storeOwnerDTO.setBirthday(results.getBirthday());
            storeOwnerDTO.setEmail(results.getEmail());

            storeOwnerDTO.setFirstName(results.getFirstName());

            storeOwnerDTO.setId(results.getId());
            storeOwnerDTO.setLastName(results.getLastName());
            storeOwnerDTO.setName(results.getName());
            storeOwnerDTO.setPassword(results.getPassword());
            storeOwnerDTO.setPicture(results.getPicture());

            storeOwnerDTO.setAdmin(results.isAdmin());
            return storeOwnerDTO;
        }
        return null;
    }

    public static List<StoreOwnerDTO> getDTOs(List<StoreOwner> results) {
        List<StoreOwnerDTO> list = new ArrayList<StoreOwnerDTO>();
        for (StoreOwner storeOwner : results) {
            list.add(getDTO(storeOwner));
        }
        return list;
    }

    public static EventTypeDTO getDTO(EventType eventTypePojo) {
        EventTypeDTO eventType = null;
        if (eventTypePojo != null) {
            eventType = new EventTypeDTO();
            eventType.setEventPlaces(null);
            eventType.setId(eventTypePojo.getId());
            eventType.setName(eventTypePojo.getName());
            eventType.setUserEvents(null);
            eventType.setGreetings(null);

            return eventType;
        }
        return null;
    }

    public static UserEventDTO getDTO(UserEvent userEventPojo) {

        UserEventDTO userEvent = null;

        if (userEventPojo != null) {
            userEvent = new UserEventDTO();
            userEvent.setDate(userEventPojo.getDate());
            userEvent.setDescription(userEventPojo.getDescription());
            userEvent.setEventTypeDTO(getDTO(userEventPojo.getEventType()));
            userEvent.setEventCommentses(getDTO(userEventPojo.getEventCommentses()));
            userEvent.setId(userEventPojo.getId());
            userEvent.setName(userEventPojo.getName());
            userEvent.setPlace(PojoUtil.getDTO(userEventPojo.getPlace()));
            userEvent.setTime(userEventPojo.getTime());
            userEvent.setUserDTO(getDTO(userEventPojo.getUser()));
            if (userEventPojo.getPicture() != null) {
                userEvent.setImage(userEventPojo.getPicture());
            }

            return userEvent;
        }
        return null;
    }

    public static PlaceDTO getDTO(Place placePojo) {
        PlaceDTO placeDto = null;
        if (placePojo != null) {
            placeDto = new PlaceDTO();
            placeDto.setId(placePojo.getId());
            placeDto.setName(placePojo.getName());
            placeDto.setDescription(placePojo.getDescription());
            placeDto.setAddress(placePojo.getAddress());
            placeDto.setTelNo(placePojo.getTelNo());
            placeDto.setWebSite(placePojo.getWebSite());
            placeDto.setEventPlaces(null);
            placeDto.setGooglePlaceId(placePojo.getGooglePlaceId());
            return placeDto;
        }

        return null;
    }

    public static UserEvent getPojo(UserEventDTO userEventDTO) {

        UserEvent eventPojo = null;
        if (userEventDTO != null) {
            eventPojo = new UserEvent();
            if (userEventDTO.getId() != null) {
                eventPojo.setId(userEventDTO.getId());
            }
            // eventPojo.setId(userEventDTO.getId());
            eventPojo.setName(userEventDTO.getName());
            eventPojo.setDate(userEventDTO.getDate());
            eventPojo.setDescription(userEventDTO.getDescription());
            eventPojo.setPlace(PojoUtil.getPojo(userEventDTO.getPlace()));
            eventPojo.setTime(userEventDTO.getTime());
            eventPojo.setUser(getPojo(userEventDTO.getUserDTO()));
            eventPojo.setEventCommentses(null);
            eventPojo.setEventType(getPojo(userEventDTO.getEventTypeDTO()));
            if (userEventDTO.getImage() != null) {
                eventPojo.setPicture(userEventDTO.getImage());
            }

            return eventPojo;
        }
        return null;
    }

    public static Place getPojo(PlaceDTO placeDTO) {
        Place placePojo = null;
        if (placeDTO != null) {
            placePojo = new Place();
            placePojo.setId(placeDTO.getId());
            placePojo.setName(placeDTO.getName());
            placePojo.setDescription(placeDTO.getDescription());
            placePojo.setAddress(placeDTO.getAddress());
            placePojo.setTelNo(placeDTO.getTelNo());
            placePojo.setWebSite(placeDTO.getWebSite());
            placePojo.setEventPlaces(null);
            placePojo.setGooglePlaceId(placeDTO.getGooglePlaceId());
        }

        return placePojo;

    }

    public static EventType getPojo(EventTypeDTO eventTypeDTO) {
        EventType eventTypePojo = null;
        if (eventTypeDTO != null) {
            eventTypePojo = new EventType();
            eventTypePojo.setId(eventTypeDTO.getId());
            eventTypePojo.setName(eventTypeDTO.getName());
            eventTypePojo.setEventPlaces(null);
            eventTypePojo.setGreetings(null);
            eventTypePojo.setUserEvents(null);
            return eventTypePojo;
        }

        return null;
    }

    public static List<UserEventDTO> getUserEventDTOList(
            List<UserEvent> eventsPojoList) {
        List<UserEventDTO> list = new ArrayList<UserEventDTO>();
        for (UserEvent userEvent : eventsPojoList) {
            list.add(getDTO(userEvent));
        }
        return list;
    }

    public static List<EventTypeDTO> getEventTypeDTOList(
            List<EventType> eventTypeList) {
        List<EventTypeDTO> list = new ArrayList<EventTypeDTO>();
        for (EventType eventType : eventTypeList) {
            list.add(getDTO(eventType));
        }

        return list;
    }

    public static List<PlaceDTO> getPlacesDTOList(List<Place> placesPojoList) {
        List<PlaceDTO> list = new ArrayList<PlaceDTO>();
        for (Place place : placesPojoList) {
            list.add(getDTO(place));
        }

        return list;
    }

    public static UserProduct getPojo(UserProductDTO userProduct) {
        UserProduct userPro = new UserProduct();
        userPro.setId(userProduct.getId());
        userPro.setProduct(PojoUtil.getPojo(userProduct.getProductDTO()));
        userPro.setReceived(userProduct.getReceived());
        userPro.setUser(PojoUtil.getPojo(userProduct.getUserDTO()));
        userPro.setDeleted(userProduct.isDeleted());
        return userPro;
    }

    public static ProductReserve getReservePojo(ProductReserveDTO productReserveDTO) {
        ProductReserve productReserve = null;
        if (productReserveDTO != null) {
            productReserve.setId(productReserveDTO.getId());
            productReserve.setComment(productReserveDTO.getComment());
            productReserve.setParticipate(productReserveDTO.getParticipate());
            productReserve.setUser(getPojo(productReserveDTO.getUserDTO()));
            productReserve.setUserProduct(getUserProductPojo(productReserveDTO.getUserProductDto()));
            return productReserve;
        }
        return null;
    }

    private static UserProduct getUserProductPojo(UserProductDTO userProductDTO) {
        UserProduct userProduct = null;
        if (userProductDTO != null) {
            userProduct.setId(userProductDTO.getId());
            userProduct.setProduct(getPojo(userProductDTO.getProductDTO()));
            userProduct.setUser(getPojo(userProductDTO.getUserDTO()));
            userProduct.setReceived(userProductDTO.getReceived());
            userProduct.setProductReserves(getProductReservePojos(userProductDTO.getProductReserves()));
            userProduct.setDeleted(userProductDTO.isDeleted());
        }
        return null;
    }

    private static Set<ProductReserve> getProductReservePojos(Set<ProductReserveDTO> productReserveDTO) {
        Set<ProductReserve> productReserveSet = new HashSet<ProductReserve>();
        for (ProductReserveDTO product : productReserveDTO) {
            productReserveSet.add(getProductReservePojo(product));
        }
        return productReserveSet;
    }

    private static ProductReserve getProductReservePojo(ProductReserveDTO productReserveDTO) {
        ProductReserve productReserve = null;
        if (productReserveDTO != null) {
            productReserve.setId(productReserveDTO.getId());
            productReserve.setUser(getPojo(productReserveDTO.getUserDTO()));
            productReserve.setComment(productReserveDTO.getComment());
            productReserve.setParticipate(productReserveDTO.getParticipate());
            productReserve.setUserProduct(getUserProductPojo(productReserveDTO.getUserProductDto()));
            productReserve.setDeleted(productReserveDTO.isDeleted());
            return productReserve;
        }
        return null;
    }

    public static IWishConfig getPojo(IWishConfigDTO selected) {

        IWishConfig iWishConfig = new IWishConfig();
        iWishConfig.setConfigKey(selected.getConfigKey());
        iWishConfig.setConfigValue(selected.getConfigValue());

        return iWishConfig;
    }

    public static IWishConfigDTO getDTO(IWishConfig selected) {

        IWishConfigDTO iWishConfigDTO = new IWishConfigDTO();

        iWishConfigDTO.setId(selected.getId());
        iWishConfigDTO.setConfigKey(selected.getConfigKey());
        iWishConfigDTO.setConfigValue(selected.getConfigValue());

        return iWishConfigDTO;
    }

    public static NotificationDTO getDTO(Notification notification) {
        if (notification != null) {
            NotificationDTO dto = new NotificationDTO();
            dto.setDelivered(notification.isDelivered());
            dto.setDetail(notification.getDetail());
            dto.setId(notification.getId());
            dto.setPicture(notification.getPicture());
            dto.setReceiveTime(notification.getReceiveTime());
            dto.setUrl(notification.getUrl());
            dto.setUserDTO(getDTO(notification.getUser()));
            dto.setUserEvent(getDTO(notification.getUserEvent()));
            dto.setEventcomment(getDTO(notification.getEventComment()));
            return dto;
        }
        return null;
    }
    
    public static Notification getPojo(NotificationDTO notification){
        if(notification != null){
            Notification pojo = new Notification();
            pojo.setDelivered(notification.isDelivered());
            pojo.setDetail(notification.getDetail());
            pojo.setId(notification.getId());
            pojo.setPicture(notification.getPicture());
            pojo.setReceiveTime(notification.getReceiveTime());
            pojo.setTitle(notification.getTitle());
            pojo.setUrl(notification.getUrl());
            pojo.setUser(getPojo(notification.getUserDTO()));
            pojo.setEventComment(getPojo(notification.getEventcomment()));
            pojo.setUserEvent(getPojo(notification.getUserEvent()));
            return pojo;
        }
        return null;
    }
}
