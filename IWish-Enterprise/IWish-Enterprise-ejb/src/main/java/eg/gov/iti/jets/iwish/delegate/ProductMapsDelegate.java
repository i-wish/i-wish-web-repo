package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.CategoryDTO;
import eg.gov.iti.jets.iwish.dto.CompanyDTO;
import eg.gov.iti.jets.iwish.dto.ProductDTO;

import java.util.List;

/**
 * Created by Hossam Abdallah on 4/28/2015.
 */
public interface ProductMapsDelegate {
	List<CategoryDTO> getCategories();

	public List<CompanyDTO> getCompanies();

	public List<CompanyDTO> getCompanies(CategoryDTO category);

	public List<ProductDTO> getProducts(CompanyDTO company);

	public List<CompanyDTO> getAllCompanies();

	public List<CategoryDTO> getAllCategories();

	public void fillAllProducts();
}
