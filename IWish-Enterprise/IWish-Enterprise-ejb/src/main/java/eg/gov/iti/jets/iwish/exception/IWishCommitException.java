/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.exception;

import javax.ejb.ApplicationException;

/**
 *
 * @author eltntawy
 */
@ApplicationException(rollback = false)
public class IWishCommitException extends IWishDatabaseException {
    public IWishCommitException(String message){
        super(message);
    }
}
