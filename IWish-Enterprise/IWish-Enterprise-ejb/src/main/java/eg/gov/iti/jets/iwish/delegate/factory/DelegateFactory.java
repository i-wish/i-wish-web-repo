/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eg.gov.iti.jets.iwish.delegate.factory;

import eg.gov.iti.jets.iwish.delegate.*;
/**
 *
 * @author Hossam Abdallah
 */
public class DelegateFactory {


    private static FooterDataDelegateImpl footerDataDelegate = new FooterDataDelegateImpl();
    private static SuggestProductNoWishListDelegateImpl suggestProductNoWishListDelegate = new SuggestProductNoWishListDelegateImpl();
    private static SuggestGreetingDelegateImpl suggestGreetingDelegate = new SuggestGreetingDelegateImpl();
    private static SendGreetingDelegateImpl sendGreetingDelegate = new SendGreetingDelegateImpl();
    private static ProductDelegateImpl productDelegate = new ProductDelegateImpl();
    private static FacebookDelegateImpl facebookDelegate = new FacebookDelegateImpl();
    private static WishListDelegateImpl wishListDelegate = new WishListDelegateImpl();
    private static NotificationDelegateImpl notificationDelegate = new NotificationDelegateImpl();
    private static UserDelegateImpl userDelegate = new UserDelegateImpl();
    private static ProductMapsDelegateImpl productMapsDelegate = new ProductMapsDelegateImpl();
    private static StoreOwnerDelegateImpl storeOwnerDelegate = new StoreOwnerDelegateImpl();
    private static EventDelegateImpl eventDelegate = new EventDelegateImpl();
    private static PlaceDelegateImpl placeDelegate = new PlaceDelegateImpl();


    public static SuggestProductNoWishListDelegate getSuggestProductNoWishListDelegateInstance(){return suggestProductNoWishListDelegate;}
    public static SuggestGreetingDelegate getSuggestGreetingDelegateInstance(){
        return  suggestGreetingDelegate;
    }
    public static FooterDataDelegate getFooterDataDelegateInstance(){
         return footerDataDelegate;
    }
    public static SendGreetingDelegate getSendGreetingDelegateInstance(){
        return  sendGreetingDelegate;
    }
    public static ProductDelegate getProductDelegateInstance()  {
        return productDelegate;
    }
    public static FacebookDelegate getFacebookDelegateInstance() {
        return facebookDelegate;
    }
    public static NotificationDelegate getNotificationDelegateInstance() {
         return notificationDelegate;
     }
    public static UserDelegate getUserDelegateInstance() {
        return userDelegate;
    }
    public static WishListDelegate getWishListDelegate(){
        return wishListDelegate;
    }
    public static ProductMapsDelegate getProductMapsDelegate(){
        return productMapsDelegate;
    }
    public static StoreOwnerDelegate getStoreOwnerDelegate(){
        return storeOwnerDelegate;
    }
    public static EventDelegate getEventDelegate() {return eventDelegate;}
    public static PlaceDelegate getPlaceDelegate(){
        return placeDelegate;
    }
}
