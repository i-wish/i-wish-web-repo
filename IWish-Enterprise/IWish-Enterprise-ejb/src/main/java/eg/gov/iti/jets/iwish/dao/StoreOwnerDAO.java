package eg.gov.iti.jets.iwish.dao;

// Generated Mar 31, 2015 5:37:08 PM by Hibernate Tools 4.0.0

import eg.gov.iti.jets.iwish.dto.StoreOwnerDTO;
import eg.gov.iti.jets.iwish.pojo.StoreOwner;
import eg.gov.iti.jets.iwish.util.PojoUtil;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;
import java.util.logging.Logger;


/**
 * Home object for domain model class StoreOwner.
 * 
 * @see eg.gov.iti.jets.iwish.pojo.StoreOwner
 * @author Hibernate Tools
 */
@Stateless
@LocalBean
public class StoreOwnerDAO extends AbstractDAO<StoreOwner, StoreOwnerDTO>  {
	private static final Logger log = Logger.getLogger(StoreOwnerDAO.class.getName());
	
	public StoreOwnerDAO() {
		super(StoreOwner.class);
	}
	
	@Override
	public StoreOwner getEntity(StoreOwner t, StoreOwnerDTO e) {
		t.setName(e.getName());
		t.setBirthday(e.getBirthday());
		t.setEmail(e.getEmail());
		t.setPassword(e.getPassword());
		t.setPicture(e.getPicture());
		t.setLastName(e.getLastName());
		t.setFirstName(e.getFirstName());
		t.setId(e.getId());
		t.setAdmin(e.isAdmin());
		return t;
	}
	
	public StoreOwnerDTO findStoreOwnerByEmail(String email) {
		Session session = getHibernateSession();
		try {
			Query query = session.createQuery("From StoreOwner where email = :email");
			query.setString("email", email);
			StoreOwner results = (StoreOwner) query.uniqueResult();
			if (results != null)
				return PojoUtil.getDTO(results);
			else
				return null;
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public StoreOwner findByEmail(StoreOwnerDTO user) {
		String hqlQuery = "From StoreOwner u where u.email = :email";
		Query query = getHibernateSession().createQuery(hqlQuery);
		StoreOwner storeOwner = (StoreOwner) query.setParameter("email",user.getEmail()).uniqueResult();
		return storeOwner;
	}

	public StoreOwnerDTO findStoreOwnerByUsernameAndPassword(String email, String password) {
		String queryString = "From StoreOwner where email = :email And password = :password";
		Query query = getHibernateSession().createQuery(queryString);
		query.setParameter("email", email);
		query.setParameter("password", password);
		List<StoreOwner> result = (List<StoreOwner>) query.list();
		if (result != null && result.size() > 0) {
			StoreOwnerDTO storeOwnerDTO = PojoUtil.getDTO(result.get(0));
			return storeOwnerDTO;
		} else {
			return null;
		}
	}
}
