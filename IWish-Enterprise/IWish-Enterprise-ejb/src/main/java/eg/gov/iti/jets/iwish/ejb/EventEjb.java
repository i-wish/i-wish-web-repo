package eg.gov.iti.jets.iwish.ejb;

import eg.gov.iti.jets.iwish.dao.*;
import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.PlaceDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;
import eg.gov.iti.jets.iwish.interceptor.LoggerInterceptor;
import eg.gov.iti.jets.iwish.pojo.*;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Stateless(mappedName = "EventEjb")
@LocalBean
@Interceptors(LoggerInterceptor.class)
public class EventEjb {

    @Inject
    UserEventDAO eventDAO;

    @Inject
    PlaceDAO placeDAO;

    @Inject
    EventTypeDAO eventTypeDAO;

    @Inject
    EventPlaceDAO eventPlaceDAO;

    @Inject
    UserDAO userDAO;

    @Inject
    NotificationDAO notificationDAO;

    @Inject
    EventCommentsDAO eventCommentsDAO;

    public void addEvent(UserEventDTO event) {

        UserEvent eventPojo = PojoUtil.getPojo(event);

        User user = userDAO.findUserById(event.getUserDTO().getId());

        eventPojo.setUser(user);
        /*EventPlace eventPlace=new EventPlace();
         eventPlace.setEventType(eventPojo.getEventType());
         eventPlace.setPlace(eventPojo.getPlace());*/
        Place place = placeDAO.getPlaceByGooglePlaceId(event.getPlace().getGooglePlaceId());
        if (place != null){
            eventPojo.setPlace(place);
        }
        eventDAO.persist(eventPojo);
        event.setId(eventPojo.getId());
        //eventPlaceDAO.persist(eventPlace);

    }

    public void updateEvent(UserEventDTO event) {
        //UserDTO user = userDAO.findByExample(PojoUtil.getPojo(event.getUserDTO())).get(0);
        //event.setUserDTO(user);
        UserEvent eventPojo = PojoUtil.getPojo(event);
        Place place = placeDAO.getPlaceByGooglePlaceId(event.getPlace().getGooglePlaceId());
        if (place != null){
            eventPojo.setPlace(place);
        }
        eventDAO.saveOrUpdate(eventPojo);
    }

    public void removeEvent(UserEventDTO event) {

        UserEvent eventPojo = PojoUtil.getPojo(event);
        User user = userDAO.findEntityById(eventPojo.getUser().getId());
        eventPojo.setUser(user);
        eventPojo = eventDAO.findEntityById(eventPojo.getId());

        Set<EventComments> eventCommentsList = eventPojo.getEventCommentses();
        if(eventCommentsList != null && eventCommentsList.size() > 0) {
            for(EventComments eventComments : eventCommentsList) {
                eventCommentsDAO.delete(eventComments);
            }
        }


        List<Notification> notifications=  notificationDAO.getNotificationsOfEvent(eventPojo);

        if(notifications != null && notifications.size() > 0) {
            for(Notification notification : notifications) {
                notificationDAO.delete(notification);
            }
        }

        eventDAO.delete(eventPojo);


    }

    public List<UserEventDTO> getEvents(UserDTO user) {
        if(user.getId()==null && user.getFacebookUserId()!=null){
            user=userDAO.findByFacebookId(user.getFacebookUserId());
        }
        User userPojo = PojoUtil.getPojo(user);
        userPojo = userDAO.findEntityById(userPojo.getId());
        user = PojoUtil.getDTO(userPojo);
        return eventDAO.FindByUserId(user);
//		UserEvent event = new UserEvent();
//                
//		User userPojo=PojoUtil.getPojo(user);
//                userPojo=userDAO.findEntityByExample(userPojo).get(0);
//                user=PojoUtil.getDTO(userPojo);
//		event.setUser(userPojo);
//		
//		List<UserEventDTO> eventList = eventDAO.FindByUserId(user);
//		//List<UserEventDTO> eventDTOlist =PojoUtil.getUserEventDTOList(eventList);
//	
//		return eventList;
    }

    public List<EventTypeDTO> getEventTypes() {
        EventType eventType = new EventType();
        List<EventType> eventTypeList = eventTypeDAO.findEntityByExample(eventType);
        List<EventTypeDTO> eventTypeDTOList = PojoUtil.getEventTypeDTOList(eventTypeList);

        return eventTypeDTOList;
    }

    public List<PlaceDTO> getPlaces() {
        Place place = new Place();
        List<PlaceDTO> placesList = placeDAO.findByExample(place);
        return placesList;
    }

    public List<UserEventDTO> getFriendEvents(UserDTO user) {


        List<UserDTO> friends = userDAO.getFriends(user);

        List<UserEventDTO> events = new ArrayList<UserEventDTO>();

        for (UserDTO friend : friends) {
            List<UserEventDTO> friendEvetns = getEvents(friend);
            //return friendEvetns;
            events.addAll(friendEvetns);
        }

        return events;
    }

    public ArrayList<UserEventDTO> getFriendsEvents(UserDTO userDTO) {
        ArrayList<UserDTO> friends = userDAO.getFriends(userDTO);

        ArrayList<UserEventDTO> events = new ArrayList<UserEventDTO>();

        for (UserDTO friend : friends) {
            List<UserEventDTO> friendEvetns = getEvents(friend);
            events.addAll(friendEvetns);
        }

        return events;
    }

    public UserEventDTO getEvent(UserEventDTO event) {
        return eventDAO.findById(event.getId());
    }

    public List<EventTypeDTO> getEventTypesByName(String eventTypeName) {
        EventType eventType = new EventType();
        List<EventType> eventTypeList = eventTypeDAO.findByName(eventTypeName);
        List<EventTypeDTO> eventTypeDTOList = PojoUtil.getEventTypeDTOList(eventTypeList);

        return eventTypeDTOList;
    }
    public boolean isEventExist(UserDTO user,EventTypeDTO eventTypeDTO) {
        List<UserEventDTO> userEventDTOs=eventDAO.findByUserAndType(user, eventTypeDTO);
        if (userEventDTOs.isEmpty()){
            return false;
        }
        return true;
    }
    }
