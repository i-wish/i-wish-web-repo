/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.GreetingDTO;

import java.util.ArrayList;

/**
 *
 * @author Hossam Abdallah
 */
public interface SuggestGreetingDelegate {
    public ArrayList<GreetingDTO> getGreetings(EventTypeDTO eventTypeDTO);
}
