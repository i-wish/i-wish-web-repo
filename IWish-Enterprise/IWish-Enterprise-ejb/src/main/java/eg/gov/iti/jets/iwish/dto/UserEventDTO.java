package eg.gov.iti.jets.iwish.dto;

// Generated Mar 31, 2015 5:37:07 PM by Hibernate Tools 4.0.0
import eg.gov.iti.jets.iwish.pojo.UserEvent;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * UserEvent generated by hbm2java
 */
public class
        UserEventDTO extends AbstractDTO implements java.io.Serializable {

    private Integer id;
    private UserDTO userDTO;
    private EventTypeDTO eventTypeDTO;
    private String name;
    private String description;
    private PlaceDTO placeDTO;
    private Date date;
    private Date time;
    private Set<EventCommentsDTO> eventCommentsSet = new HashSet(0);
    private byte[] img;
    
    public String getUniqueString(){
            return name+"_"+id+"_"+name;
        }
    
    public UserEventDTO() {
    }

    public UserEventDTO(UserEvent userEvent) {
        this.id = userEvent.getId();
        this.userDTO = PojoUtil.getDTO(userEvent.getUser());
        this.eventTypeDTO = PojoUtil.getDTO(userEvent.getEventType());;
        this.name = userEvent.getName();
        this.description = userEvent.getDescription();
        this.placeDTO = PojoUtil.getDTO(userEvent.getPlace());
        this.date = userEvent.getDate();
        this.time = userEvent.getTime();
//        this.eventComments = PojoUtil.getDTO(userEvent.getEventComments());
    }

    public UserEventDTO(UserDTO userDTO, EventTypeDTO eventTypeDTO, String name) {
        this.userDTO = userDTO;
        this.eventTypeDTO = eventTypeDTO;
        this.name = name;
    }

    public UserEventDTO(UserDTO userDTO, EventTypeDTO eventTypeDTO, String name, String desc,
            PlaceDTO place, Date date, Date time, Set<EventCommentsDTO> eventCommentses) {
        this.userDTO = userDTO;
        this.eventTypeDTO = eventTypeDTO;
        this.name = name;
        this.description = desc;
        this.placeDTO = place;
        this.date = date;
        this.time = time;
        this.eventCommentsSet = eventCommentses;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserDTO getUserDTO() {
        return this.userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public EventTypeDTO getEventTypeDTO() {
        return this.eventTypeDTO;
    }

    public void setEventTypeDTO(EventTypeDTO eventTypeDTO) {
        this.eventTypeDTO = eventTypeDTO;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String desc) {
        this.description = desc;
    }

    public PlaceDTO getPlace() {
        return this.placeDTO;
    }

    public void setPlace(PlaceDTO place) {
        this.placeDTO = place;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTime() {
        return this.time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Set<EventCommentsDTO> getEventCommentses() {
        return this.eventCommentsSet;
    }

    public void setEventCommentses(Set<EventCommentsDTO> eventCommentses) {
        this.eventCommentsSet = eventCommentses;
    }

    public void setImage(byte[] image) {
        this.img = image;
    }

    public byte[] getImage() {
        return img;
    }

    @Override
    public String getValueString() {
        return getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserEventDTO that = (UserEventDTO) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
