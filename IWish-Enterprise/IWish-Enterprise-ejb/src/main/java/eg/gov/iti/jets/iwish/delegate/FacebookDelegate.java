package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.facebook.type.FacebookPost;

/**
 * Created by eltntawy on 22/04/15.
 */

public interface FacebookDelegate {

    String getFacebookDialogUrl(String redirectUrl);

    UserDTO getFacebookUserData(String redirectUrl, String verificationCode);
    
    UserDTO getFacebookUserData(String accessToken);
    
    void updateFacebookUserFriend(UserDTO userDTO);

    public void postOnFacebook(UserDTO userDTO, FacebookPost post);

    public void pushFacebookNotification(UserDTO userDTO, String href, String message);

}
