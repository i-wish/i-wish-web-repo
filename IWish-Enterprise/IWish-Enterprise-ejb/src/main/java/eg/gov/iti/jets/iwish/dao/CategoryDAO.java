package eg.gov.iti.jets.iwish.dao;

// Generated Mar 31, 2015 5:37:08 PM by Hibernate Tools 4.0.0

import eg.gov.iti.jets.iwish.dto.CategoryDTO;
import eg.gov.iti.jets.iwish.pojo.Category;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.logging.Logger;

/**
 * Home object for domain model class Category.
 * 
 * @see eg.gov.iti.jets.iwish.pojo.Category
 * @author Hibernate Tools
 */
@Stateless
@LocalBean
public class CategoryDAO extends AbstractDAO<Category, CategoryDTO> {
	private static final Logger log = Logger.getLogger(CategoryDAO.class.getName());

	public CategoryDAO() {
		super(Category.class);
	}

	@Override
	public Category getEntity(Category t, CategoryDTO e) {
		t.setName(e.getName());
		t.setImage(e.getImage());
		return t;
	}

}
