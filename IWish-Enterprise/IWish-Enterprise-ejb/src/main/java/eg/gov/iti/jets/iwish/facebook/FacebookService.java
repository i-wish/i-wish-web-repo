package eg.gov.iti.jets.iwish.facebook;

import com.restfb.*;
import com.restfb.FacebookClient.AccessToken;
import com.restfb.scope.ScopeBuilder;
import com.restfb.scope.UserDataPermissions;
import com.restfb.types.FacebookType;
import com.restfb.types.User;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.facebook.type.FacebookPost;

import javax.ejb.Stateless;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static eg.gov.iti.jets.iwish.facebook.type.FacebookPost.FacebookPostType;

/**
 * Created by eltntawy on 22/04/15.
 */
@Stateless
public class FacebookService {

    private static String appID;
    private static String appSecret;
    private static String appAccessToken;
    private static FacebookClient client = new DefaultFacebookClient(Version.VERSION_2_2);
    private String loginDialogUrlString = null;

    public FacebookService() {

        Properties properties = new Properties();
        try {
            InputStream is = getClass().getResourceAsStream("facebook-app.properties");
            properties.load(is);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Get added security by using your app secret:
        appID = properties.getProperty("app.id");
        appSecret = properties.getProperty("app.secret");
    }
    
    /**
     * @param redirectUrl is the url the facebook will call when user login with
     * his facebook account
     * @return loginDialogUrl facebook login dialog will appear to the user to
     * login
     */
    public String getLoginDialogUrl(String redirectUrl) {


            if(loginDialogUrlString == null) {

                System.out.println("getdialog : " + redirectUrl);
                ScopeBuilder scopeBuilder = new ScopeBuilder();

                // Permission scope the the app should use
                // friends_events,friends_birthday,friends_about_me,user_about_me,friends_birthday,public_profile,publish_stream,offline_access,user_birthday,user_events,user_friends,user_posts,publish_actions
                scopeBuilder.addPermission(UserDataPermissions.USER_ABOUT_ME);
                scopeBuilder.addPermission(UserDataPermissions.USER_BIRTHDAY);
                scopeBuilder.addPermission(UserDataPermissions.USER_FRIENDS);
                scopeBuilder.addPermission(UserDataPermissions.USER_LOCATION);
                scopeBuilder.addPermission(UserDataPermissions.USER_LIKES);
                scopeBuilder.addPermission(UserDataPermissions.USER_HOMETOWN);
                scopeBuilder.addPermission(UserDataPermissions.USER_STATUS);
                //    scopeBuilder.addPermission(UserDataPermissions.USER_POSTS);
                scopeBuilder.addPermission(UserDataPermissions.USER_RELATIONSHIPS);

                // login dialog url
                FacebookClient client = new DefaultFacebookClient(Version.VERSION_2_2);
                loginDialogUrlString = client.getLoginDialogUrl(appID, redirectUrl, scopeBuilder)
                        + ",email,manage_notifications,publish_actions";
            }


        return loginDialogUrlString;
    }

    /**
     * @return return object of facebook application access token
     */
    public FacebookClient.AccessToken getAppAccessToken() {
        // Obtains an access token which can be used to perform Graph API operations
        // on behalf of an application instead of a user.

        FacebookClient.AccessToken accessToken
                = new DefaultFacebookClient().obtainAppAccessToken(appID, appSecret);
        return accessToken;
    }

    /**
     * *
     *
     * @param redirectUri the redirect URI you used to get verificationCode
     * @param verificationCode the verificationCode you get from facebook
     * @return return UserDTO with the data
     * {FacebookUserId,FacebookUserAccessToken, Name, Email, Birthday,
     * FirstName, LastName} only. any other data will be null
     */
    public UserDTO getFacebookUserData(String redirectUri, String verificationCode) {
        FacebookClient.AccessToken accessToken = client.obtainUserAccessToken(appID, appSecret, redirectUri, verificationCode);
        client = new DefaultFacebookClient(accessToken.getAccessToken(), Version.VERSION_2_2);
        User user = client.fetchObject("me", User.class);

        UserDTO userDTO = FacebookUserUtil.convertToUserDTO(user, accessToken.getAccessToken());

        if(userDTO != null && userDTO.getEmail() == null) {
            userDTO.setEmail(userDTO.getFacebookUserId()+"@facebook.com");
        }
        if (userDTO != null) {
            userDTO.setPicture(getFacebookUserPicture(userDTO,userDTO.getFacebookUserAccessToken()));
        }
        return userDTO;
    }

    /**
     * *
     *
     * @param accessToken the accessToken associated with user and appId
     * @return return UserDTO with the data
     * {FacebookUserId,FacebookUserAccessToken, Name, Email, Birthday,
     * FirstName, LastName} only. any other data will be null
     */
    public UserDTO getFacebookUserData(String accessToken) {

        FacebookClient client = new DefaultFacebookClient(accessToken, Version.VERSION_2_2);
        User user = client.fetchObject("me", User.class);

        UserDTO userDTO = FacebookUserUtil.convertToUserDTO(user, accessToken);

        if (userDTO != null) {
            userDTO.setPicture(getFacebookUserPicture(userDTO,userDTO.getFacebookUserAccessToken()));
        }
        return userDTO;
    }

     public byte[] getFacebookUserPicture(UserDTO userDTO,String accessToken) {

        String picUrl = "https://graph.facebook.com/"+userDTO.getFacebookUserId()+
                "/picture?type=large&access_token="+accessToken;

        try {

            URL url = new URL(picUrl);
            InputStream is = url.openStream();

            byte[] picByte = new byte[url.openConnection().getContentLength()];

            is.read(picByte);
            is.close();

            return picByte;

        } catch (IOException ex) {
            return null;
        }

    }

    public void postOnFacebook(UserDTO userDTO, FacebookPost post) {
        if (post.getMessage() == null) {
            throw new IllegalArgumentException("message can not be null");
        }
        if (userDTO.getFacebookUserAccessToken() == null) {
            throw new IllegalArgumentException("facebook accesss_token can not be null of userDTO:" + userDTO);
        }

        FacebookClient client = new DefaultFacebookClient(userDTO.getFacebookUserAccessToken());
        Parameter parameterPicture = null;
        Parameter parameterLink = null;
        Parameter parameterDescription = null;

        Parameter parameterMessage = Parameter.with(FacebookPostType.MESSAGE + "", post.getMessage());

        if (post.getLink() != null) {
            parameterLink = Parameter.with(FacebookPostType.LINK + "", post.getLink());
        }
        if (post.getDescription() != null) {
            parameterDescription = Parameter.with(FacebookPostType.DESCRIPTION + "", post.getDescription());
        }
        if (post.getPicture() != null) {
            parameterPicture = Parameter.with(FacebookPostType.PICUTRE + "", post.getPicture());
        }

        if (parameterDescription != null && parameterLink != null && parameterPicture != null) {
            client.publish("me/feed", FacebookType.class, parameterDescription, parameterLink, parameterMessage, parameterPicture);
        }

        if (parameterDescription != null && parameterLink != null && parameterPicture == null) {
            client.publish("me/feed", FacebookType.class, parameterDescription, parameterLink, parameterMessage);
        }
        if (parameterDescription != null && parameterLink == null && parameterPicture == null) {
            client.publish("me/feed", FacebookType.class, parameterDescription, parameterMessage);
        }
        if (parameterDescription == null && parameterLink == null && parameterPicture == null) {
            client.publish("me/feed", FacebookType.class, parameterMessage);
        }
    }

    public void pushFacebookNotification(UserDTO userDTO, String href, String message) {

        if (userDTO.getFacebookUserAccessToken() == null) {
            throw new IllegalArgumentException("facebook accesss_token can not be null of userDTO:" + userDTO);
        }
        if (href == null) {
            throw new IllegalArgumentException("href can not be null of userDTO:" + userDTO);
        }
        if (message == null) {
            throw new IllegalArgumentException("messaage can not be null of userDTO:" + userDTO);
        }

        AccessToken appAccessToken = new DefaultFacebookClient().obtainAppAccessToken(appID, appSecret);
        FacebookClient facebookClient = new DefaultFacebookClient(appAccessToken.getAccessToken());

        Parameter hrefParameter = Parameter.with("href", href);
        Parameter messageParameter = Parameter.with("template", message);

        facebookClient.publish(userDTO.getFacebookUserId() + "/notifications", FacebookType.class, hrefParameter, messageParameter);
    }
    
    
    public List<UserDTO> getFacebookUserFriends(UserDTO userDTO) {
        
        if(userDTO.getFacebookUserAccessToken() == null ) throw new IllegalArgumentException("user facebook access token can not be null");
        
        FacebookClient facebookClient = new DefaultFacebookClient(userDTO.getFacebookUserAccessToken(), Version.VERSION_2_2);
        
//        User user = facebookClient.fetchObject("me", User.class);
        
        Connection<User> friendsConnection = facebookClient.fetchConnection("me/friends", User.class,
                Parameter.with("fields", "id,name,first_name,last_name,birthday,email"));
        
        List<User> friendsList = friendsConnection.getData();
        
        List<UserDTO> ret = new ArrayList<>();
        
        for(User user : friendsList) {
            UserDTO friendDTO = FacebookUserUtil.convertToUserDTO(user, "friend");
            friendDTO.setPicture(getFacebookUserPicture(friendDTO,userDTO.getFacebookUserAccessToken()));
            if(friendDTO.getEmail() == null ) friendDTO.setEmail(friendDTO.getFacebookUserId()+"@facebook.com");
            
            ret.add(friendDTO);
        }
        
        return ret;
        
    }
    
}
