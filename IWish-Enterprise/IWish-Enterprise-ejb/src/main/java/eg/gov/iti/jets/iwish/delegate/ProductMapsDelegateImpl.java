package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.CategoryDTO;
import eg.gov.iti.jets.iwish.dto.CompanyDTO;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.ejb.ProductMapsEjb;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil.ServiceLocatorType;

import java.util.List;

/**
 * Created by Hossam Abdallah on 4/28/2015.
 */

public class ProductMapsDelegateImpl implements ProductMapsDelegate {
	ProductMapsEjb productMapsEjb = (ProductMapsEjb) ServiceLocatorUtil.getInistance().lookup(ServiceLocatorType.PRODUCT_MAPS_SERVICE);

    @Override
    public List<CategoryDTO> getCategories() {
        return productMapsEjb.getCategories();
    }

    @Override
    public List<CompanyDTO> getCompanies(CategoryDTO category) {	
        return productMapsEjb.getCompanies(category);
    }

    @Override
    public List<CompanyDTO> getCompanies() {    	
        return productMapsEjb.getCompanies();
    }

    @Override
    public List<ProductDTO> getProducts(CompanyDTO company) {
        return productMapsEjb.getProducts(company);
    }

    @Override
    public List<CompanyDTO> getAllCompanies() {
        return productMapsEjb.getAllCompanies();
    }

    @Override
    public List<CategoryDTO> getAllCategories() {
        return productMapsEjb.getAllCategories();
    }

    @Override
    public void fillAllProducts() {
        productMapsEjb.fillAllProducts();
    }
}
