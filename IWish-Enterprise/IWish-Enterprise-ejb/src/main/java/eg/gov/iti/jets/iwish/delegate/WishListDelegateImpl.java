package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.ProductReserveDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserProductDTO;
import eg.gov.iti.jets.iwish.ejb.WishListEjb;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil.ServiceLocatorType;

import java.util.ArrayList;

/**
 * Created by Hossam Abdallah on 4/28/2015.
 */
public class WishListDelegateImpl implements WishListDelegate {
    WishListEjb wishListEjb = (WishListEjb) ServiceLocatorUtil.getInistance().lookup(ServiceLocatorType.WISHLIST_SERVICE);

    @Override
    public void addProducts(ArrayList<ProductDTO> products, UserDTO user) {
        wishListEjb.addProducts(products, user);
    }

    @Override
    public void deleteProducts(ArrayList<ProductDTO> products, UserDTO user) {
        wishListEjb.deleteProducts(products, user);
    }

    @Override
    public ArrayList<ProductDTO> getFriendWishList(UserDTO friend) {
        return wishListEjb.getFriendWishlist(friend);
    }

    @Override
    public ArrayList<UserProductDTO> getFriendWishListWithDetails(UserDTO friend) {
        return wishListEjb.getFriendWishlistWithDetails(friend);
    }

    @Override
    public boolean addProducts(ArrayList<Integer> products, String email) {
        return wishListEjb.addProducts(products, email);
    }

    @Override
    public boolean reserveGift(UserDTO friendDTO, UserProductDTO userProductDTO, String comment) {
        return wishListEjb.reserveGift(friendDTO, userProductDTO, comment);
    }

    @Override
    public UserProductDTO getUserProductDTO(UserDTO userDTO, ProductDTO productDTO) {
        return wishListEjb.getUserProductDTO(userDTO, productDTO);
    }

    @Override
    public ArrayList<UserProductDTO> getUserProductDTOs(UserDTO userDTO) {
        return wishListEjb.getUserProductDTOs(userDTO);
    }

    @Override
    public ProductDTO getWishlistProduct(UserProductDTO userProductDTO) {
        return wishListEjb.getWishlistProduct(userProductDTO);
    }

    @Override
    public ArrayList<UserDTO> getFriendsReserved(UserProductDTO userProductDTO) {
        return wishListEjb.getReservingFriends(userProductDTO);
    }

    @Override
    public void deleteGift(UserProductDTO userProduct) {
        wishListEjb.deleteGift(userProduct);
    }

    @Override
    public boolean reserveGiftWithOthers(UserDTO friendDTO, UserProductDTO userProductDTO, String comment, Double participate) {
        return wishListEjb.reserveGiftWithOthers(friendDTO, userProductDTO, comment, participate);
    }

    @Override
    public ArrayList<ProductReserveDTO> getProductReservers(UserProductDTO userProductDTO) {
        return wishListEjb.getProductReserves(userProductDTO);
    }

    @Override
    public UserDTO getUserReservedProduct(ProductReserveDTO productReserveDTO) {
        return wishListEjb.getUserReservedProduct(productReserveDTO);
    }

    @Override
    public boolean isReservedBefore(UserDTO friend, UserProductDTO userProductDTO) {
        return wishListEjb.isReservedBefore(friend,userProductDTO);
    }

    @Override
    public boolean deleteReservation(UserDTO friend,UserProductDTO userProductDTO){
        return wishListEjb.deleteReservation(friend,userProductDTO);
    }
}
