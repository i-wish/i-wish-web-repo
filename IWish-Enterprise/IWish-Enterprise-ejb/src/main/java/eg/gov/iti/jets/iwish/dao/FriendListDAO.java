package eg.gov.iti.jets.iwish.dao;

// Generated Mar 31, 2015 5:37:08 PM by Hibernate Tools 4.0.0

import eg.gov.iti.jets.iwish.dto.FriendListDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.pojo.FriendList;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import org.hibernate.Query;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;
import java.util.logging.Logger;

/**
 * Home object for domain model class FriendList.
 * 
 * @see eg.gov.iti.jets.iwish.pojo.FriendList
 * @author Hibernate Tools
 */
@Stateless
@LocalBean
public class FriendListDAO extends AbstractDAO<FriendList, FriendListDTO> {
	private static final Logger log = Logger.getLogger(FriendListDAO.class.getName());

	public FriendListDAO() {
		super(FriendList.class);
	}

	@Override
	public FriendList getEntity(FriendList t, FriendListDTO e) {
		t.setId(e.getId());
		t.setUserByFriendId(PojoUtil.getPojo(e.getUserDTOByFriendId()));
		t.setUserByUserId(PojoUtil.getPojo(e.getUserByUserDTOId()));
		return t;
	}

	public boolean isFriends(UserDTO user, UserDTO friend) {
		String queryString = "FROM FriendList fl "
				+ " WHERE (fl.userByUserId.id = :userId AND fl.userByFriendId.id = :friendId )";
		Query query = getHibernateSession().createQuery(queryString);
		query.setParameter("userId", user.getId());
		query.setParameter("friendId", friend.getId());
		List ret = query.list();

		if (ret != null && ret.size() > 0) {
			return true;
		} else {
			return false;
		}
	}
}
