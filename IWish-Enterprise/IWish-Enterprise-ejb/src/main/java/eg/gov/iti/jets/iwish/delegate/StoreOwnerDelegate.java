package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.CategoryDTO;
import eg.gov.iti.jets.iwish.dto.CompanyDTO;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.StoreOwnerDTO;

import java.util.List;

public interface StoreOwnerDelegate {

	StoreOwnerDTO login(String email, String password);
	
	void updateUserProfile(StoreOwnerDTO user);
	
	@Deprecated
	public void addProduct(ProductDTO productDTO, CompanyDTO companyDTO, CategoryDTO categoryDTO);

	@Deprecated
	List<ProductDTO> getProducts(StoreOwnerDTO storeOwnerDTO);

	@Deprecated
	void deleteProduct(ProductDTO productDTO);

	@Deprecated
	void updateProduct(ProductDTO productDTO);

	
}
