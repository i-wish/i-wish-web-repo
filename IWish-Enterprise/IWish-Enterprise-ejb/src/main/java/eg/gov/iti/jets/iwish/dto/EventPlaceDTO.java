package eg.gov.iti.jets.iwish.dto;

// Generated Mar 31, 2015 5:37:07 PM by Hibernate Tools 4.0.0

/**
 * EventPlace generated by hbm2java
 */
public class EventPlaceDTO extends AbstractDTO implements java.io.Serializable {

	private Integer id;
	private EventTypeDTO eventTypeDTO;
	private PlaceDTO placeDTO;

	public EventPlaceDTO() {
	}

	public EventPlaceDTO(EventTypeDTO eventTypeDTO, PlaceDTO placeDTO) {
		this.eventTypeDTO = eventTypeDTO;
		this.placeDTO = placeDTO;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EventTypeDTO getEventTypeDTO() {
		return this.eventTypeDTO;
	}

	public void setEventTypeDTO(EventTypeDTO eventTypeDTO) {
		this.eventTypeDTO = eventTypeDTO;
	}

	public PlaceDTO getPlaceDTO() {
		return this.placeDTO;
	}

	public void setPlaceDTO(PlaceDTO placeDTO) {
		this.placeDTO = placeDTO;
	}

	@Override
	public String getValueString() {
		// TODO insert name
		return null;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		EventPlaceDTO that = (EventPlaceDTO) o;

		if (id != null ? !id.equals(that.id) : that.id != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}
}
