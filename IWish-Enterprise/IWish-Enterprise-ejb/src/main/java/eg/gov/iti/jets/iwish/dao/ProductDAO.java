package eg.gov.iti.jets.iwish.dao;

// Generated Mar 31, 2015 5:37:08 PM by Hibernate Tools 4.0.0

import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.StoreOwnerDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserProductDTO;
import eg.gov.iti.jets.iwish.pojo.*;
import eg.gov.iti.jets.iwish.util.PojoUtil;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Home object for domain model class Products.
 * 
 * @see eg.gov.iti.jets.iwish.pojo.Product
 * @author Hibernate Tools
 */
@Stateless
@LocalBean
public class ProductDAO extends AbstractDAO<Product, ProductDTO> {
	private static final Logger log = Logger.getLogger(ProductDAO.class.getName());

	@Inject
	UserDAO userDAO;

	@Inject
	StoreOwnerDAO storeOwnerDAO;

	@Inject
	CompanyDAO companyDAO;

	@Inject
	CategoryDAO categoryDAO;

	public ProductDAO() {
		super(Product.class);
	}

	@Override
	public Product getEntity(Product t, ProductDTO e) {
		t.setName(e.getName());
		t.setDescription(e.getDescription());
		t.setPrice(e.getPrice());
		t.setImage(e.getImage());
		t.setId(e.getId());
		StoreOwner storeOwner = storeOwnerDAO.findEntityById(e.getStoreOwnerDTO().getId());
		Category category = categoryDAO.findEntityById(e.getCategoryDTO().getId());
		Company company = companyDAO.findEntityById(e.getCompanyDTO().getId());
		t.setStoreOwner(storeOwner);
		t.setCategory(category);
		t.setCompany(company);
		return t;
	}

	public ArrayList<ProductDTO> getUserProductsList(UserDTO userDTO) {
		try {
			User user = userDAO.findUserById(userDTO.getId());
			String hql = "Select userProduct.product "
					+ " From UserProduct userProduct where userProduct.user = :user and userProduct.deleted = 0";
			Query query = getHibernateSession().createQuery(hql);
			query.setParameter("user", user);
			List<Product> products = (List<Product>) query.list();
			List<ProductDTO> productDTOs = PojoUtil.getDTOList(products);
			return (ArrayList<ProductDTO>) productDTOs;
		} catch (RuntimeException ex) {
			log.log(Level.SEVERE, "get user products failed", ex);
			throw ex;
		}
	}

	public ArrayList<UserProductDTO> getUserProductsDTOList(UserDTO userDTO) {
		try {
			User user = userDAO.findUserById(userDTO.getId());
			String hql = " From UserProduct userProduct where userProduct.user = :user and userProduct.deleted = 0";
			Query query = getHibernateSession().createQuery(hql);
			query.setParameter("user", user);
			ArrayList<UserProduct> products = (ArrayList<UserProduct>) query.list();
			List<UserProductDTO> productDTOs = PojoUtil.getDTOList(products);
			return (ArrayList<UserProductDTO>) productDTOs;
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	public Product findById(ProductDTO product) {
		String hqlQuery = "From Product u where u.id = :id";
		Query query = getHibernateSession().createQuery(hqlQuery);
		Product object = (Product) query.setParameter("id", product.getId()).uniqueResult();
		return object;
	}

	public List<ProductDTO> findStoreOwnerProducts(StoreOwnerDTO storeOwnerDTO) {
		Query query = getHibernateSession().createQuery("Select new eg.gov.iti.jets.iwish.dto.ProductDTO(p) from Product p where p.storeOwner.id = :storeOwnerId");
		query.setParameter("storeOwnerId", storeOwnerDTO.getId());
		List<ProductDTO> products = query.list();
		return products;
	}

	public void deleteAllProduct() {
		log.fine("deleting all Products");
		try {
			Query query = getHibernateSession().createQuery("DELETE FROM Product");
			int result = query.executeUpdate();
			log.fine("after delete method result is : " + result);
		} catch (RuntimeException ex) {
			log.log(Level.SEVERE, "delete products failed", ex);
			throw ex;
		}
	}

	public Long getProductsCount() {
		Session session = getHibernateSession();
		Query query = session.createQuery("Select count(*) From Product");
		Long result = (Long) query.uniqueResult();
		return result;
	}
}