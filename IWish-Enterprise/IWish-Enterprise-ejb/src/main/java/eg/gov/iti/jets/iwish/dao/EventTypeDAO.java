package eg.gov.iti.jets.iwish.dao;

// Generated Mar 31, 2015 5:37:08 PM by Hibernate Tools 4.0.0

import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.pojo.EventType;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import org.hibernate.Query;
import org.hibernate.criterion.Example;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Home object for domain model class EventType.
 * 
 * @see eg.gov.iti.jets.iwish.pojo.EventType
 * @author Hibernate Tools
 */
@Stateless
@LocalBean
public class EventTypeDAO extends AbstractDAO<EventType, EventTypeDTO> {
	private static final Logger log = Logger.getLogger(EventTypeDAO.class.getName());

	public EventTypeDAO() {
		super(EventType.class);
	}

	@Override
	//FIXME
	//why not to copy other attributes
	public EventType getEntity(EventType t, EventTypeDTO e) {
		t.setName(e.getName());
		return t;
	}

	//FIXME
	//why to create findByExample while there is a one already
	public List findByExample(EventType instance) {
		log.log(Level.FINE, "finding EventType instance by example");
		try {
			List results = getHibernateSession().createCriteria("eg.gov.iti.jets.iwish.pojo.EventType")
					.add(Example.create(instance)).list();
			log.log(Level.FINE, "find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.log(Level.SEVERE, "find by example failed", re);
			throw re;
		}
	}

	public List findByName(String eventName) {
		log.log(Level.FINE, "finding EventType instance by Name");
		try {
			Query query = getHibernateSession().createQuery("From EventType where name= :name");
			query.setParameter("name", eventName);
			List results = query.list();
			log.log(Level.FINE, "find by name successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.log(Level.SEVERE, "find by neme failed", re);
			throw re;
		}
	}

}
