package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.NotificationDTO;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserProductDTO;
import eg.gov.iti.jets.iwish.ejb.UserEjb;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil.ServiceLocatorType;

import java.util.ArrayList;
import java.util.List;

public class UserDelegateImpl implements UserDelegate {
	UserEjb userEjb = (UserEjb) ServiceLocatorUtil.getInistance().lookup(ServiceLocatorType.USER_DATA_SERVICE);

	@Override
	public List<ProductDTO> getUserProductsList(String email) {
		UserDTO userDTO = userEjb.findUserByEmail(email);
		if (userDTO != null) {
			List<ProductDTO> products = userEjb.getUserProductsList(userDTO);
			return products;
		} else {
			return null;
		}
	}

	@Override
	public ArrayList<UserDTO> getFriends(UserDTO user) {
		if (user == null)
			throw new IllegalArgumentException("input parameter can not be null");
		
		return userEjb.getFriends(user);
	}

	@Override
	@Deprecated
	/**
	 * use the one in notification delegate
	 */
	public List<NotificationDTO> getAllNotification(UserDTO userDTO) {
		return userEjb.getNotification(userDTO);
	}
	

	@Override
	@Deprecated
	/**
	 * use the one in notification delegate
	 */
	public List<NotificationDTO> getUnReadedUserNotification(UserDTO userDTO) {
		return userEjb.getUnReadedNotification(userDTO);
	}

	@Override
	@Deprecated
	/**
	 * use the one in product delegate
	 */
	public List<UserProductDTO> getUserProductsDTOsList(UserDTO user) {
		return userEjb.getUserProductsDTOsList(user);
	}

	@Override
	public UserDTO login(String email, String password) {
		return userEjb.login(email, password);
	}

	@Override
	public void saveRegId(UserDTO userDTO, String regId) {
		userEjb.saveRegId(userDTO, regId);
	}

	@Override
	public UserDTO findById(int id) {
		return userEjb.findById(id);
	}

	@Override
	public void updateUserProfile(UserDTO user) {
		userEjb.updateUser(user);
	}

	@Override
	/**
	 * @author Hossam ElDeen
	 */
	public UserDTO findUserByFacebookId(String facebookUserId){
		if(facebookUserId == null)
			throw new IllegalArgumentException("input parameter can not be null");
		
		return userEjb.findUserByFacebookId(facebookUserId);
	}
}
