package eg.gov.iti.jets.iwish.notification;

import eg.gov.iti.jets.iwish.dto.NotificationDTO;

import java.util.List;

/**
 * Created by eltntawy on 11/06/15.
 */
public interface NotificationWebService {

    void pushNotification(String facebookUserId, NotificationDTO notificationDTO);

    void pushNotifications(String facebookUserId, List<NotificationDTO> notificationList);
}
