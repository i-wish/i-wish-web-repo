/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eg.gov.iti.jets.iwish.ejb;

import eg.gov.iti.jets.iwish.dao.IWishConfigDAO;
import eg.gov.iti.jets.iwish.dto.IWishConfigDTO;
import eg.gov.iti.jets.iwish.interceptor.LoggerInterceptor;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Hossam Abdallah
 */
@Stateless(mappedName = "FooterDataEjb")
@LocalBean
@Interceptors(LoggerInterceptor.class)
public class FooterDataEjb {
    @EJB
    IWishConfigDAO iWishConfigDAO;
    
    public Map<String,String> getFooterStaticText(){
        ArrayList<IWishConfigDTO> list = (ArrayList<IWishConfigDTO>)iWishConfigDAO.getAll();
        int size = list.size();
        System.out.println(size);
        Map<String, String> data = new HashMap<String, String>();
        for(IWishConfigDTO item: list){
            data.put(item.getConfigKey(),item.getConfigValue());
        }
        return data;
    }
}
