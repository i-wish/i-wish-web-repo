package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.CategoryDTO;
import eg.gov.iti.jets.iwish.dto.CompanyDTO;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.StoreOwnerDTO;
import eg.gov.iti.jets.iwish.ejb.ProductEjb;
import eg.gov.iti.jets.iwish.ejb.StoreOwnerEjb;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil.ServiceLocatorType;

import java.util.List;


public class StoreOwnerDelegateImpl implements StoreOwnerDelegate {
	StoreOwnerEjb storeOwnerEjb = (StoreOwnerEjb) ServiceLocatorUtil.getInistance().lookup(ServiceLocatorType.STORE_OWNER_SERVICE);
	ProductDelegate productDelegate = DelegateFactory.getProductDelegateInstance();
	
	 
	@Override
	public StoreOwnerDTO login(String email, String password) {
		StoreOwnerDTO dto = storeOwnerEjb.findStoreOwner(email, password);
		return dto;
	}
	
	@Override
	public void updateUserProfile(StoreOwnerDTO user) {
		storeOwnerEjb.updateUser(user);
	}
	
	
	
	
	
	@Override
	@Deprecated
	/**
	 * use the method found in productEjb instead with the same signature
	 */
	public void addProduct(ProductDTO productDTO, CompanyDTO companyDTO, CategoryDTO categoryDTO) {
		productDelegate.addProduct(productDTO,companyDTO,categoryDTO);
    }
	@Override 
	@Deprecated
	/**
	 * use the method found in productEjb instead with the same signature
	 */
	public List<ProductDTO> getProducts(StoreOwnerDTO storeOwnerDTO) {
		return productDelegate.getProducts(storeOwnerDTO);
	}
	@Override
	@Deprecated
	/**
	 * use the method found in productEjb instead with the same signature
	 */
	public void deleteProduct(ProductDTO productDTO) {
		productDelegate.deleteProduct(productDTO);
	}
	@Override
	@Deprecated
	/**
	 * use the method found in productEjb instead with the same signature
	 */
	public void updateProduct(ProductDTO productDTO) {
		productDelegate.updateProduct(productDTO);
	}
}
