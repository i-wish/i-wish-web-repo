/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.listeners;

import com.google.gson.Gson;
import eg.gov.iti.jets.iwish.delegate.NotificationDelegate;
import eg.gov.iti.jets.iwish.delegate.UserDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.NotificationDTO;
import eg.gov.iti.jets.iwish.notification.NotificationWebService;
import eg.gov.iti.jets.iwish.observer.NotificationObserver;
import eg.gov.iti.jets.iwish.pojo.Notification;
import eg.gov.iti.jets.iwish.pojo.User;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;

/**
 *
 * @author yomna
 */
public class NotificationListener {

    UserDelegate userDelegate;
    NotificationDelegate notificationDelegate;
    NotificationWebService notificationWebService;

    // @PostConstruct
    @PrePersist
    public void init(Object object) {

        if(notificationWebService == null) {
            notificationWebService = NotificationObserver.getNotificationWebService();
        }

        if(userDelegate  == null) {
            userDelegate = DelegateFactory.getUserDelegateInstance();
        }

        if(notificationDelegate  == null) {
            notificationDelegate = DelegateFactory.getNotificationDelegateInstance();
        }

    }

    @PostPersist
    //FIXME
    //need more documentation and adjustment
    public void onPersist(Object object) {

        if (object instanceof Notification) {
            Notification notification = (Notification) object;
            User user = notification.getUser();

            if (user.getRegisterationId() != null) {
                notificationJson notJson = new notificationJson();
                notJson.eventId = notification.getUserEvent().getId();
                notJson.message = notification.getDetail();
                notJson.notif_id= notification.getId();
                Gson gson = new Gson();
                String message = gson.toJson(notJson);
                notificationDelegate.sendNotification(message, user.getRegisterationId());
            }

            if(notificationWebService != null && user.getFacebookUserId() != null) {
                NotificationDTO notificationDTO = PojoUtil.getDTO(notification);
                notificationWebService.pushNotification(user.getFacebookUserId(),notificationDTO);
            }

        }

    }
    
    class notificationJson {
       String message;
       int eventId;
       int notif_id;
    }
}
