/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eg.gov.iti.jets.iwish.service.locator;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * 
 * @author Hossam Abdallah
 */
public class ServiceLocatorUtil {

	private static ServiceLocator instance;
	private static Context context;

	public static enum ServiceLocatorType {
		FACEBOOK_SERVICE, PRODUCT_SERVICE, WISHLIST_SERVICE, PRODUCT_MAPS_SERVICE, USER_DATA_SERVICE, STORE_OWNER_SERVICE,USEREVENT_SERVICE , SEND_GREETING_SERVICE , FOOTER_DATA_SERVICE , SUGGEST_GREETING_SERVICE , SUGGEST_PRODUCT_NO_WISHLIST_SERVICE,SUGGEST_PLACE_SERVICE,PUSH_NOTIFICATION_SERVICE
	}

	static {
		try {
			context = new InitialContext();
			
			try {
				// for glassfish
				//instance = (ServiceLocator) context.lookup("java:global/IWish-Enterprise/IWish-Enterprise-ejb/ServiceLocator!eg.gov.iti.jets.iwish.service.locator.ServiceLocator");

				// for jboss wildfly
				instance = (ServiceLocator) context.lookup("java:global/IWish-Enterprise_ear/ejb/ServiceLocator!eg.gov.iti.jets.iwish.service.locator.ServiceLocator");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	public static synchronized ServiceLocator getInistance() {
		return instance;
	}
}
