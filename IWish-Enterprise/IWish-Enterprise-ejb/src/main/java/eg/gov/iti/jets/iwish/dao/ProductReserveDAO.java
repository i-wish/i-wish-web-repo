package eg.gov.iti.jets.iwish.dao;

// Generated Mar 31, 2015 5:37:08 PM by Hibernate Tools 4.0.0
import eg.gov.iti.jets.iwish.dto.ProductReserveDTO;
import eg.gov.iti.jets.iwish.pojo.ProductReserve;
import eg.gov.iti.jets.iwish.pojo.User;
import eg.gov.iti.jets.iwish.pojo.UserProduct;
import eg.gov.iti.jets.iwish.util.PojoUtil;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Home object for domain model class ProductReserve.
 *
 * @see eg.gov.iti.jets.iwish.pojo.ProductReserve
 * @author Hibernate Tools
 */
@Stateless
@LocalBean
public class ProductReserveDAO extends AbstractDAO<ProductReserve, ProductReserveDTO> {
	private static final Logger log = Logger.getLogger(ProductReserveDAO.class.getName());

	@Inject
	UserProductDAO userProductDAO;

	public ProductReserveDAO() {
		super(ProductReserve.class);
	}

	@Override
	public ProductReserve getEntity(ProductReserve t, ProductReserveDTO e) {
		t.setComment(e.getComment());
		t.setDeleted(e.isDeleted());
		t.setId(e.getId());
		t.setParticipate(e.getParticipate());
		t.setUser(PojoUtil.getPojo(e.getUserDTO()));
		t.setUserProduct(PojoUtil.getPojo(e.getUserProductDto()));
		return t;
	}

	public ArrayList<ProductReserve> findByUserProduct(UserProduct userProduct) {
		log.log(Level.FINE, "finding UserProductDTO instance by example");
		try {
			String hql = "From ProductReserve productreserve where productreserve.userProduct = :userProduct and productreserve.deleted = 0 ";
			Query query = getHibernateSession().createQuery(hql);
			query.setParameter("userProduct", userProduct);
			ArrayList<ProductReserve> productReserves = (ArrayList<ProductReserve>) query.list();
			return productReserves;
		} catch (RuntimeException re) {
			log.log(Level.SEVERE, "find by example failed", re);
			throw re;
		}
	}

	//FIXME
	//why copying the array to set and then convert it to DTOs
	public ArrayList<ProductReserveDTO> findProductReserveDTOByUserProduct(UserProduct userProduct) {
		log.log(Level.FINE, "finding UserProductDTO instance by example");
		try {
			String hql = "From ProductReserve productreserve where productreserve.userProduct = :userProduct and productreserve.deleted = 0 ";
			Query query = getHibernateSession().createQuery(hql);
			query.setParameter("userProduct", userProduct);
			ArrayList<ProductReserve> productReserves = (ArrayList<ProductReserve>) query.list();
			Set<ProductReserve> productReservesSet = new HashSet<>(productReserves);
			ArrayList<ProductReserveDTO> productReserveDTOs = new ArrayList<>(PojoUtil.getDTOListt(productReservesSet));
			return productReserveDTOs;
		} catch (RuntimeException re) {
			log.log(Level.SEVERE, "find by example failed", re);
			throw re;
		}
	}

	public ProductReserve findById(ProductReserveDTO productReserveDTO) {
		log.log(Level.FINE, "finding UserProductDTO instance by example");
		try {
			String hql = "From ProductReserve productreserve where productreserve.id = :id";
			Query query = getHibernateSession().createQuery(hql);
			query.setParameter("id", productReserveDTO.getId());
			ProductReserve productReserve = (ProductReserve) query.uniqueResult();
			return productReserve;
		} catch (RuntimeException re) {
			log.log(Level.SEVERE, "find by example failed", re);
			throw re;
		}
	}

	public ArrayList<ProductReserveDTO> findProductReserveDTOByUserProductAndFriend(User friend,
			UserProduct userProduct) {
		log.log(Level.FINE, "finding UserProductDTO instance by example");
		try {
			String hql = "From ProductReserve productreserve where productreserve.userProduct = :userProduct and productreserve.user= :friend and productreserve.deleted = 0 ";
			Query query = getHibernateSession().createQuery(hql);
			query.setParameter("userProduct", userProduct);
			query.setParameter("friend", friend);
			ArrayList<ProductReserve> productReserves = (ArrayList<ProductReserve>) query.list();
			Set<ProductReserve> productReservesSet = new HashSet<>(productReserves);
			ArrayList<ProductReserveDTO> productReserveDTOs = new ArrayList<>(PojoUtil.getDTOListt(productReservesSet));
			return productReserveDTOs;
		} catch (RuntimeException re) {
			log.log(Level.SEVERE, "find by example failed", re);
			throw re;
		}
	}
}
