/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.ejb;

import eg.gov.iti.jets.iwish.dao.EventCommentsDAO;
import eg.gov.iti.jets.iwish.dao.UserDAO;
import eg.gov.iti.jets.iwish.dao.UserEventDAO;
import eg.gov.iti.jets.iwish.delegate.NotificationDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.EventCommentsDTO;
import eg.gov.iti.jets.iwish.dto.NotificationDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;
import eg.gov.iti.jets.iwish.interceptor.LoggerInterceptor;
import eg.gov.iti.jets.iwish.pojo.EventComments;
import eg.gov.iti.jets.iwish.pojo.User;
import eg.gov.iti.jets.iwish.pojo.UserEvent;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.ArrayList;

/**
 *
 * @author Hossam Abdallah
 */
@Stateless(mappedName = "SendGreetingEjb")
@LocalBean
@Interceptors(LoggerInterceptor.class)
public class SendGreetingEjb {

    @EJB
    EventCommentsDAO eventCommentsDAO;

    @EJB
    UserEventDAO userEventDAO;

    @EJB
    UserDAO userDAO;

    @EJB
    UserEjb userEjb;

    public void addGreetingToEvent(String greeting, UserEventDTO userEventDTO, UserDTO userDTO) {
        System.out.println(userEventDTO.getId());
        UserEvent userEvent = userEventDAO.findEntityById(userEventDTO.getId());
        
        User user = userDAO.findEntityById(userEventDTO.getUserDTO().getId());
        User sender = userDAO.findUserById(userDTO.getId());
        EventComments eventComments = new EventComments(userEvent, greeting, 0, sender);
        eventCommentsDAO.persist(eventComments);
        NotificationDelegate notificationDelegate = DelegateFactory.getNotificationDelegateInstance();
        NotificationDTO notification = new NotificationDTO();
        notification.setEventcomment(PojoUtil.getDTO(eventComments));
        notification.setUserDTO(PojoUtil.getDTO(user));
        notification.setUserEvent(PojoUtil.getDTO(userEvent));
        notification.setTitle(userEvent.getName());
        notification.setDetail(eventComments.getUser().getName() + " sent you a greeting");
        notificationDelegate.sendNotification(notification);

    }

    public ArrayList<EventCommentsDTO> getCommentsOnEvent(UserEventDTO userEventDTO) {
        if (userEventDTO == null) {
            return null;
        }
        UserEvent userEvent = userEventDAO.findEntityById(userEventDTO.getId());
        return eventCommentsDAO.getCommentsOnEvent(userEvent);
    }

}
