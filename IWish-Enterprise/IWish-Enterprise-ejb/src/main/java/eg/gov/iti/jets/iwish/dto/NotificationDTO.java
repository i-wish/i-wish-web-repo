package eg.gov.iti.jets.iwish.dto;

import eg.gov.iti.jets.iwish.pojo.Notification;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import java.util.Date;

/**
 * Created by eltntawy on 15/05/15.
 */
public class NotificationDTO extends AbstractDTO implements java.io.Serializable {

    private int id;
    private String title;
    private String detail;
    private byte[] picture;
    private boolean isDelivered;
    private Date receiveTime;
    private UserDTO userDTO;
    private String url;
    private EventCommentsDTO eventcomment;
    private UserEventDTO userEvent;

    public NotificationDTO() {
    }

    public NotificationDTO(Notification notification) {

        this.id = notification.getId();
        this.title = notification.getTitle();
        this.detail = notification.getDetail();
        this.picture = notification.getPicture();
        this.isDelivered = notification.isDelivered();
        this.receiveTime = notification.getReceiveTime();
        this.userDTO = PojoUtil.getDTO(notification.getUser());
        this.url = notification.getUrl();
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detial) {
        this.detail = detial;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] pic) {
        this.picture = pic;
    }

    public boolean isDelivered() {
        return isDelivered;
    }

    public void setDelivered(boolean isDelivered) {
        this.isDelivered = isDelivered;
    }

    public Date getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(Date receiveTime) {
        this.receiveTime = receiveTime;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public EventCommentsDTO getEventcomment() {
        return eventcomment;
    }

    public void setEventcomment(EventCommentsDTO eventcomment) {
        this.eventcomment = eventcomment;
    }

    public UserEventDTO getUserEvent() {
        return userEvent;
    }

    public void setUserEvent(UserEventDTO userEvent) {
        this.userEvent = userEvent;
    }

    
    @Override
    public String getValueString() {
        // TODO Auto-generated method stub
        return getTitle();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NotificationDTO that = (NotificationDTO) o;

        if (id != that.id) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
