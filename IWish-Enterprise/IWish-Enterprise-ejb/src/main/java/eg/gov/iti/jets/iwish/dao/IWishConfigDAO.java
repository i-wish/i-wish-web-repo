/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eg.gov.iti.jets.iwish.dao;

import eg.gov.iti.jets.iwish.dto.IWishConfigDTO;
import eg.gov.iti.jets.iwish.pojo.IWishConfig;

import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author Hossam Abdallah
 */
@Stateless
@LocalBean
public class IWishConfigDAO extends AbstractDAO<IWishConfig, IWishConfigDTO> {
	private static final Logger log = Logger.getLogger(IWishConfigDAO.class.getName());
	
    public IWishConfigDAO() {
        super(IWishConfig.class);
    }

    @Override
    public IWishConfig getEntity(IWishConfig t, IWishConfigDTO e) {
        t.setConfigKey(e.getConfigKey());
        t.setConfigValue(e.getConfigValue());
        t.setId(e.getId());
        return t;
    }
    
}
