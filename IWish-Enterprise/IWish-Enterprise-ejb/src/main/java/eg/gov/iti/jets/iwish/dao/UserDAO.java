package eg.gov.iti.jets.iwish.dao;

// Generated Mar 31, 2015 5:37:08 PM by Hibernate Tools 4.0.0

import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.exception.IWishCommitException;
import eg.gov.iti.jets.iwish.pojo.FriendList;
import eg.gov.iti.jets.iwish.pojo.User;
import eg.gov.iti.jets.iwish.util.PojoUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Home object for domain model class User.
 * 
 * @see eg.gov.iti.jets.iwish.pojo.User
 * @author Hibernate Tools
 */
@Stateless
@LocalBean
public class UserDAO extends AbstractDAO<User, UserDTO> {
	private static final Logger log = Logger.getLogger(UserDAO.class.getName());

	@Inject
	FriendListDAO friendListDAO;

	public UserDAO() {
		super(User.class);
	}

	@Override
	public User getEntity(User t, UserDTO e) {
		t.setName(e.getName());
		t.setBirthday(e.getBirthday());
		t.setEmail(e.getEmail());
		t.setPassword(e.getPassword());
		t.setPicture(e.getPicture());
		t.setLastName(e.getLastName());
		t.setFirstName(e.getFirstName());

		return t;
	}

	// FIXME
	// why creating it one more time while it is found on AbstractDAO
	public User findUserById(java.lang.Integer id) {
		log.log(Level.FINE, "getting User instance with id: " + id);
		try {
			User instance = (User) getHibernateSession().get("eg.gov.iti.jets.iwish.pojo.User", id);
			if (instance == null) {
				log.log(Level.FINE, "get successful, no instance found");
			} else {
				log.log(Level.FINE, "get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.log(Level.SEVERE, "get failed", re);
			throw re;
		}
	}

	public UserDTO findByFacebookId(String facebookId) {
		log.log(Level.FINE, "finding User instance by example");
		try {
			Query query = getHibernateSession().createQuery("From User where facebookUserId = :facebookId");
			query.setString("facebookId", facebookId);

			User results = (User) query.uniqueResult();
			if (results != null)
				return PojoUtil.getDTO(results);
			else
				return null;
		} catch (RuntimeException re) {
			log.log(Level.SEVERE, "find by example failed", re);
			throw re;
		}
	}

	public UserDTO findByEmail(String email) {
		log.log(Level.FINE, "finding User instance by example");
		try {
			Query query = getHibernateSession().createQuery("From User where email = :email");
			query.setString("email", email);

			User results = (User) query.uniqueResult();
			if (results != null)
				return PojoUtil.getDTO(results);
			else
				return null;
		} catch (RuntimeException re) {
			log.log(Level.SEVERE, "find by example failed", re);
			throw re;
		}
	}

	public User findByEmail(UserDTO user) {
		String hqlQuery = "From User u where u.email = :email";
		Query query = getHibernateSession().createQuery(hqlQuery);
		User object = (User) query.setParameter("email", user.getEmail()).uniqueResult();
		return object;
	}

	public ArrayList<UserDTO> getFriends(UserDTO userdto) {
		User user = this.findByEmail(userdto);

		ArrayList<UserDTO> friends = new ArrayList<UserDTO>();
		String hqlQuery = "From FriendList f where f.userByUserId = :user";
		Query query = getHibernateSession().createQuery(hqlQuery);
		List result = query.setParameter("user", user).list();
		for (Object obj : result) {
			if (obj instanceof FriendList) {
				FriendList friendshipInstance = (FriendList) obj;
				UserDTO friend = PojoUtil.getDTO(friendshipInstance.getUserByFriendId());
				friends.add(friend);
			}
		}
		return friends;
	}

	// FIXME
	// fix names of methods
	public void saveOrUpdateUserFriend(UserDTO userDTO, UserDTO friend) {
		try {
			// find friend if it exist
			User userFriend = (User) getHibernateSession().createCriteria(User.class)
					.add(Restrictions.eq("facebookUserId", friend.getFacebookUserId())).uniqueResult();
			if (userFriend == null) {
				addFriendThatDoesNotExist(userDTO, friend);
			} else {
				addFriendThatDoesExist(userDTO, friend, userFriend);
			}
		} catch (RuntimeException re) {
			log.log(Level.SEVERE, "error while save user friend");
			throw new IWishCommitException(re.getMessage());
		}

	}

	// FIXME
	// fix my name
	// check my code
	public void addFriendThatDoesNotExist(UserDTO userDTO, UserDTO friend) {
		User newUserFriend = PojoUtil.getPojo(friend);
		if (newUserFriend.getBirthday() == null) {
			newUserFriend.setBirthday(null);
		}
		persist(newUserFriend);

		User currentUser = findEntityById(userDTO.getId());
		FriendList friendList = new FriendList(currentUser, newUserFriend);
		friendListDAO.persist(friendList);
	}

	// FIXME
	// fix my name
	// check my code
	public void addFriendThatDoesExist(UserDTO userDTO, UserDTO friend, User userFriend) {
		if (friend.getPicture() != null) {
			userFriend.setPicture(friend.getPicture());
			persist(userFriend);
		}
		User currentUser = findEntityById(userDTO.getId());
		friend = PojoUtil.getDTO(userFriend);
		FriendList friendList = new FriendList(currentUser, userFriend);
		boolean isFriends = friendListDAO.isFriends(userDTO, friend);
		if (!isFriends) {
			friendListDAO.persist(friendList);
			if (userFriend.getPicture() != null) {
				userFriend.setPicture(friend.getPicture());
			}
			persist(userFriend);
		}
	}
}
