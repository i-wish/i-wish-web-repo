package eg.gov.iti.jets.iwish.ejb;

import eg.gov.iti.jets.iwish.dao.ProductDAO;
import eg.gov.iti.jets.iwish.dao.ProductReserveDAO;
import eg.gov.iti.jets.iwish.dao.UserDAO;
import eg.gov.iti.jets.iwish.dao.UserProductDAO;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.ProductReserveDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserProductDTO;
import eg.gov.iti.jets.iwish.interceptor.LoggerInterceptor;
import eg.gov.iti.jets.iwish.pojo.Product;
import eg.gov.iti.jets.iwish.pojo.ProductReserve;
import eg.gov.iti.jets.iwish.pojo.User;
import eg.gov.iti.jets.iwish.pojo.UserProduct;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.util.ArrayList;

/**
 * Created by Hossam Abdallah o
 *
 * @LocalBeann 4/28/2015.
 */
@Stateless(mappedName = "WishListEjb")
@LocalBean
@Interceptors(LoggerInterceptor.class)
public class WishListEjb {

    @Inject
    UserProductDAO userProductDAO;

    @Inject
    UserDAO userDAO;

    @Inject
    ProductDAO productDAO;

    @Inject
    ProductReserveDAO productReserveDAO;

    public boolean addProducts(ArrayList<Integer> products, String email) {
        for (Integer productId : products) {

            UserProduct userProduct = new UserProduct();

            ProductDTO product = new ProductDTO();
            product.setId(productId);

            Product productObject = (Product) productDAO.findById(product);

            int x = productObject.getId();
            userProduct.setProduct(productObject);

            UserDTO user = new UserDTO();
            user.setEmail(email);
            User userObject = userDAO.findByEmail(user);
            userProduct.setUser(userObject);
            UserProductDTO existUserProduct = userProductDAO.findByUserProduct(userObject , productObject);
            if(existUserProduct == null){
                userProductDAO.persist(userProduct);
                return true;
            }else{
                return false;
            }
        }
        return false;           
    }




    public void addProducts(ArrayList<ProductDTO> products, UserDTO user) {
        for (ProductDTO product : products) {

            UserProduct userProduct = new UserProduct();

            Product productObject = productDAO.findById(product);
            int x = productObject.getId();
            userProduct.setProduct(productObject);

            User userObject = userDAO.findByEmail(user);
            userProduct.setUser(userObject);
            userProduct.setDeleted(false);

            if(userProductDAO.isFoundBefore(product,user)){
                userProductDAO.reActivateUserProductByUserAndProduct(product,user);
            }else{
                userProductDAO.persist(userProduct);
            }


        }
    }

    public void deleteProducts(ArrayList<ProductDTO> products, UserDTO user) {
        for (ProductDTO product : products) {

            UserProduct userProduct = new UserProduct();
            Product productObject = productDAO.findById(product);

            userProduct.setProduct(productObject);

            User userObject = userDAO.findEntityByExample(PojoUtil.getPojo(user)).get(0);
            userProduct.setUser(userObject);
            UserProductDTO userProductDTO = userProductDAO.findByUserProduct(userObject, productObject);
            userProductDAO.deleteUserProduct(userProductDTO);
        }

    }

    public ArrayList<ProductDTO> getFriendWishlist(UserDTO friend) {
        if (friend != null) {
            return (ArrayList<ProductDTO>) productDAO.getUserProductsList(friend);
        } else {
            return null;
        }
    }

    public ArrayList<UserProductDTO> getFriendWishlistWithDetails(UserDTO friend) {
        if (friend != null) {
            return (ArrayList<UserProductDTO>) productDAO.getUserProductsDTOList(friend);
        } else {
            return null;
        }
    }

    public void deleteGift(UserProductDTO userProduct) {
        Product product = PojoUtil.getPojo(userProduct.getProductDTO());
        User user = PojoUtil.getPojo(userProduct.getUserDTO());
        UserProductDTO userPro = userProductDAO.findByUserProduct(user, product);
        userPro.setReceived(1);
        userProductDAO.saveOrUpdate(userPro);
    }

    public boolean reserveGift(UserDTO friendDTO, UserProductDTO userProductDTO, String comment) {

        try {
            User friend = userDAO.findEntityById(friendDTO.getId());
            UserProduct userProduct = userProductDAO.findEntityById(userProductDTO.getId());
            userProduct.setProductReserves(null);

            ProductReserve productReserve = new ProductReserve(friend, userProduct, comment);

            productReserve.setParticipate(100d);
            productReserveDAO.persist(productReserve);
        } catch (RuntimeException ex) {
            return false;
        }
        return true;

    }

    public UserProductDTO getUserProductDTO(UserDTO userDTO, ProductDTO productDTO) {

        User user = userDAO.findUserById(userDTO.getId());
        Product product = productDAO.findById(productDTO);
        return userProductDAO.findByUserProduct(user, product);
    }

    public ArrayList<UserProductDTO> getUserProductDTOs(UserDTO userDTO) {
        User user = userDAO.findUserById(userDTO.getId());
        return userProductDAO.findByUser(user);
    }

    public ArrayList<UserDTO> getReservingFriends(UserProductDTO userProductDTO) {
        UserProduct userProduct = userProductDAO.findById(userProductDTO);
        ArrayList<ProductReserve> productReserves = productReserveDAO.findByUserProduct(userProduct);
        ArrayList<UserDTO> userDTOs = new ArrayList<>();
        for (ProductReserve productReserve : productReserves) {
            UserDTO userDTO = userDAO.findById(productReserve.getUser().getId());
            userDTOs.add(userDTO);
        }
        return userDTOs;
    }

    public ProductDTO getWishlistProduct(UserProductDTO userProductDTO) {
        UserProduct userProduct = userProductDAO.findById(userProductDTO);
        ProductDTO productDTO = userProductDAO.findByUserProduct(userProduct);
        return productDTO;
    }

    public boolean reserveGiftWithOthers(UserDTO friendDTO, UserProductDTO userProductDTO, String comment, double participate) {
        try {
            User friend = userDAO.findEntityById(friendDTO.getId());
            UserProduct userProduct = userProductDAO.findEntityById(userProductDTO.getId());
            ProductReserve productReserve = new ProductReserve(friend, userProduct, comment, participate);
            productReserveDAO.persist(productReserve);
            return true;
        } catch (RuntimeException ex) {
            return false;
        }
    }

    public ArrayList<ProductReserveDTO> getProductReserves(UserProductDTO userProductDTO) {
        UserProduct userProduct = userProductDAO.findById(userProductDTO);
        ArrayList<ProductReserveDTO> productReserveDTOs = productReserveDAO.findProductReserveDTOByUserProduct(userProduct);
        return productReserveDTOs;
    }

    public UserDTO getUserReservedProduct(ProductReserveDTO productReserveDTO) {

        ProductReserve productReserve = productReserveDAO.findById(productReserveDTO);
        UserDTO userDTO = userDAO.findById(productReserve.getUser().getId());
        return userDTO;
    }
    public boolean isReservedBefore(UserDTO friend,UserProductDTO userProductDTO){
       try{
           ArrayList<ProductReserveDTO> productReserveDTOs = productReserveDAO.findProductReserveDTOByUserProductAndFriend(PojoUtil.getPojo(friend),PojoUtil.getPojo(userProductDTO));
        if(productReserveDTOs.size()!=0){

            return true;
        }else
        {
            return false;
        }
       }catch (NullPointerException ex){
           return  false;
       }
    }
    public boolean deleteReservation(UserDTO friendDTO, UserProductDTO userProductDTO){
        try {
            User friend = userDAO.findEntityById(friendDTO.getId());
            UserProduct userProduct = userProductDAO.findEntityById(userProductDTO.getId());
            ArrayList<ProductReserveDTO> productReserveDTOs = productReserveDAO.findProductReserveDTOByUserProductAndFriend(friend, userProduct);
            for(ProductReserveDTO product:productReserveDTOs){
                ProductReserve productReserve=productReserveDAO.findById(product);
                productReserveDAO.delete(productReserve);
            }
            return true;
        } catch (RuntimeException ex) {
            return false;
        }

    }
}
