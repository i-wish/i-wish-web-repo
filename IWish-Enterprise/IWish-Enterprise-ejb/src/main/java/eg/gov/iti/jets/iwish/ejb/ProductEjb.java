/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.ejb;

import eg.gov.iti.jets.iwish.dao.CategoryDAO;
import eg.gov.iti.jets.iwish.dao.CompanyDAO;
import eg.gov.iti.jets.iwish.dao.ProductDAO;
import eg.gov.iti.jets.iwish.dto.CategoryDTO;
import eg.gov.iti.jets.iwish.dto.CompanyDTO;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.StoreOwnerDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserProductDTO;
import eg.gov.iti.jets.iwish.interceptor.LoggerInterceptor;
import eg.gov.iti.jets.iwish.pojo.Product;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.SystemException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Hossam Abdallah
 */
@Stateless(mappedName = "ProductEjb")
@LocalBean
@Interceptors(LoggerInterceptor.class)
public class ProductEjb {

	@EJB
	ProductDAO productDAO;

	@EJB
	ParseSouq parseSouq;

	@EJB
	ProductMapsEjb productMapsEjb;

	@Inject
	CategoryDAO categoryDAO;
	
	@Inject
    CompanyDAO companyDAO;

	public ArrayList<ProductDTO> getProducts() {
		ArrayList<ProductDTO> list = (ArrayList<ProductDTO>) productDAO.getAll();
		return list;
	}

	public List<ProductDTO> getUserProductsList(UserDTO user) {
		return (ArrayList<ProductDTO>) productDAO.getUserProductsList(user);
	}

	public void remove(ProductDTO productDTO) {
		Product product = productDAO.findById(productDTO);
		productDAO.delete(product);
	}

	public void updateProduct(ProductDTO productDTO) {
		productDAO.saveOrUpdate(productDTO);
	}

	public void synchProduct() {
		try {
			parseSouq.getProducts();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}

	public ProductDTO getProductImage(ProductDTO productDTO) {
		Product product = productDAO.findById(productDTO);
		return PojoUtil.getDTO(product);
	}

	public Long getProductCount() {

		return productDAO.getProductsCount();

	}

	public Map<String, Integer> getCompanyProductsMap() {

		Map<String, Integer> companyProductsMap = new HashMap<>();

		List<CompanyDTO> companyDTOList = productMapsEjb.getAllCompanies();
		List<CategoryDTO> categoryDTOList = productMapsEjb.getAllCategories();

		for (CompanyDTO companyDTO : companyDTOList) {
			int count = 0;
			for (CategoryDTO categoryDTO : categoryDTOList) {

				count += productMapsEjb.getAllProductsCount(companyDTO, categoryDTO);
			}
			companyProductsMap.put(companyDTO.getName(), count);
		}

		return companyProductsMap;
	}

	/**
	 * @author Hossam ElDeen
	 */
	public void addProduct(ProductDTO productDTO, CompanyDTO companyDTO, CategoryDTO categoryDTO) {
		if(productDTO==null)
			throw  new IllegalArgumentException("productDTO can not be null");
		if(companyDTO == null)
			throw new IllegalArgumentException("companyDTO can not be null");
		if(categoryDTO == null)
			throw new IllegalArgumentException("categoryDTO can not be null");
		
		productDTO.setCompanyDTO(companyDTO);
		productDTO.setCategoryDTO(categoryDTO);
		Product product = PojoUtil.getPojo(productDTO);
		productDAO.persist(product);
		productMapsEjb.fillAllProducts();
	}

	/**
	 * @author Hossam ElDeen
	 */
	public List<ProductDTO> getProducts(StoreOwnerDTO storeOwnerDTO) {
		if (storeOwnerDTO == null)
			throw new IllegalArgumentException("storeOwnerDTO can not be null");

		return productDAO.findStoreOwnerProducts(storeOwnerDTO);
	}

	/**
	 * @author Hossam ElDeen
	 */
	public CategoryDTO getCategoryById(int id) {
		categoryDAO.getHibernateSession().beginTransaction();
		CategoryDTO categoryDTO = categoryDAO.findById(id);
		categoryDAO.getHibernateSession().getTransaction().commit();
		return categoryDTO;
	}

	/**
	 * @author Hossam ElDeen
	 */
	public CompanyDTO getCompanyById(int id){
        companyDAO.getHibernateSession().beginTransaction();
        CompanyDTO companyDTO = companyDAO.findById(id);
        companyDAO.getHibernateSession().getTransaction().commit();

        return companyDTO;
    }
	
	/**
	 * @author Hossam ElDeen
	 */
	public List<UserProductDTO> getUserProductsDTOsList(UserDTO user) {
		return (ArrayList<UserProductDTO>) productDAO.getUserProductsDTOList(user);
	}
}
