package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.PlaceDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;

import java.util.ArrayList;
import java.util.List;

public interface EventDelegate {

    public void addEvent(UserEventDTO event);

    public void updateEvent(UserEventDTO event);

    public void removeEvent(UserEventDTO event);

    public List<UserEventDTO> getEvents(UserDTO user);

    public List<EventTypeDTO> getEventsType();

    public List<PlaceDTO> getPlaces();

    public ArrayList<UserEventDTO> getFriendsEvents(UserDTO userDTO);

    public List<UserEventDTO> getFriendEvents(UserDTO user);

    public UserEventDTO getEvent(UserEventDTO event);

    public List<EventTypeDTO> getEventsTypeByName(String eventTypeName);

    public boolean isEventExist(UserDTO user,EventTypeDTO eventTypeDTO);
}
