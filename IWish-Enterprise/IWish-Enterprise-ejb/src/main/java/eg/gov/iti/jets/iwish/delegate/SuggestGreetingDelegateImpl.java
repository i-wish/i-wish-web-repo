/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.GreetingDTO;
import eg.gov.iti.jets.iwish.ejb.SuggestGreetingEjb;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil;

import java.util.ArrayList;

/**
 *
 * @author Hossam Abdallah
 */
public class SuggestGreetingDelegateImpl implements SuggestGreetingDelegate{
    SuggestGreetingEjb suggestGreetingEjb = (SuggestGreetingEjb) ServiceLocatorUtil.getInistance().lookup(ServiceLocatorUtil.ServiceLocatorType.SUGGEST_GREETING_SERVICE);

    @Override
    public ArrayList<GreetingDTO> getGreetings(EventTypeDTO eventTypeDTO) {
        return suggestGreetingEjb.getGreetings(eventTypeDTO);
    }
}
