package eg.gov.iti.jets.iwish.ejb;

import eg.gov.iti.jets.iwish.dao.*;
import eg.gov.iti.jets.iwish.dto.*;
import eg.gov.iti.jets.iwish.interceptor.LoggerInterceptor;
import eg.gov.iti.jets.iwish.pojo.User;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hossam Abdallah on 5/4/2015.
 */
@Stateless(mappedName = "UserEjb")
@LocalBean
@Interceptors(LoggerInterceptor.class)
public class UserEjb {

	@Inject
	UserDAO userDAO;
	@Inject
	ProductDAO productDAO;
	@Inject
	CategoryDAO categoryDAO;

	@Inject
	NotificationDAO notificationDAO;

	@Inject
	ProductReserveDAO productReserveDAO;

	@Inject
	CompanyDAO companyDAO;

	public ArrayList<UserDTO> getFriends(UserDTO user) {
		if (user != null) {
			return (ArrayList<UserDTO>) userDAO.getFriends(user);
		} else {
			return null;
		}
	}

	public List<ProductDTO> getUserProductsList(UserDTO user) {
		return (ArrayList<ProductDTO>) productDAO.getUserProductsList(user);
	}

	public UserDTO findUserByEmail(String email) {
		return userDAO.findByEmail(email);
	}

	@Deprecated
	/**
	 * use the one in notificationEjb
	 * @param userDTO
	 * @return
	 */
	public List<NotificationDTO> getNotification(UserDTO userDTO) {
		return notificationDAO.getAllNotification(userDTO);
	}

	@Deprecated
	/**
	 * use the one in notificationEjb
	 * @param userDTO
	 * @return
	 */
	public List<NotificationDTO> getUnReadedNotification(UserDTO userDTO) {
		return notificationDAO.getUnReadedNotification(userDTO);
	}
	
	@Deprecated
	/**
	 * use the one in productEjb
	 * @param user
	 * @return
	 */
	public List<UserProductDTO> getUserProductsDTOsList(UserDTO user) {
		return (ArrayList<UserProductDTO>) productDAO.getUserProductsDTOList(user);
	}

	public UserDTO login(String email, String password) {
		UserDTO userDTO = userDAO.findByEmail(email);
		if (userDTO != null && userDTO.getPassword() != null) {
			if (userDTO.getPassword().equals(password)) {
				return userDTO;
			}
		}
		return null;
	}

	public void saveRegId(UserDTO userDTO, String regId) {
		User user = userDAO.findEntityById(userDTO.getId());
		user.setRegisterationId(regId);
		userDAO.persist(user);
	}

	public UserDTO findUserByFacebookId(String facebookId) {
		return userDAO.findByFacebookId(facebookId);
	}

	public UserDTO findById(int id) {

		User user = userDAO.findUserById(id);
		UserDTO userDTO = PojoUtil.getDTO(user);
		return userDTO;
	}

	public void updateUser(UserDTO user) {
		userDAO.saveOrUpdate(user);
	}

	@Deprecated
	/**
	 * use the method in productEjb
	 * 
	 * @param id
	 * @return
	 */
	public CompanyDTO getCompanyById(int id) {
		companyDAO.getHibernateSession().beginTransaction();
		CompanyDTO companyDTO = companyDAO.findById(id);
		companyDAO.getHibernateSession().getTransaction().commit();

		return companyDTO;
	}

	@Deprecated
	/**
	 * use the method in productEjb
	 * 
	 * @param id
	 * @return
	 */
	public CategoryDTO getCategoryById(int id) {
		categoryDAO.getHibernateSession().beginTransaction();
		CategoryDTO categoryDTO = categoryDAO.findById(id);
		categoryDAO.getHibernateSession().getTransaction().commit();
		return categoryDTO;
	}
}
