/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.ejb;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eg.gov.iti.jets.iwish.dao.NotificationDAO;
import eg.gov.iti.jets.iwish.dao.UserDAO;
import eg.gov.iti.jets.iwish.dao.UserEventDAO;
import eg.gov.iti.jets.iwish.dto.NotificationDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;
import eg.gov.iti.jets.iwish.interceptor.LoggerInterceptor;
import eg.gov.iti.jets.iwish.notification.PushNotification.PushNotificationService;
import eg.gov.iti.jets.iwish.pojo.Notification;
import eg.gov.iti.jets.iwish.pojo.User;
import eg.gov.iti.jets.iwish.pojo.UserEvent;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Maram
 */
@Stateless(mappedName = "PushNotificationEjb")
@LocalBean
@Interceptors(LoggerInterceptor.class)
public class NotificationEjb {
    
    @Inject
    PushNotificationService pushNotificationService ;
    
    @Inject
    NotificationDAO notificationDAO;
    
    @Inject
    UserDAO userDAO;
    
    @Inject
    UserEventDAO userEventDAO;
    
    
    public void sendNotification(String notification , String RegId){
       
        pushNotificationService.sendNotification(notification, RegId);
    }
    
    public void saveNotification(UserEventDTO userEventDTO , UserDTO userDTO){
        
        Notification notification = new Notification();
        
        UserEvent userEvent = userEventDAO.findPojoById(userEventDTO.getId());
        User user = userDAO.findUserById(userDTO.getId());
        notification.setUser(user);
        notification.setTitle(userEventDTO.getName());
        String name = userEvent.getUser().getName();
        notification.setDetail( name + " created event ("+userEvent.getName()+" will be in "+userEvent.getDate()+")");

        notification.setDelivered(false);
        notification.setReceiveTime(new Date());
        notification.setUserEvent(userEvent);
        notificationDAO.persist(notification);
    }
    
    public ArrayList<NotificationDTO> getUserNotification (UserDTO userDTO){
        
        return (ArrayList<NotificationDTO>) notificationDAO.getAllNotification(userDTO);
        
    }
    
    public void confirmReceivingNotification(NotificationDTO notificationDTO){
        notificationDAO.confirmReceivingNotification(notificationDTO);
    }


    public void saveNotification(NotificationDTO notificationDTO){
        Notification notification = PojoUtil.getPojo(notificationDTO);
        notificationDAO.persist(notification);
    }

    public void sendNotification(NotificationDTO notificationDTO) {
        Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
        Notification notification = new Notification();

        User user = userDAO.findUserById(notificationDTO.getUserDTO().getId());

        UserEvent userEvent = userEventDAO.findEntityById(notificationDTO.getUserEvent().getId());

        notification.setUser(user);
        notification.setTitle(notificationDTO.getTitle());
        notification.setDetail(notificationDTO.getDetail());
        notification.setDelivered(false);
        notification.setReceiveTime(new Date());
        notification.setUserEvent(userEvent);
   
        notificationDAO.persist(notification);

    }
    
    
    class notificationJson {
       String message;
       int eventId;
    }
   
    
    /**
     * @author Hossam ElDeen
     */
    public List<NotificationDTO> getNotification(UserDTO userDTO) {
		return notificationDAO.getAllNotification(userDTO);
	}
    
    /**
     * @author Hossam ElDeen
     */
    public List<NotificationDTO> getUnReadedNotification(UserDTO userDTO) {
		return notificationDAO.getUnReadedNotification(userDTO);
	}

}
