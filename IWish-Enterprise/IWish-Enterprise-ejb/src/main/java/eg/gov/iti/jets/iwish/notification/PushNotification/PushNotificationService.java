/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.notification.PushNotification;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Maram
 */
@Singleton
@Lock(LockType.READ)
//FIXME
//need more documentation or to be well structured
public class PushNotificationService {

    private static String Project_Key;
    int offset;

    public PushNotificationService() {
        offset = 0;
        Properties properties = new Properties();
        try {
            InputStream is = getClass().getResourceAsStream("pushNotification.properties");
            properties.load(is);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Get added security by using your app secret:
        Project_Key = properties.getProperty("Project_Key");
    }
    

    public void sendNotification(String notification, String RegId) {

        try {
           
            Properties properties = new Properties();
            Sender sender = new Sender(Project_Key);
            Message message = new Message.Builder()
                    .collapseKey("message")
                    .timeToLive(3)
                    .delayWhileIdle(true)
                    .addData("message" , notification) //you can get this message on client side app
                    .build();

            Result result = sender.send(message, RegId, 5);
            if (result.getCanonicalRegistrationId() != null) {
                System.out.println(result.toString());

            } else {
                String error = result.getErrorCodeName();
                System.out.println("Broadcast failure: " + error);

            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
