package eg.gov.iti.jets.iwish.dao;

// Generated Mar 31, 2015 5:37:08 PM by Hibernate Tools 4.0.0

import eg.gov.iti.jets.iwish.dto.PlaceDTO;
import eg.gov.iti.jets.iwish.pojo.Place;
import eg.gov.iti.jets.iwish.util.PojoUtil;
import org.hibernate.Query;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.logging.Logger;



/**
 * Home object for domain model class Place.
 * @see eg.gov.iti.jets.iwish.pojo.Place
 * @author Hibernate Tools
 */
@Stateless
@LocalBean
public class PlaceDAO extends AbstractDAO<Place, PlaceDTO>  {
	private static final Logger log = Logger.getLogger(PlaceDAO.class.getName());

	public PlaceDAO(){
		super(Place.class);		
	}

	@Override
	public Place getEntity(Place t, PlaceDTO e) {
		t.setAddress(e.getAddress());
		t.setDescription(e.getDescription());
		t.setName(e.getName());
		t.setTelNo(e.getTelNo());
		t.setWebSite(e.getWebSite());
		t.setId(e.getId());
		t.setGooglePlaceId(e.getGooglePlaceId());
		return t;
	}

    public Place getPlaceByGooglePlaceId (String googlePlaceId){
        String hql = "From Place place where place.googlePlaceId = :id";
        Query query = getHibernateSession().createQuery(hql);
        query.setParameter("id", googlePlaceId);
        Place place = (Place) query.uniqueResult();
        return place;
    }

}
