/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.NotificationDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Maram
 */
public interface NotificationDelegate {

	public void sendNotification(String notification, String RegId);

	public void sendNotification(UserEventDTO userEventDTO);

	public void sendNotificationUser(UserEventDTO userEventDTO, UserDTO userDTO);

	public void sendNotification(NotificationDTO notification);

	public ArrayList<NotificationDTO> getUserNotifications(UserDTO userDTO);

	public void confirmReceivingNotification(NotificationDTO notificationDTO);
	
	public List<NotificationDTO> getAllNotification(UserDTO userDTO);
	
	public List<NotificationDTO> getUnReadedUserNotification(UserDTO userDTO);
}
