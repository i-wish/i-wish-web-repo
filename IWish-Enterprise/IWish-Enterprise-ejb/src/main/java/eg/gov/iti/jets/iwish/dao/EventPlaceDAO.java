package eg.gov.iti.jets.iwish.dao;

// Generated Mar 31, 2015 5:37:08 PM by Hibernate Tools 4.0.0

import eg.gov.iti.jets.iwish.dto.EventPlaceDTO;
import eg.gov.iti.jets.iwish.pojo.EventPlace;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.logging.Logger;

/**
 * Home object for domain model class EventPlace.
 * @see eg.gov.iti.jets.iwish.pojo.EventPlace
 * @author Hibernate Tools
 */
@Stateless
@LocalBean
public class EventPlaceDAO extends AbstractDAO<EventPlace, EventPlaceDTO>  {
	private static final Logger log = Logger.getLogger(EventPlaceDAO.class.getName());

	public EventPlaceDAO(){
		super(EventPlace.class);
	}

	@Override
	public EventPlace getEntity(EventPlace t, EventPlaceDTO e) {
		t.setPlace(PojoUtil.getPojo(e.getPlaceDTO()));
		t.setEventType(PojoUtil.getPojo(e.getEventTypeDTO()));
		return t;
	}
}
