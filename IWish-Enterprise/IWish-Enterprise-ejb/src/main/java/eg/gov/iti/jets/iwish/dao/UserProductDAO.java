package eg.gov.iti.jets.iwish.dao;

// Generated Mar 31, 2015 5:37:08 PM by Hibernate Tools 4.0.0
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserProductDTO;
import eg.gov.iti.jets.iwish.pojo.Product;
import eg.gov.iti.jets.iwish.pojo.User;
import eg.gov.iti.jets.iwish.pojo.UserProduct;
import eg.gov.iti.jets.iwish.util.PojoUtil;
import org.hibernate.Query;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Home object for domain model class UserProduct.
 *
 * @see eg.gov.iti.jets.iwish.pojo.UserProduct
 * @author Hibernate Tools
 */
@Stateless
@LocalBean
public class UserProductDAO extends AbstractDAO<UserProduct, UserProductDTO> {
    private static final Logger log = Logger.getLogger(UserProductDAO.class.getName());

    public UserProductDAO() {
        super(UserProduct.class);
    }

    @Override
    public UserProduct getEntity(UserProduct t, UserProductDTO e) {
        t.setId(e.getId());
        t.setProduct(PojoUtil.getPojo(e.getProductDTO()));
        t.setReceived(1);
        t.setUser(PojoUtil.getPojo(e.getUserDTO()));
        t.setDeleted(e.isDeleted());
        return t;
    }

    //FIXME
    //why to check with objects not ids
    public boolean isFoundBefore(ProductDTO productDTO , UserDTO userDTO){
        log.log(Level.FINE, "finding UserProductDTO instance by example");
        try {
            String hql = "From UserProduct userproduct where userproduct.user = :user and userproduct.product = :product";
            Query query = getHibernateSession().createQuery(hql);
            query.setParameter("user", PojoUtil.getPojo(userDTO));
            query.setParameter("product", PojoUtil.getPojo(productDTO));

            UserProduct result = (UserProduct) query.uniqueResult();
            if(result == null)
                return false;
            else
                return true;
        } catch (RuntimeException re) {
            log.log(Level.SEVERE, "find by example failed", re);
            throw re;
        }
    }

    public void reActivateUserProductByUserAndProduct(ProductDTO productDTO, UserDTO userDTO){
        log.log(Level.FINE, "finding UserProductDTO instance by example");
        try {
            String hql = "update UserProduct userproduct set userproduct.deleted = 0 , userproduct.received = 0 where userproduct.user = :user and userproduct.product = :product";
            Query query = getHibernateSession().createQuery(hql);
            query.setParameter("user", PojoUtil.getPojo(userDTO));
            query.setParameter("product", PojoUtil.getPojo(productDTO));

            query.executeUpdate();
        } catch (RuntimeException re) {
            log.log(Level.SEVERE, "find by example failed", re);
            throw re;
        }
    }
    
    public UserProductDTO findByUserProduct(User user, Product product) {
        log.log(Level.FINE, "finding UserProductDTO instance by example");
        try {
            String hql = "From UserProduct userproduct where userproduct.user = :user and userproduct.product = :product";
            Query query = getHibernateSession().createQuery(hql);
            query.setParameter("user", user);
            query.setParameter("product", product);

            UserProduct result = (UserProduct) query.uniqueResult();
            UserProductDTO userProductDTO = PojoUtil.getDTO(result);
            return userProductDTO;
        } catch (RuntimeException re) {
            log.log(Level.SEVERE, "find by example failed", re);
            throw re;
        }

    }

    public ArrayList<UserProductDTO> findByUser(User user) {
        log.log(Level.FINE, "finding UserProductDTO instance by example");
        try {

            String hql = "From UserProduct userproduct where userproduct.user = :user";
            Query query = getHibernateSession().createQuery(hql);
            query.setParameter("user", user);
            ArrayList<UserProduct> userProducts = (ArrayList<UserProduct>) query.list();
            ArrayList<UserProductDTO> userProductDTOs = PojoUtil.getDTOList(userProducts);
            return userProductDTOs;

        } catch (RuntimeException re) {
            log.log(Level.SEVERE, "find by example failed", re);
            throw re;
        }
    }

    public void Update(UserProductDTO userProduct) {
        UserProduct userPro = PojoUtil.getPojo(userProduct);
        getHibernateSession().saveOrUpdate(userPro);
    }

    public UserProduct findById(UserProductDTO userProductDTO) {
        log.log(Level.FINE, "finding UserProductDTO instance by example");
        try {

            String hql = "From UserProduct userproduct where userproduct.id = :id";
            Query query = getHibernateSession().createQuery(hql);
            query.setParameter("id", userProductDTO.getId());
            UserProduct userProduct = (UserProduct) query.uniqueResult();
            return userProduct;

        } catch (RuntimeException re) {
            log.log(Level.SEVERE, "find by example failed", re);
            throw re;
        }
    }

    public ProductDTO findByUserProduct(UserProduct userProduct) {
        log.log(Level.FINE, "finding UserProductDTO instance by example");
        try {

            String hql = "Select userproduct.product From UserProduct userproduct where userproduct.id = :id";
            Query query = getHibernateSession().createQuery(hql);
            query.setParameter("id", userProduct.getId());
            Product product = (Product) query.uniqueResult();
            ProductDTO productDTO = PojoUtil.getDTO(product);
            return productDTO;

        } catch (RuntimeException re) {
            log.log(Level.SEVERE, "find by example failed", re);
            throw re;
        }
    }

    public void deleteUserProduct(UserProductDTO persistentInstance) {
        log.log(Level.FINE, "deleting UserProduct instance");
        try {
            UserProduct userProduct=PojoUtil.getPojo(persistentInstance);
            deleteUserProductEntry(userProduct);
            deleteProductReserveEntry(userProduct);
        } catch (RuntimeException re) {
            log.log(Level.SEVERE, "delete User Product failed", re);
            throw re;
        }

    }
    
    private void deleteUserProductEntry(UserProduct userProduct){
    	String queryStatement = "update UserProduct userproduct set userproduct.deleted = 1 where userproduct.id = :id";
        Query query = getHibernateSession().createQuery(queryStatement);
        query.setParameter("id", userProduct.getId());
        query.executeUpdate();
    }
    
    private void deleteProductReserveEntry(UserProduct userProduct){
    	String queryStatement = "update   ProductReserve productReserve set productReserve.deleted = 1 where productReserve.userProduct = :product";
    	Query query = getHibernateSession().createQuery(queryStatement);
        query.setParameter("product", userProduct);
        query.executeUpdate();
    }

}
