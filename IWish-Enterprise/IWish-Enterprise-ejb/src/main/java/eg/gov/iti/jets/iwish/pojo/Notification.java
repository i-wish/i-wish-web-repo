package eg.gov.iti.jets.iwish.pojo;

import eg.gov.iti.jets.iwish.listeners.NotificationListener;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created by eltntawy on 15/05/15.
 */
@Entity
@Table(name = "Notification", catalog = "I_Wish_DB")
@XmlRootElement
@EntityListeners(NotificationListener.class)
public class Notification extends AbstractPojo implements java.io.Serializable {

    private int id;
    private String title;
    private String detail;
    private byte[] picture;
    private boolean isDelivered;
    private Date receiveTime;
    private User user;
    private String url;
    private EventComments eventComment;
    private UserEvent userEvent;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "detail")
    public String getDetail() {
        return detail;
    }

    public void setDetail(String detial) {
        this.detail = detial;
    }

    @Column(name = "picture", columnDefinition = "longblob")
    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] pic) {
        this.picture = pic;
    }

    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "is_deliverd")
    public boolean isDelivered() {
        return isDelivered;
    }

    public void setDelivered(boolean isDelivered) {
        this.isDelivered = isDelivered;
    }

    @Column(name = "receive_time")
    public Date getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(Date receiveTime) {
        this.receiveTime = receiveTime;
    }

    @OneToOne(fetch = FetchType.EAGER, targetEntity = User.class)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    

    public void setEventComment(EventComments eventComment) {
        this.eventComment = eventComment;
    }

     @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "eventComment_id", nullable = false)
    public EventComments getEventComment() {
        return eventComment;
    }

     @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userEvent_id", nullable = false)
    public UserEvent getUserEvent() {
        return userEvent;
    }

    public void setUserEvent(UserEvent userEvent) {
        this.userEvent = userEvent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Notification that = (Notification) o;

        if (id != that.id) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
