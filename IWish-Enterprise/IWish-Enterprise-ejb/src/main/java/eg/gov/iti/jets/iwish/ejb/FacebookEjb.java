package eg.gov.iti.jets.iwish.ejb;

import com.restfb.exception.FacebookException;
import eg.gov.iti.jets.iwish.dao.UserDAO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.facebook.FacebookService;
import eg.gov.iti.jets.iwish.facebook.type.FacebookPost;
import eg.gov.iti.jets.iwish.interceptor.LoggerInterceptor;
import eg.gov.iti.jets.iwish.pojo.User;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.util.List;

/**
 * Created by eltntawy on 22/04/15.
 */
@Stateless(mappedName = "FacebookEjb")
@LocalBean
@Interceptors(LoggerInterceptor.class)
public class FacebookEjb {

    @Inject
    UserDAO userDAO;

    @Inject
    SuggestGreetingEjb suggestGreetingEjb;
    
    @Inject
    FacebookService facebookService ;

    public String getFacebookLoginDialogUrl(String redirectUrl) {
        return facebookService.getLoginDialogUrl(redirectUrl);
    }

    public UserDTO getFacebookUserData(String redirectUrl, String verificationCode) {

        UserDTO userDTO = null;

        try {

            userDTO = facebookService.getFacebookUserData(redirectUrl, verificationCode);

        } catch (FacebookException ex) {
            ex.printStackTrace();
        }

        if (userDTO != null) {
            userDTO = saveNewFacebookUser(userDTO);

            return userDTO;
        } else {
            return null;
        }

    }

    public UserDTO getFacebookUserData(String accessToken) {

        UserDTO userDTO = null;
        userDTO = facebookService.getFacebookUserData(accessToken);

        if (userDTO != null) {
            userDTO = saveNewFacebookUser(userDTO);
        } else {
            throw new RuntimeException("can not get user data from facebook");
        }

        return userDTO;

    }

    private UserDTO saveNewFacebookUser(UserDTO userDTO) {

        UserDTO dbUserDTO = userDAO.findByFacebookId(userDTO.getFacebookUserId());
        
        if (dbUserDTO == null) {
            
            User user = new User();
            user.setFacebookUserId(userDTO.getFacebookUserId());

            user.setFacebookUserAccessToken(userDTO.getFacebookUserAccessToken());
            user.setName(userDTO.getName());
            if(userDTO.getEmail()==null){
                userDTO.setEmail(userDTO.getFacebookUserId()+"@"+"facebook");
            }

            user.setEmail(userDTO.getEmail());
            user.setBirthday(userDTO.getBirthday());
            user.setPassword(userDTO.getFacebookUserAccessToken());
            user.setFirstName(userDTO.getFirstName());
            user.setLastName(userDTO.getLastName());
            user.setPicture(userDTO.getPicture());

            userDAO.persist(user);

            return PojoUtil.getDTO(user);
        } else {

            User user =  userDAO.findEntityById(dbUserDTO.getId());
            user.setFacebookUserAccessToken(userDTO.getFacebookUserAccessToken());

            if(user.getEmail() == null){
                if(userDTO.getEmail() == null ) {
                    userDTO.setEmail(userDTO.getFacebookUserId()+"@"+"facebook");
                } else {
                    user.setEmail(userDTO.getEmail());
                }
            }

            if(user.getPassword() == null) {
                user.setPassword(userDTO.getFacebookUserAccessToken());
            }

            if(userDTO.getPicture() != null) {
                user.setPicture(userDTO.getPicture());
            }

            userDAO.persist(user);
        
            return PojoUtil.getDTO(user);
        }
    }
    
    public void updateUserFriendsList(UserDTO userDTO) {
        
        List<UserDTO> friends = facebookService.getFacebookUserFriends(userDTO);
        
        for(UserDTO friend : friends) {
            userDAO.saveOrUpdateUserFriend(userDTO,friend);
        }
    }
    
     public void postOnFacebook(UserDTO userDTO, FacebookPost post) {
         facebookService.postOnFacebook(userDTO, post);
     }
     
     public void pushFacebookNotification(UserDTO userDTO, String href, String message){
         facebookService.pushFacebookNotification(userDTO, href, message);
     }
     
     
}
