/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.NotificationDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;
import eg.gov.iti.jets.iwish.ejb.NotificationEjb;
import eg.gov.iti.jets.iwish.ejb.UserEjb;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Maram
 */
public class NotificationDelegateImpl implements NotificationDelegate {
    NotificationEjb notificationEjb = (NotificationEjb) ServiceLocatorUtil.getInistance().lookup(ServiceLocatorUtil.ServiceLocatorType.PUSH_NOTIFICATION_SERVICE);
    UserDelegate userDelegate = DelegateFactory.getUserDelegateInstance();

    @Override
    public void sendNotification(String notification, String RegId){
        notificationEjb.sendNotification(notification, RegId);
    }

    @Override
    public void sendNotification(UserEventDTO userEventDTO) {
        UserDTO userDTO = userDelegate.findUserByFacebookId(userEventDTO.getUserDTO().getFacebookUserId());
        ArrayList<UserDTO> userFriends = (ArrayList<UserDTO>) userDelegate.getFriends(userDTO);      
        for (UserDTO friend : userFriends){
            sendNotificationUser(userEventDTO, friend);
        }
    }

    @Override
    public void sendNotificationUser(UserEventDTO userEventDTO, UserDTO userDTO) {
        notificationEjb.saveNotification(userEventDTO, userDTO);
    }

    @Override
    public ArrayList<NotificationDTO> getUserNotifications(UserDTO userDTO) {
        return notificationEjb.getUserNotification(userDTO);
    }

    @Override
    public void confirmReceivingNotification(NotificationDTO notificationDTO) {
        notificationEjb.confirmReceivingNotification(notificationDTO);
    }

    @Override
    public void sendNotification(NotificationDTO notification) {
        notificationEjb.sendNotification(notification);
    }
    
    @Override
    /**
     * @author Hossam ElDeen
     */
    public List<NotificationDTO> getAllNotification(UserDTO userDTO) {
		return notificationEjb.getNotification(userDTO);
	}
    
    @Override
    /**
     * @author Hossam ElDeen
     */
    public List<NotificationDTO> getUnReadedUserNotification(UserDTO userDTO) {
		return notificationEjb.getUnReadedNotification(userDTO);
	}
}
