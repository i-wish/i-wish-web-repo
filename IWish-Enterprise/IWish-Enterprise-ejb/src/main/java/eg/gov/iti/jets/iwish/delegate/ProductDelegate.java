/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.CategoryDTO;
import eg.gov.iti.jets.iwish.dto.CompanyDTO;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.StoreOwnerDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserProductDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Hossam Abdallah
 */
public interface ProductDelegate {
	public ArrayList<ProductDTO> getAllProducts();

	public List<ProductDTO> getProductsByUser(UserDTO userDTO);

	public CategoryDTO getCategoryById(int id);

	public CompanyDTO getCompanyById(int id);

	void updateProduct(ProductDTO productDTO);

	void synchProudct();

	public ProductDTO getProductImage(ProductDTO productDTO);

	Long getProductsCount();

	Map<String, Integer> getCompanyProductsMap();
	
	public void addProduct(ProductDTO productDTO, CompanyDTO companyDTO, CategoryDTO categoryDTO);
	
	List<ProductDTO> getProducts(StoreOwnerDTO storeOwnerDTO);
	
	void deleteProduct(ProductDTO productDTO);
	
	public List<UserProductDTO> getUserProductsDTOsList(UserDTO user);
}
