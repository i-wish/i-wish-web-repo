/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.ejb.SuggestProductNoWishListEjb;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil;

import java.util.ArrayList;

/**
 *
 * @author Hossam Abdallah
 */
public class SuggestProductNoWishListDelegateImpl implements SuggestProductNoWishListDelegate{
    SuggestProductNoWishListEjb suggestProductNoWishListEjb = (SuggestProductNoWishListEjb) ServiceLocatorUtil.getInistance().lookup(ServiceLocatorUtil.ServiceLocatorType.SUGGEST_PRODUCT_NO_WISHLIST_SERVICE);

    @Override
    public ArrayList<ProductDTO> getSuggestedProducts(Integer startOfRange, Integer endOfRange) {
        return suggestProductNoWishListEjb.getSuggestedProducts(startOfRange,endOfRange);
    }
    
}
