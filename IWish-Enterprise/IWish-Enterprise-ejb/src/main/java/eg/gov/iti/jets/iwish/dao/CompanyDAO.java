package eg.gov.iti.jets.iwish.dao;

// Generated Mar 31, 2015 5:37:08 PM by Hibernate Tools 4.0.0

import eg.gov.iti.jets.iwish.dto.CompanyDTO;
import eg.gov.iti.jets.iwish.pojo.Company;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.logging.Logger;

/**
 * Home object for domain model class Company.
 * 
 * @see eg.gov.iti.jets.iwish.pojo.Company
 * @author Hibernate Tools
 */
@Stateless
@LocalBean
public class CompanyDAO extends AbstractDAO<Company, CompanyDTO> {
	private static final Logger log = Logger.getLogger(CompanyDAO.class.getName());

	public CompanyDAO() {
		super(Company.class);
	}

	@Override
	public Company getEntity(Company t, CompanyDTO e) {
		t.setName(e.getName());
		return t;
	}

}
