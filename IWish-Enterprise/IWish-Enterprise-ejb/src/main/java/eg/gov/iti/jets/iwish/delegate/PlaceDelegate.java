/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.PlaceDTO;

import java.util.List;

/**
 *
 * @author Radwa Manssour
 */
public interface PlaceDelegate {
	public List<PlaceDTO> getPlaces(EventTypeDTO eventType);
}
