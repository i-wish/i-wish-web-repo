package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.ProductReserveDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserProductDTO;

import java.util.ArrayList;

/**
 * Created by Hossam Abdallah on 4/28/2015.
 */
public interface WishListDelegate {

    void addProducts(ArrayList<ProductDTO> products, UserDTO user);

    boolean addProducts(ArrayList<Integer> products, String email);

    void deleteProducts(ArrayList<ProductDTO> products, UserDTO user);

    ArrayList<ProductDTO> getFriendWishList(UserDTO friend);

    ArrayList<UserProductDTO> getFriendWishListWithDetails(UserDTO friend);

    void deleteGift(UserProductDTO userProduct);

    boolean reserveGift(UserDTO friendDTO, UserProductDTO userProductDTO, String comment);

    public UserProductDTO getUserProductDTO(UserDTO userDTO, ProductDTO productDTO);

    public ArrayList<UserProductDTO> getUserProductDTOs(UserDTO userDTO);

    public ArrayList<UserDTO> getFriendsReserved(UserProductDTO userProductDTO);

    public ProductDTO getWishlistProduct(UserProductDTO userProductDTO);

    public ArrayList<ProductReserveDTO> getProductReservers(UserProductDTO userProductDTO);

    public UserDTO getUserReservedProduct(ProductReserveDTO productReserveDTO);

    public boolean reserveGiftWithOthers(UserDTO friend,UserProductDTO userProductDTO,String comment,Double participate);

    public boolean isReservedBefore(UserDTO friend,UserProductDTO userProductDTO);

    public boolean deleteReservation(UserDTO friend,UserProductDTO userProductDTO);
}
