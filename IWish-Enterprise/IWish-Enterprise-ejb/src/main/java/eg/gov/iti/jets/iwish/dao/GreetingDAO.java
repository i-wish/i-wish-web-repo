package eg.gov.iti.jets.iwish.dao;

// Generated Mar 31, 2015 5:37:08 PM by Hibernate Tools 4.0.0

import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.GreetingDTO;
import eg.gov.iti.jets.iwish.pojo.EventType;
import eg.gov.iti.jets.iwish.pojo.Greeting;
import eg.gov.iti.jets.iwish.util.PojoUtil;
import org.hibernate.Query;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Home object for domain model class Greeting.
 * 
 * @see eg.gov.iti.jets.iwish.pojo.Greeting
 * @author Hibernate Tools
 */
@Stateless
@LocalBean
public class GreetingDAO extends AbstractDAO<Greeting, GreetingDTO> {
	private static final Logger log = Logger.getLogger(GreetingDAO.class.getName());

	public GreetingDAO() {
		super(Greeting.class);
	}

	@Override
	public Greeting getEntity(Greeting t, GreetingDTO e) {
		t.setDescription(e.getDescription());
		t.setEventType(PojoUtil.getPojo(e.getEventTypeDTO()));
		t.setId(e.getId());
		return t;
	}

	//FIXME
	//why to check with a pojo object that was converted from dto
	public ArrayList<GreetingDTO> getGreetingOfType(EventTypeDTO eventTypeDTO) {
		EventType eventType = PojoUtil.getPojo(eventTypeDTO);
		Query query = getHibernateSession().createQuery("FROM Greeting g where g.eventType=:eventType")
				.setParameter("eventType", eventType);
		ArrayList<Greeting> greetings = (ArrayList<Greeting>) query.list();
		ArrayList<GreetingDTO> greetingDTOs = new ArrayList<>();

		for (Greeting greeting : greetings) {
			greetingDTOs.add(PojoUtil.getDTO(greeting));
		}

		return greetingDTOs;
	}
}
