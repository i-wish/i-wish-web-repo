package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.ejb.FacebookEjb;
import eg.gov.iti.jets.iwish.facebook.type.FacebookPost;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil.ServiceLocatorType;

/**
 * Created by eltntawy on 22/04/15.
 */

public class FacebookDelegateImpl implements FacebookDelegate {
	FacebookEjb facebookEjb = (FacebookEjb) ServiceLocatorUtil.getInistance()
			.lookup(ServiceLocatorType.FACEBOOK_SERVICE);

	@Override
	public String getFacebookDialogUrl(String redirectUrl) {
		return facebookEjb.getFacebookLoginDialogUrl(redirectUrl);
	}

	@Override
	public UserDTO getFacebookUserData(String redirectUrl, String verificationCode) {
		return facebookEjb.getFacebookUserData(redirectUrl, verificationCode);
	}

	@Override
	public UserDTO getFacebookUserData(String accessToken) {
		return facebookEjb.getFacebookUserData(accessToken);
	}

	@Override
	public void updateFacebookUserFriend(UserDTO userDTO) {
		facebookEjb.updateUserFriendsList(userDTO);
	}

	@Override
	public void postOnFacebook(UserDTO userDTO, FacebookPost post) {
		facebookEjb.postOnFacebook(userDTO, post);
	}

	@Override
	public void pushFacebookNotification(UserDTO userDTO, String href, String message) {
		facebookEjb.pushFacebookNotification(userDTO, href, message);
	}
}
