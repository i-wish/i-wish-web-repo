package eg.gov.iti.jets.iwish.dao;

// Generated Mar 31, 2015 5:37:08 PM by Hibernate Tools 4.0.0

import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;
import eg.gov.iti.jets.iwish.pojo.UserEvent;
import eg.gov.iti.jets.iwish.util.PojoUtil;
import org.hibernate.Query;
import org.hibernate.criterion.Example;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Home object for domain model class UserEvent.
 * 
 * @see eg.gov.iti.jets.iwish.pojo.UserEvent
 * @author Hibernate Tools
 */
@Stateless
@LocalBean
public class UserEventDAO extends AbstractDAO<UserEvent, UserEventDTO> {
	private static final Logger log = Logger.getLogger(UserEventDAO.class.getName());

	public UserEventDAO() {
		super(UserEvent.class);
	}
	
	@Override
	public UserEvent getEntity(UserEvent userEvent, UserEventDTO userEventDTO) {
		userEvent.setDate(userEventDTO.getDate());
		userEvent.setDescription(userEventDTO.getDescription());
		userEvent.setEventCommentses(PojoUtil.getDTOList(userEventDTO.getEventCommentses()));
		userEvent.setName(userEventDTO.getName());
		userEvent.setPlace(PojoUtil.getPojo(userEventDTO.getPlace()));
		userEvent.setTime(userEventDTO.getTime());
		userEvent.setEventType(PojoUtil.getPojo(userEventDTO.getEventTypeDTO()));
		userEvent.setPicture(userEventDTO.getImage());
		userEvent.setUser(PojoUtil.getPojo(userEventDTO.getUserDTO()));
		userEvent.setId(userEventDTO.getId());
		return userEvent;
	}
	
	//FIXME
	//why not to use the one in AbstractDAO
	public List findByExample(UserEvent instance) {
		log.log(Level.FINE, "finding UserEvent instance by example");
		try {
			List results = getHibernateSession().createCriteria("eg.gov.iti.jets.iwish.pojo.UserEvent")
					.add(Example.create(instance)).list();
			log.log(Level.FINE, "find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.log(Level.SEVERE, "find by example failed", re);
			throw re;
		}
	}

	public List<UserEventDTO> getEventsOfUserInToday(UserDTO userDTO) {
		Query query = getHibernateSession().createSQLQuery(
				"select * from User_Event as u where u.User_id= " + userDTO.getId() + " and u.date = curdate() ");
		ArrayList<UserEventDTO> userEvents = (ArrayList<UserEventDTO>) query.list();
		return userEvents;
	}

	public List<UserEventDTO> FindByUserId(UserDTO user) {
		Query query = getHibernateSession().createQuery("From UserEvent u Where u.user.id = " + user.getId());
		ArrayList<UserEvent> userEvents = (ArrayList<UserEvent>) query.list();
		ArrayList<UserEventDTO> userEventDTOs = (ArrayList<UserEventDTO>) PojoUtil.getDTOListForUserEvent(userEvents);
		return userEventDTOs;
	}

	public List<UserEventDTO> findByUserAndType(UserDTO user, EventTypeDTO eventTypeDTO) {
		String hql = "From UserEvent event where user.id= :userId and eventType.id= :eventTypeDTOId";
		Query query = getHibernateSession().createQuery(hql);
		query.setParameter("userId", user.getId());
		query.setParameter("eventTypeDTOId", eventTypeDTO.getId());

		List<UserEvent> userEvents = (List<UserEvent>) query.list();
		ArrayList<UserEventDTO> userEventDTOs = (ArrayList<UserEventDTO>) PojoUtil.getDTOListForUserEvent(userEvents);
		return userEventDTOs;
	}

	//FIXME
	//why not to use the version from AbstractDAO
	public UserEvent findPojoById(int id) {
		String hql = "From UserEvent event where event.id = :id";
		Query query = getHibernateSession().createQuery(hql);
		query.setParameter("id", id);
		UserEvent userEvent = (UserEvent) query.uniqueResult();
		return userEvent;
	}

	public List<UserEventDTO> getAllUserEvents() {
		Query query = getHibernateSession().createQuery("From UserEvent");
		
		List<UserEvent> userEvents = (List<UserEvent>) query.list();
		ArrayList<UserEventDTO> userEventDTOs = (ArrayList<UserEventDTO>) PojoUtil.getDTOListForUserEvent(userEvents);
		return userEventDTOs;
	}
}
