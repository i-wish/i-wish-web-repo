/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.EventCommentsDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;
import eg.gov.iti.jets.iwish.ejb.SendGreetingEjb;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil;

import java.util.ArrayList;

/**
 *
 * @author Hossam Abdallah
 */
public class SendGreetingDelegateImpl implements SendGreetingDelegate {
	SendGreetingEjb sendGreetingEjb = (SendGreetingEjb) ServiceLocatorUtil.getInistance().lookup(ServiceLocatorUtil.ServiceLocatorType.SEND_GREETING_SERVICE);

	@Override
	public void addGreetingToEvent(String greeting, UserEventDTO userEventDTO, UserDTO userDTO) {
		sendGreetingEjb.addGreetingToEvent(greeting, userEventDTO, userDTO);
	}

	@Override
	public ArrayList<EventCommentsDTO> getCommentsOnEvent(UserEventDTO userEventDTO) {
		return sendGreetingEjb.getCommentsOnEvent(userEventDTO);
	}

}
