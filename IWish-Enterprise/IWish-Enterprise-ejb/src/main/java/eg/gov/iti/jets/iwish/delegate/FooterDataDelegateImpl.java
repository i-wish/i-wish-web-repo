/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.ejb.FooterDataEjb;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil;

import java.util.Map;

/**
 *
 * @author Hossam Abdallah
 */
public class FooterDataDelegateImpl implements FooterDataDelegate{
    FooterDataEjb footerDataEjb = (FooterDataEjb) ServiceLocatorUtil.getInistance().lookup(ServiceLocatorUtil.ServiceLocatorType.FOOTER_DATA_SERVICE);
    
    @Override
    public Map<String, String> getFooterStaticText() {
        return footerDataEjb.getFooterStaticText();
    }
    
}
