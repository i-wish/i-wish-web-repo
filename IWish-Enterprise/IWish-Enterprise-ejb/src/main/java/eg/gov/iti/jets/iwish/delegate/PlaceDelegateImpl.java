/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.PlaceDTO;
import eg.gov.iti.jets.iwish.ejb.PlaceEjb;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil;

import java.util.List;

/**
 *
 * @author Radwa Manssour
 */
public class PlaceDelegateImpl implements PlaceDelegate{
    PlaceEjb placeEjb = (PlaceEjb) ServiceLocatorUtil.getInistance().lookup(ServiceLocatorUtil.ServiceLocatorType.SUGGEST_PLACE_SERVICE);

    @Override
    public List<PlaceDTO> getPlaces(EventTypeDTO eventType) {
        return  placeEjb.getPlaces(eventType);
    }
}
