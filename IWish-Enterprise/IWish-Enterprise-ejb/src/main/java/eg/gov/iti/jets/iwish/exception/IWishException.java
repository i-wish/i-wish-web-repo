package eg.gov.iti.jets.iwish.exception;

/**
 * Created by eltntawy on 15/05/15.
 */

public class IWishException extends RuntimeException {
    public IWishException(String message) {
        super(message);
    }
}
