package eg.gov.iti.jets.iwish.exception;

import javax.ejb.ApplicationException;

/**
 * Created by eltntawy on 15/05/15.
 */
@ApplicationException(rollback = true)
public class IWishRollbackException extends IWishDatabaseException {
    public IWishRollbackException(String message) {
        super(message);
    }
}
