/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eg.gov.iti.jets.iwish.ejb;

import eg.gov.iti.jets.iwish.dao.ProductReserveDAO;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.ProductReserveDTO;
import eg.gov.iti.jets.iwish.interceptor.LoggerInterceptor;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Hossam Abdallah
 */
@Stateless(mappedName="SuggestProductNoWishListEjb")
@LocalBean
@Interceptors(LoggerInterceptor.class)
public class SuggestProductNoWishListEjb {
    @EJB
    ProductReserveDAO productReserveDAO;

    public ArrayList<ProductDTO> getSuggestedProducts(Integer startOfRange, Integer endOfRange) {
        ArrayList<ProductReserveDTO> productReserveDTOs = (ArrayList<ProductReserveDTO>) productReserveDAO.getAll();
        HashMap<ProductDTO,Integer> prodMap = new HashMap<>();
        ArrayList<ProductDTO> productDTOs = new ArrayList<>();
        
        for(ProductReserveDTO productReserveDTO: productReserveDTOs){
            ProductDTO currentProduct = productReserveDTO.getUserProductDto().getProductDTO();
            if(prodMap.containsKey(currentProduct)){
                Integer x = prodMap.get(currentProduct);
                x= x+1;
                prodMap.remove(currentProduct);
                prodMap.put(currentProduct,x);
            }else{
                prodMap.put(currentProduct,1);
            }
        }
        
        
        int max=0;
        ProductDTO suggestedProduct = null;
        for(ProductDTO product: prodMap.keySet()){
            if(prodMap.get(product) > max && product.getPrice()> startOfRange && product.getPrice() < endOfRange){
                max = prodMap.get(product);
                suggestedProduct = product;
            }
        }
        
        
        
        productDTOs.add(suggestedProduct);
        
        return productDTOs;
    }
    
}
