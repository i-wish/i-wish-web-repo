package eg.gov.iti.jets.iwish.dao;

import eg.gov.iti.jets.iwish.dto.AbstractDTO;
import eg.gov.iti.jets.iwish.exception.IWishCommitException;
import eg.gov.iti.jets.iwish.exception.IWishRollbackException;
import eg.gov.iti.jets.iwish.pojo.AbstractPojo;
import eg.gov.iti.jets.iwish.util.PojoUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.ejb.EntityManagerImpl;
import javax.naming.OperationNotSupportedException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractDAO<T extends AbstractPojo, E extends AbstractDTO> {

	private static final Logger log = Logger.getLogger(AbstractDAO.class.getName());
	private final Class<T> type;

	@PersistenceContext(unitName = "entityManager")
	protected EntityManager entityManager;

	public AbstractDAO(Class<T> type) {
		this.type = type;
	}

	public Class<T> getMyType() {
		return this.type;
	}

	public Session getHibernateSession() {
		try {
			// for jboss wildfly and intellij
			Session session = (Session) entityManager.getDelegate();
			return session;
		} catch (ClassCastException ex) {
			// for glassfish and netbeans
			EntityManagerImpl entityManagerImpl = (EntityManagerImpl) entityManager.getDelegate();
			return entityManagerImpl.getSession();
		}
	}

	public void persist(T transientInstance) {
		if (transientInstance == null) {
			throw new IllegalArgumentException(getMyType().getSimpleName() + " can not be null");
		}

		log.log(Level.FINE, "persisting " + getMyType().getSimpleName() + " begin method");
		try {
			entityManager.persist(transientInstance);
		} catch (RuntimeException re) {
			String message = getCauseMessage(re);
			log.log(Level.SEVERE, "persisting " + getMyType().getSimpleName() + " : " + " : " + message, re);
			throw new IWishRollbackException("unable to persist : " + getMyType().getSimpleName() + " : " + message);
		}
		log.log(Level.FINE, "persisting " + getMyType().getSimpleName() + " end method");
	}

	public void saveOrUpdate(T transientInstance) {
		if (transientInstance == null) {
			throw new IllegalArgumentException(getMyType().getSimpleName() + " can not be null");
		}
		log.log(Level.FINE, "saveOrUpdate " + getMyType().getSimpleName() + " begin method");
		try {
			getHibernateSession().saveOrUpdate(transientInstance);
		} catch (RuntimeException re) {
			String message = getCauseMessage(re);
			log.log(Level.SEVERE, "saveOrUpdate " + getMyType().getSimpleName() + " : " + " : " + message, re);
			throw new IWishRollbackException(
					"unable to saveOrUpdate : " + getMyType().getSimpleName() + " : " + message);
		}
	}

	public void saveOrUpdate(E unTransientInstance) {
		if (unTransientInstance == null) {
			throw new IllegalArgumentException(getMyType().getSimpleName() + " can not be null");
		}
		log.log(Level.FINE, "saveOrUpdate " + getMyType().getSimpleName() + " begin method");
		try {
			T t = findEntityById(unTransientInstance.getId());
			t = getEntity(t, unTransientInstance);
			getHibernateSession().saveOrUpdate(t);
		} catch (RuntimeException re) {
			String message = getCauseMessage(re);
			log.log(Level.SEVERE, "saveOrUpdate " + getMyType().getSimpleName() + " : " + " : " + message, re);
			throw new IWishRollbackException(
					"unable to saveOrUpdate : " + getMyType().getSimpleName() + " : " + message);
		}
	}

	public void delete(T persistentInstance) {
		if (persistentInstance == null) {
			throw new IllegalArgumentException(getMyType().getSimpleName() + " can not be null");
		}
		log.log(Level.FINE, "delete " + getMyType().getSimpleName() + " begin method");
		try {
			entityManager.merge(persistentInstance);
			entityManager.remove(persistentInstance);
		} catch (RuntimeException re) {
			String message = getCauseMessage(re);
			log.log(Level.SEVERE, "delete " + getMyType().getSimpleName() + " : " + " : " + message, re);
			throw new IWishRollbackException("unable to delete : " + getMyType().getSimpleName() + " : " + message);
		}
	}

	public void delete(Integer id) {
		if (id == null) {
			throw new IllegalArgumentException(getMyType().getSimpleName() + " can not be null");
		}
		log.log(Level.FINE, "delete " + getMyType().getSimpleName() + " begin method");
		try {
			T instance = findEntityById(id);
			entityManager.remove(instance);
		} catch (RuntimeException re) {
			String message = getCauseMessage(re);
			log.log(Level.SEVERE, "delete " + getMyType().getSimpleName() + " : " + " : " + message, re);
			throw new IWishRollbackException("unable to delete : " + getMyType().getSimpleName() + " : " + message);
		}
	}

	public E merge(T detachedInstance) {
		if (detachedInstance == null) {
			throw new IllegalArgumentException(getMyType().getSimpleName() + " can not be null");
		}
		log.log(Level.FINE, "merge " + getMyType().getSimpleName() + " begin method");
		try {
			T result = entityManager.merge(detachedInstance);

			return (E) PojoUtil.getDTO(result);
		} catch (RuntimeException re) {
			String message = getCauseMessage(re);
			log.log(Level.SEVERE, "merge " + getMyType().getSimpleName() + " : " + " : " + message, re);
			throw new IWishRollbackException("unable to merge : " + getMyType().getSimpleName() + " : " + message);
		} catch (OperationNotSupportedException e) {
			log.log(Level.SEVERE, "merge " + getMyType().getSimpleName() + " : " + e.getMessage());
			throw new IWishRollbackException("operation getDTO not supperted yet : " + getMyType().getSimpleName());
		}
	}

	public E findById(Integer id) {
		if (id == null) {
			throw new IllegalArgumentException("id can not be null");
		}
		log.log(Level.FINE, "findById " + getMyType().getSimpleName() + " begin method");
		try {
			T instance = entityManager.find(type, id);
			return (E) PojoUtil.getDTO(instance);
		} catch (RuntimeException re) {
			String message = getCauseMessage(re);
			log.log(Level.SEVERE, "findById " + getMyType().getSimpleName() + " : " + " : " + message, re);
			throw new IWishCommitException("unable to find by id : " + getMyType().getName() + " - id : " + id);
		} catch (OperationNotSupportedException e) {
			String message = getCauseMessage(e);
			log.log(Level.SEVERE, "findById " + getMyType().getSimpleName() + " : " + message, e);
			throw new IWishRollbackException("operation getDTO not supperted yet : " + getMyType().getSimpleName());
		}
	}

	public T findEntityById(Integer id) {
		if (id == null) {
			throw new IllegalArgumentException("id can not be null");
		}
		log.log(Level.FINE, "findEntityById " + getMyType().getSimpleName() + " begin method");
		try {
			T instance = entityManager.find(type, id);
			return instance;
		} catch (RuntimeException re) {
			String message = getCauseMessage(re);
			log.log(Level.SEVERE, "findEntityById " + getMyType().getSimpleName() + " : " + " : " + message, re);
			throw new IWishCommitException("unable to find by id : " + getMyType().getName() + " - id : " + id);
		}
	}

	public List<E> findByExample(T instance) {
		if (instance == null) {
			throw new IllegalArgumentException(getMyType().getSimpleName() + " can not be null");
		}
		log.log(Level.FINE, "findByExample " + getMyType().getSimpleName() + " begin method");
		try {
			Example ex = Example.create(instance);
			List<T> results = (List<T>) getHibernateSession().createCriteria(getMyType()).add(ex).list();

			List<E> resultList = new ArrayList<E>();

			for (T t : results) {
				resultList.add((E) PojoUtil.getDTO(t));
			}
			return resultList;
		} catch (RuntimeException re) {
			String message = getCauseMessage(re);
			log.log(Level.SEVERE, "findByExample " + getMyType().getSimpleName() + " : " + " : " + message, re);
			throw new IWishCommitException(
					"unable to find by Example : " + getMyType().getSimpleName() + " : " + message);
		} catch (OperationNotSupportedException e) {
			log.log(Level.SEVERE, "findByExample " + getMyType().getSimpleName() + " : " + e.getMessage());
			throw new IWishRollbackException("operation getDTO not supperted yet : " + getMyType().getSimpleName());
		}
	}

	public List<T> findEntityByExample(T instance) {
		if (instance == null) {
			throw new IllegalArgumentException(getMyType().getSimpleName() + " can not be null");
		}
		log.log(Level.FINE, "findByExample " + getMyType().getSimpleName() + " begin method");
		try {
			Example ex = Example.create(instance);
			List<T> results = (List<T>) getHibernateSession().createCriteria(getMyType()).add(ex).list();

			return results;

		} catch (RuntimeException re) {
			String message = getCauseMessage(re);
			log.log(Level.SEVERE, "findByExample " + getMyType().getSimpleName() + " : " + " : " + message, re);
			throw new IWishCommitException(
					"unable to find by Example : " + getMyType().getSimpleName() + " : " + message);
		}
	}

	public List<E> getAll() {
		log.log(Level.FINE, "getAll " + getMyType().getSimpleName() + " begin method");
		try {
			String type = getMyType().getSimpleName();
			List<T> results = (List<T>) getHibernateSession().createQuery("From " + type).list();
			List<E> resultList = new ArrayList<E>();

			for (T t : results) {
				resultList.add((E) PojoUtil.getDTO(t));
			}
			return resultList;
		} catch (RuntimeException re) {
			String message = getCauseMessage(re);
			log.log(Level.SEVERE, "getAll " + getMyType().getSimpleName() + " : " + " : " + message, re);
			throw new IWishCommitException("unable to getAll : " + getMyType().getSimpleName() +" : "+ message);
		} catch (OperationNotSupportedException e) {
			log.log(Level.SEVERE, "getAll " + getMyType().getSimpleName() + " : " + e.getMessage());
			throw new IWishRollbackException("operation getDTO not supperted yet : " + getMyType().getSimpleName());
		}
	}

	//FIXME
	public List<E> findRange(int[] range) {
		try {
			javax.persistence.criteria.CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
			cq.select(cq.from(getMyType()));
			javax.persistence.Query q = entityManager.createQuery(cq);
			q.setMaxResults(range[1] - range[0] + 1);
			q.setFirstResult(range[0]);
			
			List<T> results = q.getResultList();	
			List<E> resultList = new ArrayList<>();
			for (T t : results) {
				resultList.add((E) PojoUtil.getDTO(t));
			}
			return resultList;
		} catch (RuntimeException re) {
			String message = getCauseMessage(re);
			log.log(Level.SEVERE, "getAll " + getMyType().getSimpleName() + " : " + " : " + message, re);
			throw re;// new IWishCommitException("unable to getAll : " +
						// getMyType().getSimpleName() +" : "+ message);
		} catch (OperationNotSupportedException e) {
			log.log(Level.SEVERE, "getAll " + getMyType().getSimpleName() + " : " + e.getMessage());
			throw new IWishRollbackException("operation getDTO not supperted yet : " + getMyType().getSimpleName());
		}

	}

	//FIXME
	public int count() {
		javax.persistence.criteria.CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
		javax.persistence.criteria.Root<T> rt = cq.from(getMyType());
		cq.select(entityManager.getCriteriaBuilder().count(rt));
		javax.persistence.Query q = entityManager.createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}

	//FIXME
	private String getCauseMessage(Throwable t) {
		Throwable cause = t.getCause();
		String message = "";
		while (cause != null) {
			message = cause.getMessage();
			cause = cause.getCause();
		}

		return message;

	}

	
	/**
	 *
	 * Method must implemented in subclass to update entity object with new data
	 * from dto object
	 *
	 * @param t
	 *            TODO
	 * @param e
	 * @return TODO
	 *
	 *
	 */
	public abstract T getEntity(T t, E e);
}
