/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.service.locator;

import eg.gov.iti.jets.iwish.ejb.*;

import javax.ejb.*;

import static eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil.ServiceLocatorType;

/**
 *
 * @author Hossam Abdallah
 */
@Singleton
@Lock(LockType.READ)
@Startup
public class ServiceLocator {

    @EJB
    FacebookEjb facebookEjb;

    @EJB
    ProductEjb productEjb;

    @EJB
    ProductMapsEjb productMapsEjb;

    @EJB
    WishListEjb wishListEjb;

    @EJB
    UserEjb userEjb;

    @EJB
    StoreOwnerEjb storeOwnerEjb;

    @EJB
    EventEjb eventEjp;

    @EJB
    SendGreetingEjb sendGreetingEjb;

    @EJB
    FooterDataEjb footerDataEjb;
    
    @EJB
    NotificationEjb pushNotificationEjb;
    
    @EJB
    SuggestGreetingEjb suggestGreetingEjb;

    @EJB
    PlaceEjb placeEjb;
    
    @EJB
    SuggestProductNoWishListEjb suggestProductNoWishListEjb;
    
    public ServiceLocator() {

    }

    public Object lookup(ServiceLocatorType key) {

        switch (key) {
            case FACEBOOK_SERVICE:
                return facebookEjb;
            case PUSH_NOTIFICATION_SERVICE:
                return pushNotificationEjb;
            case PRODUCT_SERVICE:
                return productEjb;
            case WISHLIST_SERVICE:
                return wishListEjb;
            case PRODUCT_MAPS_SERVICE:
                return productMapsEjb;
            case USER_DATA_SERVICE:
                return userEjb;
            case STORE_OWNER_SERVICE:
                return storeOwnerEjb;
            case USEREVENT_SERVICE:
                return eventEjp;
            case SUGGEST_PLACE_SERVICE:
                return placeEjb;
            case SEND_GREETING_SERVICE:
                return sendGreetingEjb;
            case FOOTER_DATA_SERVICE:
                return footerDataEjb;
            case SUGGEST_GREETING_SERVICE:
                return suggestGreetingEjb;
            case SUGGEST_PRODUCT_NO_WISHLIST_SERVICE:
                return suggestProductNoWishListEjb;
            default:
                return null;
        }

    }

}
