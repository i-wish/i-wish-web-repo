package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.PlaceDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;
import eg.gov.iti.jets.iwish.ejb.EventEjb;
import eg.gov.iti.jets.iwish.ejb.UserEjb;
import eg.gov.iti.jets.iwish.service.locator.ServiceLocatorUtil;

import java.util.ArrayList;
import java.util.List;

public class EventDelegateImpl implements EventDelegate {
    EventEjb eventEjb = (EventEjb) ServiceLocatorUtil.getInistance().lookup(ServiceLocatorUtil.ServiceLocatorType.USEREVENT_SERVICE);
    NotificationDelegate NotificationDelegate = DelegateFactory.getNotificationDelegateInstance();

    @Override
    public void addEvent(UserEventDTO event) {
        eventEjb.addEvent(event);
        NotificationDelegate.sendNotification(event);
    }

    @Override
    public void updateEvent(UserEventDTO event) {
        eventEjb.updateEvent(event);
        NotificationDelegate.sendNotification(event);
    }

    @Override
    public void removeEvent(UserEventDTO event) {
        eventEjb.removeEvent(event);
    }

    @Override
    public List<UserEventDTO> getEvents(UserDTO user) {
        return eventEjb.getEvents(user);
    }

    public List<EventTypeDTO> getEventsType() {
        return eventEjb.getEventTypes();
    }

    public List<PlaceDTO> getPlaces() {
        return eventEjb.getPlaces();
    }

    @Override
    public List<UserEventDTO> getFriendEvents(UserDTO user) {
        return eventEjb.getFriendEvents(user);
    }

    @Override
    public ArrayList<UserEventDTO> getFriendsEvents(UserDTO userDTO) {
        return eventEjb.getFriendsEvents(userDTO);
    }

    @Override
    public UserEventDTO getEvent(UserEventDTO event) {
        return eventEjb.getEvent(event);
    }
    
    @Override
    public List<EventTypeDTO> getEventsTypeByName(String eventTypeName){
        return eventEjb.getEventTypesByName(eventTypeName);
    }

    @Override
    public boolean isEventExist(UserDTO user,EventTypeDTO eventTypeDTO){
        return eventEjb.isEventExist(user,eventTypeDTO);
    }

}
