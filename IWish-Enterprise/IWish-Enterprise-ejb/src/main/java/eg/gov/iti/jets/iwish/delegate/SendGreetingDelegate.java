/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eg.gov.iti.jets.iwish.delegate;

import eg.gov.iti.jets.iwish.dto.EventCommentsDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;

import java.util.ArrayList;

/**
 *
 * @author Hossam Abdallah
 */
public interface SendGreetingDelegate {
	public void addGreetingToEvent(String greeting, UserEventDTO userEventDTO, UserDTO userDTO);

	public ArrayList<EventCommentsDTO> getCommentsOnEvent(UserEventDTO userEventDTO);
}
