/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.ejb;

import eg.gov.iti.jets.iwish.dao.ProductDAO;
import eg.gov.iti.jets.iwish.dao.UserDAO;
import eg.gov.iti.jets.iwish.dao.UserEventDAO;
import eg.gov.iti.jets.iwish.delegate.NotificationDelegate;
import eg.gov.iti.jets.iwish.delegate.NotificationDelegateImpl;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;
import eg.gov.iti.jets.iwish.pojo.UserEvent;

import javax.ejb.*;
import javax.inject.Inject;
import javax.transaction.SystemException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author eltntawy
 */
@Singleton
@Lock(LockType.READ)
@LocalBean
public class SystemTimerEjb {

    private static final Logger log = Logger.getLogger(SystemTimerEjb.class.getName());
    NotificationDelegate notificationDelegate = DelegateFactory.getNotificationDelegateInstance();

    @Inject
    ProductDAO productDAO ;
    
    @Inject
    ParseSouq parseSouq;    

    @Inject
    UserDAO userDAO;
    
    @Inject
    NotificationEjb notificationEjb;

    @Inject
    UserEventDAO userEventDAO;


    @Schedule(dayOfWeek = "Fri", hour = "0")
    public void updateProduct() {
        log.fine("***********************************************************");
        productDAO.deleteAllProduct();
        try {
            parseSouq.getProducts();
        } catch (SystemException e) {
            e.printStackTrace();
        }
    }
   /* @Schedule(dayOfWeek = "*", hour = "0")
    public void birthdayNotification() {
       ArrayList<UserDTO> users = userDAO.getAllUsers();
       for(UserDTO user : users){
           if(user.getBirthday().equals(new Date())){
               notificationEjb.sendNotification(null);
           }
       }
    }*/


   /*
   *@author Niveen
   *@
   * 1-everyday will check if any event will be after 5 days from today
   * 2-true will send notification
   *
   */

    @Schedule(dayOfWeek = "*", hour = "0")
   public void eventReminder() {
        log.fine("***********************************************************");
        Calendar eventDate = Calendar.getInstance();
        eventDate.setTime(new Date());
        eventDate.add(Calendar.DATE,5);
        List<UserEventDTO> userEvents =userEventDAO.getAllUserEvents();
        for (UserEventDTO userEventDTO:userEvents){
            if (userEventDTO.getDate().equals(eventDate.getTime())){
                notificationDelegate.sendNotification(userEventDTO);
            }
        }

   }

}
