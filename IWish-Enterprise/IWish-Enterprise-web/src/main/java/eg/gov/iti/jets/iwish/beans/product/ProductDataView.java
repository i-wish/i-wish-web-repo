package eg.gov.iti.jets.iwish.beans.product;

import eg.gov.iti.jets.iwish.delegate.ProductDelegate;
import eg.gov.iti.jets.iwish.delegate.ProductMapsDelegate;
import eg.gov.iti.jets.iwish.delegate.StoreOwnerDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.*;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eltntawy on 14/05/15.
 */

@ManagedBean
@ViewScoped
public class ProductDataView {


    private List<ProductDTO> productList;
    private ProductDTO selectedProduct;

    final StoreOwnerDelegate storeOwnerDelegate = DelegateFactory.getStoreOwnerDelegate();
    final ProductMapsDelegate productMapsDelegate = DelegateFactory.getProductMapsDelegate();
    final ProductDelegate productdelegate = DelegateFactory.getProductDelegateInstance();

    ArrayList<CompanyDTO> companies = new ArrayList<>();
    ArrayList<CategoryDTO> categories = new ArrayList<>();

    ProductDTO productDTO = new ProductDTO();
    CompanyDTO selectedCompany = new CompanyDTO();
    CategoryDTO selectedCategory = new CategoryDTO();

    @ManagedProperty("#{user}")
    GenericUser storeOwnerDTO;

    public DataModel<ProductDTO> productDTODataModel;


    {
        categories = (ArrayList<CategoryDTO>) productMapsDelegate.getCategories();
        companies = (ArrayList<CompanyDTO>) productMapsDelegate.getCompanies();
    }

    @PostConstruct
    public void postConstruct() {
        if(storeOwnerDTO != null)
            productDTODataModel = new ListDataModel<ProductDTO>(storeOwnerDelegate.getProducts((StoreOwnerDTO)storeOwnerDTO)) ;
    }

    public DataModel<ProductDTO> getProductDTODataModel() {
        return productDTODataModel;
    }

    public ArrayList<CategoryDTO> getCategories() {
        return categories;
    }

    public ArrayList<CompanyDTO> getCompanies() {
        return companies;
    }

    public ProductDTO getProductDTO() {
        return productDTO;
    }

    public CategoryDTO getSelectedCategory() {
        return selectedCategory;
    }

    public CompanyDTO getSelectedCompany() {
        return selectedCompany;
    }

    public void setCategories(ArrayList<CategoryDTO> categories) {
        this.categories = categories;
    }

    public void setCompanies(ArrayList<CompanyDTO> companies) {
        this.companies = companies;
    }

    public void setProductDTO(ProductDTO productDTO) {
        this.productDTO = productDTO;
    }

    public void setSelectedCategory(CategoryDTO selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public void setSelectedCompany(CompanyDTO selectedCompany) {
        this.selectedCompany = selectedCompany;
    }

    public void setProductDTODataModel(DataModel<ProductDTO> productDTODataModel) {
        this.productDTODataModel = productDTODataModel;
    }

    public GenericUser getStoreOwnerDTO() {
        return storeOwnerDTO;
    }

    public void setStoreOwnerDTO(GenericUser storeOwnerDTO) {
        this.storeOwnerDTO = storeOwnerDTO;
    }

    public List<ProductDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductDTO> productList) {
        this.productList = productList;
    }

    public ProductDTO getSelectedProduct() {
        return selectedProduct;
    }

    public void setSelectedProduct(ProductDTO selectedProduct) {
        this.selectedProduct = selectedProduct;
    }

    public String saveProduct() {

        productdelegate.updateProduct(selectedProduct);

        productDTODataModel = new ListDataModel<ProductDTO>(storeOwnerDelegate.getProducts((StoreOwnerDTO)storeOwnerDTO)) ;

        String summary = "Success";
        String detail  = "product saved successfully";
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);


        return "";
    }

    public String deleteProduct() {

        ProductDTO productDTO = productDTODataModel.getRowData();

        storeOwnerDelegate.deleteProduct(productDTO);

        productDTODataModel = new ListDataModel<ProductDTO>(storeOwnerDelegate.getProducts((StoreOwnerDTO)storeOwnerDTO)) ;

        String summary = "Success";
        String detail  = "product deleted successfully";

        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);

        return "";
    }

    public void addProduct(){
        productDTO.setStoreOwnerDTO((StoreOwnerDTO)storeOwnerDTO);
        storeOwnerDelegate.addProduct(productDTO,selectedCompany,selectedCategory);
        this.postConstruct();
    }
}
