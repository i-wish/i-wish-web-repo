package eg.gov.iti.jets.iwish.beans.admin;

import eg.gov.iti.jets.iwish.beans.admin.util.JsfUtil;
import eg.gov.iti.jets.iwish.beans.admin.util.JsfUtil.PersistAction;
import eg.gov.iti.jets.iwish.dao.EventPlaceDAO;
import eg.gov.iti.jets.iwish.dao.EventTypeDAO;
import eg.gov.iti.jets.iwish.dao.PlaceDAO;
import eg.gov.iti.jets.iwish.dto.EventPlaceDTO;
import eg.gov.iti.jets.iwish.pojo.EventPlace;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named("eventPlaceController")
@SessionScoped
public class EventPlaceController implements Serializable {

    @EJB
    private EventPlaceDAO eventPlaceDAO;

    @EJB
    private EventTypeDAO eventTypeDAO;

    @EJB
    private PlaceDAO placeDAO;

    private List<EventPlaceDTO> items = null;
    private EventPlaceDTO selected;

    public EventPlaceController() {
    }

    public EventPlaceDTO getSelected() {
        return selected;
    }

    public void setSelected(EventPlaceDTO selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

   

    public EventPlaceDTO prepareCreate() {
        selected = new EventPlaceDTO();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("EventPlaceCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("EventPlaceUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("EventPlaceDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<EventPlaceDTO> getItems() {
        if (items == null) {
            items = eventPlaceDAO.getAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if(persistAction == PersistAction.UPDATE) {

                    EventPlace eventPlace = eventPlaceDAO.findEntityById(selected.getId());

                    eventPlace.setPlace(placeDAO.findEntityById(selected.getPlaceDTO().getId()));
                    eventPlace.setEventType(eventTypeDAO.findEntityById(selected.getEventTypeDTO().getId()));

                    eventPlaceDAO.saveOrUpdate(eventPlace);

                }else if (persistAction == PersistAction.CREATE)  {
                    EventPlace eventPlace = new EventPlace();

                    eventPlace.setPlace(placeDAO.findEntityById(selected.getPlaceDTO().getId()));
                    eventPlace.setEventType(eventTypeDAO.findEntityById(selected.getEventTypeDTO().getId()));

                    eventPlaceDAO.persist(eventPlace);
                } else {
                    eventPlaceDAO.delete(selected.getId());
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public EventPlaceDTO getEventPlace(java.lang.Integer id) {
        return eventPlaceDAO.findById(id);
    }

    public List<EventPlaceDTO> getItemsAvailableSelectMany() {
        return eventPlaceDAO.getAll();
    }

    public List<EventPlaceDTO> getItemsAvailableSelectOne() {
        return eventPlaceDAO.getAll();
    }

    @FacesConverter(forClass = EventPlace.class)
    public static class EventPlaceControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            EventPlaceController controller = (EventPlaceController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "eventPlaceController");
            return controller.getEventPlace(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            try { key = Integer.valueOf(value);} catch (NumberFormatException ex) {key = null; ex.printStackTrace();}
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof EventPlaceDTO) {
                EventPlaceDTO o = (EventPlaceDTO) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), EventPlace.class.getName()});
                return null;
            }
        }

    }

}
