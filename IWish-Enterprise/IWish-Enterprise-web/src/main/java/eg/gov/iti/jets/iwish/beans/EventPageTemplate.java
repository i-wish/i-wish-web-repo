/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.beans;

import eg.gov.iti.jets.iwish.delegate.*;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.*;

import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hossam Abdallah
 */
@ManagedBean(name = "eventPageTemplate")
@javax.faces.bean.ViewScoped
public class EventPageTemplate implements Serializable{

    WishListDelegate wishlistDelegate = DelegateFactory.getWishListDelegate();

    boolean dontRenderSuggest=false;

    ProductDTO suggestedProduct = null;
    Integer index = null;
    Integer min=0;
    Integer max=9000;

    UserDelegate userDelegate = DelegateFactory.getUserDelegateInstance();
    EventDelegate eventDelegate = DelegateFactory.getEventDelegate();
    SendGreetingDelegate sendGreetingDelegate = DelegateFactory.getSendGreetingDelegateInstance();
    SuggestGreetingDelegate suggestGreetingDelegate = DelegateFactory.getSuggestGreetingDelegateInstance();
    SuggestProductNoWishListDelegate suggestProductNoWishListDelegate = DelegateFactory.getSuggestProductNoWishListDelegateInstance();
    FacebookDelegate facebookDelegate = DelegateFactory.getFacebookDelegateInstance();


    ArrayList<GreetingDTO> greetingDTOs = new ArrayList<>();
    ArrayList<EventCommentsDTO> eventCommentsDTOs = new ArrayList<>();
    String sentGreeting;

    UserEventDTO userEventDTO;


    public void sendSuggestedGreeting(){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Object object = session.getAttribute("user");

        if(object instanceof UserDTO) {

            ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
            String url = ectx.getRequestScheme()
                    + "://" + ectx.getRequestServerName()
                    + ":" + ectx.getRequestServerPort()
                    + ectx.getRequestContextPath();

            url = url + "/pages/private/user/home.xhtml";
            final String backingURL = url;


            UserDTO user = (UserDTO) object;
            sendGreetingDelegate.addGreetingToEvent(getSuggestionForGreeting(), userEventDTO, user);

            final String notificationString =  user.getName()+" has told you:"+getSuggestionForGreeting();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    facebookDelegate.pushFacebookNotification(userEventDTO.getUserDTO(), backingURL, notificationString);
                }
            }).start();

        }
    }

    public boolean isDontRenderSuggest() {
        return dontRenderSuggest;
    }

    public void setDontRenderSuggest(boolean dontRenderSuggest) {
        this.dontRenderSuggest = dontRenderSuggest;
    }

    public EventPageTemplate() {

    }

    public Integer getMin() {
        return min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public void updateEvent(UserEventDTO userEventDTO) {
        this.userEventDTO = userEventDTO;
    }

    public void suggestNewItem() {
        ArrayList<ProductDTO> productDTOs = suggestProductNoWishListDelegate.getSuggestedProducts(min, max);
        if(productDTOs.get(0) != null){
            suggestedProduct = productDTOs.get(0);
            dontRenderSuggest = false;
        }else
            dontRenderSuggest = true;
    }

    public ProductDTO getSuggestedProduct() {
        if(userEventDTO == null)
            return null;
        if (suggestedProduct == null) {
            ArrayList<ProductDTO> productDTOs = suggestProductNoWishListDelegate.getSuggestedProducts(min, max);
            if(productDTOs.get(0) != null){
                suggestedProduct = productDTOs.get(0);
                dontRenderSuggest = false;
            }
            else
                dontRenderSuggest = true;
        }
        return suggestedProduct;
    }

    private ProductDTO suggestedFromWishList;

    public ProductDTO getSuggestedFromWishList() {
        return suggestedFromWishList;
    }

    public void setSuggestedFromWishList(ProductDTO suggestedFromWishList) {
        this.suggestedFromWishList = suggestedFromWishList;
    }

    public ProductDTO getRandomProduct(UserDTO friend) {

        List<ProductDTO> wishlist = wishlistDelegate.getFriendWishList(friend);

        if(wishlist != null && wishlist.size() > 0) {
            int index = (int) (Math.random() * wishlist.size());
            suggestedFromWishList = wishlist.get(index);
            return wishlist.get(index);
        }

        suggestedFromWishList = null;
        return null;
    }

    public void setSuggestedProduct(ProductDTO suggestedProduct) {
        this.suggestedProduct = suggestedProduct;
    }

    public ArrayList<EventCommentsDTO> getEventCommentsDTOs() {
        if (eventCommentsDTOs == null || userEventDTO == null) {
            return null;
        }
        eventCommentsDTOs.clear();
        eventCommentsDTOs = sendGreetingDelegate.getCommentsOnEvent(userEventDTO);
        return eventCommentsDTOs;
    }

    public void setEventCommentsDTOs(ArrayList<EventCommentsDTO> eventCommentsDTOs) {
        this.eventCommentsDTOs = eventCommentsDTOs;
    }

    public ArrayList<GreetingDTO> getGreetingDTOs() {
        if (greetingDTOs == null || userEventDTO == null) {
            return null;
        }
        if (index == null) {
            greetingDTOs.clear();
            greetingDTOs = suggestGreetingDelegate.getGreetings(userEventDTO.getEventTypeDTO());
            index = 0;
        }

        return greetingDTOs;
    }

    public void setGreetingDTOs(ArrayList<GreetingDTO> greetingDTOs) {
        this.greetingDTOs = greetingDTOs;
    }

    public String getSuggestionForGreeting() {

        if (greetingDTOs == null || userEventDTO == null) {
            return null;
        }
        getGreetingDTOs();
        if (index < greetingDTOs.size()) {
            return greetingDTOs.get(index).getDescription();
        } else {
            return "";
        }
    }

    public void nextSuggestionForGreeting() {
        if (index + 1 != greetingDTOs.size()) {
            index++;
        } else {
            index = 0;
        }
    }

    public void getEvent(UserEventDTO eventDTO) throws InterruptedException {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.setAttribute("event", eventDTO);
        Thread.sleep(50);
        userEventDTO = eventDTO;
    }

    public void setUserEventDTO(UserEventDTO userEventDTO) {
        ///////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////
        this.userEventDTO = userEventDTO;
    }

    public String getSentGreeting() {
        return sentGreeting;
    }

    public void setSentGreeting(String sentGreeting) {
        this.sentGreeting = sentGreeting;
    }

    public UserEventDTO getUserEventDTO() {
        return userEventDTO;
    }

    public void sendGreeting() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Object object = session.getAttribute("user");

        if(object instanceof UserDTO) {
            UserDTO user = (UserDTO) object;
            ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
            String url = ectx.getRequestScheme()
                    + "://" + ectx.getRequestServerName()
                    + ":" + ectx.getRequestServerPort()
                    + ectx.getRequestContextPath();

            url = url + "/pages/private/user/home.xhtml";

            final String backingURL=url;

            sendGreetingDelegate.addGreetingToEvent(sentGreeting, userEventDTO, user);

            final String notificationString =  user.getName()+" has told you:"+sentGreeting;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    facebookDelegate.pushFacebookNotification(userEventDTO.getUserDTO(), backingURL, notificationString);
                }
            }).start();
        }
    }
}
