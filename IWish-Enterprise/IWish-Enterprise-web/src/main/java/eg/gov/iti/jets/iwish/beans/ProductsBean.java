/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.beans;

import eg.gov.iti.jets.iwish.delegate.ProductDelegate;
import eg.gov.iti.jets.iwish.delegate.WishListDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.CategoryDTO;
import eg.gov.iti.jets.iwish.dto.CompanyDTO;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.*;

/**
 *
 * @author Hossam Abdallah
 */
@ManagedBean(name = "products", eager = true)
@ApplicationScoped
public class ProductsBean implements Serializable {

    Map<CategoryDTO, ArrayList<CompanyDTO>> categoryCompanyMap = new HashMap<>();
    Map<CompanyDTO, ArrayList<ProductDTO>> companyProductMap = new HashMap<>();

    ProductDelegate productDelegate;
    WishListDelegate wishlistDelegate;

    public ProductsBean() {
        productDelegate = DelegateFactory.getProductDelegateInstance();
        wishlistDelegate = DelegateFactory.getWishListDelegate();
    }

    public void setCategoryCompanyMap(Map<CategoryDTO, ArrayList<CompanyDTO>> categoryCompanyMap) {
        this.categoryCompanyMap = categoryCompanyMap;
    }

    public Map<CategoryDTO, ArrayList<CompanyDTO>> getCategoryCompanyMap() {
        return categoryCompanyMap;
    }

    public void setCompanyProductMap(Map<CompanyDTO, ArrayList<ProductDTO>> companyProductMap) {
        this.companyProductMap = companyProductMap;
    }

    public Map<CompanyDTO, ArrayList<ProductDTO>> getCompanyProductMap() {
        return companyProductMap;
    }

    public void fillAllProducts() {
        List<ProductDTO> allProducts = productDelegate.getAllProducts();

        ArrayList<CategoryDTO> categories = new ArrayList<>();

        for (ProductDTO product : allProducts) {
            if (!isCatExist(categories, product.getCategoryDTO())) {
                categories.add(product.getCategoryDTO());
            }
        }

        for (CategoryDTO currentCat : categories) {
            ArrayList<CompanyDTO> companiesOfCat = new ArrayList<>();
            for (ProductDTO product : allProducts) {
                if (product.getCategoryDTO().equals(currentCat)) {
                    companiesOfCat.add(product.getCompanyDTO());

                    for (ProductDTO prod : allProducts) {
                        if (prod.getCompanyDTO().equals(product.getCompanyDTO())) {

                        }
                    }
                }
            }
            categoryCompanyMap.put(currentCat, companiesOfCat);
        }

    }

    private boolean isCatExist(ArrayList<CategoryDTO> categories, CategoryDTO keyCat) {
        for (CategoryDTO cat : categories) {
            if (cat.equals(keyCat)) {
                return true;
            }
        }
        return false;
    }

    public List<ProductDTO> getUserWishList() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);

        Object object = session.getAttribute("user");

        if(object instanceof UserDTO) {
            UserDTO user = (UserDTO) object;

            if (user != null) {
                List<ProductDTO> productDTOs = productDelegate.getProductsByUser(user);
                return productDTOs;
            }
        }

        return null;
    }

    
    
    public ProductDTO getRandomProduct(){
        
        Set<CompanyDTO> keis =  companyProductMap.keySet();
        int companiesSize = keis.size();
        if(companiesSize > 0){
            int randomCompanyIndex  =(int) (Math.random() * companiesSize );

            CompanyDTO randomCompany  = (CompanyDTO) keis.toArray()[randomCompanyIndex];

            List<ProductDTO> productList =  companyProductMap.get(randomCompany);

            if(productList != null && productList.size() > 0){
                int randomProduct = (int) ( Math.random() * productList.size());
                return productList.get(randomProduct);
            }

        }
        return null;
        
    }
}
