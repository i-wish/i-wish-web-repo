/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.filters;

import eg.gov.iti.jets.iwish.dto.StoreOwnerDTO;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 *
 * @author Niveen
 */
@WebFilter(urlPatterns = {"/pages/private/admin/*", "/pages/private/user/*"})

public class StoreOwnerFilter implements Filter{

    public StoreOwnerFilter() {
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = ((HttpServletRequest) request);
        HttpSession session = httpServletRequest.getSession();
        Object user = session.getAttribute("user");

        if (user instanceof StoreOwnerDTO && !((StoreOwnerDTO) user).isAdmin()) {

            ((HttpServletResponse) response).sendRedirect(httpServletRequest.getContextPath()
                    + "/pages/private/StoreOwner/storeOwnerHome.xhtml");

        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
    }
    
}
