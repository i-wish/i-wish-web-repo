package eg.gov.iti.jets.iwish.beans;

import eg.gov.iti.jets.iwish.delegate.StoreOwnerDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.GenericUser;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.StoreOwnerDTO;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

@ManagedBean(name = "storeOwnerLoginBean")
@SessionScoped
public class StoreOwnerLoginBean implements Serializable {


	StoreOwnerDelegate storeOwnerDelegate = DelegateFactory.getStoreOwnerDelegate();


	public GenericUser user;

	@NotNull
	private String email;

	@NotNull
	private String password;

//    @ManagedProperty("#{bundle}")
    private ResourceBundle bundle;


    private String newPassword1;
	private String newPassword2;


    @PostConstruct
    public void init() {
        String language = FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage();
        if (!language.equals("ar")) {
            Locale defaultLocale = Locale.getDefault();
            bundle = ResourceBundle.getBundle("Bundle", defaultLocale);
        } else {
            Locale arBundle = new Locale("ar");
            bundle = ResourceBundle.getBundle("Bundle_ar", arBundle);
        }
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String username) {
		this.email = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public GenericUser getUser() {
		return user;
	}

	public void setUser(StoreOwnerDTO user) {
		this.user = user;
	}

	ArrayList<StoreOwnerDTO> friends = new ArrayList<>();
	ArrayList<ProductDTO> products = new ArrayList<>();

	private boolean editProfile = false;
	private boolean editPassword = false;



	public void setFriends(ArrayList<StoreOwnerDTO> friends) {
		this.friends = friends;
	}

	public ArrayList<StoreOwnerDTO> getFriends() {
		return friends;
	}

	public void fetchProducts(StoreOwnerDTO userr) {
		products = (ArrayList<ProductDTO>) storeOwnerDelegate.getProducts(userr);
	}

	public ArrayList<ProductDTO> getProducts() {
		if (products.size() != 0) {
			ProductDTO d = products.get(0);
			String name = d.getName();
		}
		return products;
	}

	public void setProducts(ArrayList<ProductDTO> products) {
		this.products = products;
	}

	public boolean isEditProfile() {
		return editProfile;
	}

	public void setEditProfile(boolean editProfile) {
		this.editProfile = editProfile;
	}

	public void editProfile() {
		setEditProfile(!editProfile);
	}


	public String getNewPassword1() {
		return newPassword1;
	}

	public void setNewPassword1(String newPassword1) {
		this.newPassword1 = newPassword1;
	}

	public String getNewPassword2() {
		return newPassword2;
	}

	public void setNewPassword2(String newPassword2) {
		this.newPassword2 = newPassword2;
	}

	public boolean isEditPassword() {
		return editPassword;
	}

	public void setEditPassword(boolean editPassword) {
		this.editPassword = editPassword;
	}

	public void showEditPassword() {
		setEditPassword(true);
	}

	public void hideEditPassword() {
		setEditPassword(false);
	}

	public void saveUserProfile() {
		FacesMessage message = new FacesMessage();


		if (editPassword && getNewPassword1() != null && getNewPassword1().equals(getNewPassword2())) {

				user.setPassword(getNewPassword1());
				storeOwnerDelegate.updateUserProfile((StoreOwnerDTO)user);
				setEditProfile(false);
				setEditPassword(false);

				message.setSeverity(FacesMessage.SEVERITY_INFO);
//				message.setSummary("Your Profile updated");
            String bundleMessage = bundle.getString("storeOwnerProfileUpdated");
            message.setSummary(bundleMessage);


		} else {
			if (!editPassword) {

				storeOwnerDelegate.updateUserProfile((StoreOwnerDTO)user);
//				message.setSummary("Your Profile updated");
                String bundleMessage = bundle.getString("storeOwnerProfileUpdated");
                message.setSummary(bundleMessage);
				setEditProfile(false);
				setEditPassword(false);

			} else {
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
//				message.setSummary("Your new password are not equals");
                String bundleMessage = bundle.getString("storeOwnerUpdateProfileData_passwordNotEqual");
                message.setSummary(bundleMessage);
			}
            String bundleMessage = bundle.getString("storeOwnerProfileUpdated");
            message.setSummary(bundleMessage);
		}


		FacesContext.getCurrentInstance().addMessage(null,message);

	}

	public String login(){
		
		StoreOwnerDTO storeOwnerDTO = storeOwnerDelegate.login(email, password);
		
		if(storeOwnerDTO != null){ 

			HttpSession session = ((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true));
			session.setAttribute("user", storeOwnerDTO);

			setUser(storeOwnerDTO);

			return "storeOwnerHome";
		} else {
			FacesMessage errorMessage = new FacesMessage();
			errorMessage.setSeverity(errorMessage.SEVERITY_ERROR);
//			errorMessage.setDetail("The email or password you entered is incorrect.");
            String bundleMessage = bundle.getString("storeOwnerLogin_emailOrPasswordIsIncorrectDetail");
            errorMessage.setSummary(bundleMessage);
//			errorMessage.setSummary("Login Fail");
            bundleMessage = bundle.getString("storeOwnerLoginFailSummary");
            errorMessage.setSummary(bundleMessage);

			FacesContext.getCurrentInstance().addMessage(null, errorMessage);

			return "";
		}
	}

    public void setBundle(ResourceBundle bundle){
        this.bundle=bundle;
    }
    public ResourceBundle getBundle(){
        return  bundle;
    }
}
