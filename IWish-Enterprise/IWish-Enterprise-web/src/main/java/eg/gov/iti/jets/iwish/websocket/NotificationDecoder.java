/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.websocket;

import eg.gov.iti.jets.iwish.dto.NotificationDTO;

import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.spi.JsonProvider;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;
import java.io.IOException;
import java.io.Reader;

/**
 *
 * @author eltntawy
 */
public class NotificationDecoder implements Decoder.TextStream<NotificationDTO> {

    @Override
    public NotificationDTO decode(Reader reader) throws DecodeException, IOException {
        JsonProvider provider = JsonProvider.provider();
        JsonReader jsonReader = provider.createReader(reader);
        JsonObject jsonNotification = jsonReader.readObject();
        NotificationDTO notification = new NotificationDTO();
        notification.setTitle(jsonNotification.getString("title"));
        notification.setDetail(jsonNotification.getString("detail"));
        notification.setUrl(jsonNotification.getString("url"));
        notification.setPicture(jsonNotification.getString("picture").getBytes());
        return notification;

    }

    @Override
    public void init(EndpointConfig config) {

    }

    @Override
    public void destroy() {

    }

}
