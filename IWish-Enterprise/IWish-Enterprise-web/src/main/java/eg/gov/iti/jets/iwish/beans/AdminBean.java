package eg.gov.iti.jets.iwish.beans;

import eg.gov.iti.jets.iwish.delegate.ProductDelegate;
import eg.gov.iti.jets.iwish.delegate.ProductMapsDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.GenericUser;
import org.primefaces.model.chart.PieChartModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by eltntawy on 15/05/15.
 */
@ManagedBean
@ViewScoped
public class AdminBean implements Serializable  {

    ProductDelegate productDelegate = DelegateFactory.getProductDelegateInstance();
    ProductMapsDelegate productMapsDelegate = DelegateFactory.getProductMapsDelegate();
    
    @ManagedProperty("#{user}")
    GenericUser user;

    Long productCount ;



    private PieChartModel productsPieChart;


    @PostConstruct
    public void init() {
        productCount = productDelegate.getProductsCount();
        fillProductsPieChartModel();

    }

    private void fillProductsPieChartModel() {
        productsPieChart =new PieChartModel();
        Map<String,Integer> companyProductsMap = new HashMap<String,Integer>();

        companyProductsMap = productDelegate.getCompanyProductsMap();

        Set<String> keySet = companyProductsMap.keySet();

        for(String key : keySet) {
            productsPieChart.set(key,companyProductsMap.get(key));
        }

        productsPieChart.setShadow(true);
        productsPieChart.setMouseoverHighlight(true);
        productsPieChart.setTitle("Category Products Chart");

        productsPieChart.setFill(true);
        productsPieChart.setShowDataLabels(true);
        productsPieChart.setDiameter(150);
    }

    public GenericUser getUser() {
        return user;
    }

    public void setUser(GenericUser user) {
        this.user = user;
    }

    public Long getProductCount() {
        return productCount;
    }

    public void setProductCount(Long productCount) {
        this.productCount = productCount;
    }


    public PieChartModel getProductsPieChart() {
        return productsPieChart;
    }

    public void setProductsPieChart(PieChartModel productsPieChart) {
        this.productsPieChart = productsPieChart;
    }

    public void syncProduct() {

        new Thread(){
            @Override
            public void run() {
                productDelegate.synchProudct();
            }
        }.start();

    }



    public void refreshProductCount(){
        productCount = productDelegate.getProductsCount();
        productMapsDelegate.fillAllProducts();
        fillProductsPieChartModel();


    }


}
