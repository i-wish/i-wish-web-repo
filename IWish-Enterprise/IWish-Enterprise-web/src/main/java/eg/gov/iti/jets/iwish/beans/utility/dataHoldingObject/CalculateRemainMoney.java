/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.beans.utility.dataHoldingObject;

import eg.gov.iti.jets.iwish.delegate.WishListDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.ProductReserveDTO;
import eg.gov.iti.jets.iwish.dto.UserProductDTO;

import java.util.ArrayList;

/**
 *
 * @author Nova
 */
public class CalculateRemainMoney {

    WishListDelegate wishListDelegate = DelegateFactory.getWishListDelegate();
    ArrayList<ProductReserveDTO> ProductReserverDTOs;
    UserProductDTO userProductDTO;


    public CalculateRemainMoney() {

    }

    public CalculateRemainMoney(ArrayList<ProductReserveDTO> ProductReserverDTOs, UserProductDTO userProductDTO) {
        this.ProductReserverDTOs = ProductReserverDTOs;
        this.userProductDTO = userProductDTO;

    }


    public ArrayList<ProductReserveDTO> getProductReserverDTOs() {
        return ProductReserverDTOs;
    }

    public void setProductReserverDTOs(ArrayList<ProductReserveDTO> ProductReserverDTOs) {
        this.ProductReserverDTOs = ProductReserverDTOs;
    }

    public UserProductDTO getUserProductDTO() {
        return userProductDTO;
    }

    public void setUserProductDTO(UserProductDTO userProductDTO) {
        this.userProductDTO = userProductDTO;
    }

    public double CalcRemain(UserProductDTO userProductDTO) {

        try {
            double remain = 0;
            ProductReserverDTOs = (ArrayList<ProductReserveDTO>) wishListDelegate.getProductReservers(userProductDTO);
            for (ProductReserveDTO Product : ProductReserverDTOs) {
                remain += Product.getParticipate();
            }
            return (100-remain);
        } catch (NullPointerException ex) {
            return 100;
        }
    }
}
