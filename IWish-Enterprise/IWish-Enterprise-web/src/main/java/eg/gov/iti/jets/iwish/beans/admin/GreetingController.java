package eg.gov.iti.jets.iwish.beans.admin;

import eg.gov.iti.jets.iwish.beans.admin.util.JsfUtil;
import eg.gov.iti.jets.iwish.beans.admin.util.JsfUtil.PersistAction;
import eg.gov.iti.jets.iwish.dao.EventTypeDAO;
import eg.gov.iti.jets.iwish.dao.GreetingDAO;
import eg.gov.iti.jets.iwish.dto.GreetingDTO;
import eg.gov.iti.jets.iwish.pojo.EventType;
import eg.gov.iti.jets.iwish.pojo.Greeting;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named("greetingController")
@SessionScoped
public class GreetingController implements Serializable {

    @EJB
    GreetingDAO greetingDAO;

    @EJB
    EventTypeDAO eventTypeDAO;

    private List<GreetingDTO> items = null;
    private GreetingDTO selected;

    public GreetingController() {
    }

    public GreetingDTO getSelected() {
        return selected;
    }

    public void setSelected(GreetingDTO selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }



    public GreetingDTO prepareCreate() {
        selected = new GreetingDTO();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("GreetingCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("GreetingUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("GreetingDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<GreetingDTO> getItems() {
        if (items == null) {
            items = greetingDAO.getAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if(persistAction == PersistAction.UPDATE) {
                    Greeting greeting = greetingDAO.findEntityById(selected.getId());
                    EventType eventType = eventTypeDAO.findEntityById(selected.getEventTypeDTO().getId());
                    greeting.setEventType(eventType);
                    greetingDAO.saveOrUpdate(greeting);
                }else if (persistAction == PersistAction.CREATE)  {
                    Greeting greeting = PojoUtil.getPojo(selected);
                    EventType eventType = eventTypeDAO.findEntityById(selected.getEventTypeDTO().getId());
                    greeting.setEventType(eventType);
                    greetingDAO.persist(greeting);
                } else {
                    greetingDAO.delete(selected.getId());
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public GreetingDTO getGreeting(java.lang.Integer id) {
        return greetingDAO.findById(id);
    }

    public List<GreetingDTO> getItemsAvailableSelectMany() {
        return greetingDAO.getAll();
    }

    public List<GreetingDTO> getItemsAvailableSelectOne() {
        return greetingDAO.getAll();
    }

    @FacesConverter(value = "GreetingControllerConverter")
    public static class GreetingControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            GreetingController controller = (GreetingController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "greetingController");
            return controller.getGreeting(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            try { key = Integer.valueOf(value);} catch (NumberFormatException ex) {key = null; ex.printStackTrace();}
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Greeting) {
                GreetingDTO o = (GreetingDTO) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Greeting.class.getName()});
                return null;
            }
        }

    }

}
