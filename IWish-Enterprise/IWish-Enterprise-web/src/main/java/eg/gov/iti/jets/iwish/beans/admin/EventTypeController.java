package eg.gov.iti.jets.iwish.beans.admin;

import eg.gov.iti.jets.iwish.beans.admin.util.JsfUtil;
import eg.gov.iti.jets.iwish.beans.admin.util.JsfUtil.PersistAction;
import eg.gov.iti.jets.iwish.dao.EventTypeDAO;
import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.pojo.EventType;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named("eventTypeController")
@SessionScoped
public class EventTypeController implements Serializable {

    @EJB
    EventTypeDAO eventTypeDAO;
    private List<EventTypeDTO> items = null;
    private EventTypeDTO selected;

    public EventTypeController() {
    }

    public EventTypeDTO getSelected() {
        return selected;
    }

    public void setSelected(EventTypeDTO selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }



    public EventTypeDTO prepareCreate() {
        selected = new EventTypeDTO();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("EventTypeCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("EventTypeUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("EventTypeDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<EventTypeDTO> getItems() {
        if (items == null) {
            items = eventTypeDAO.getAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if(persistAction == PersistAction.UPDATE) {
                    EventType eventType = eventTypeDAO.findEntityById(selected.getId());
                    eventTypeDAO.saveOrUpdate(eventType);
                }else if (persistAction == PersistAction.CREATE)  {
                    EventType eventType = PojoUtil.getPojo(selected);
                    eventTypeDAO.persist(eventType);
                } else {
                    eventTypeDAO.delete(selected.getId());
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public EventTypeDTO getEventType(java.lang.Integer id) {
        return eventTypeDAO.findById(id);
    }

    public List<EventTypeDTO> getItemsAvailableSelectMany() {
        return eventTypeDAO.getAll();
    }

    public List<EventTypeDTO> getItemsAvailableSelectOne() {
        return eventTypeDAO.getAll();
    }

    @FacesConverter(value = "EventTypeControllerConverter")
    public static class EventTypeControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            EventTypeController controller = (EventTypeController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "eventTypeController");
            return controller.getEventType(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            try { key = Integer.valueOf(value);} catch (NumberFormatException ex) {key = null; ex.printStackTrace();}
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof EventTypeDTO) {
                EventTypeDTO o = (EventTypeDTO) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), EventType.class.getName()});
                return null;
            }
        }

    }

}
