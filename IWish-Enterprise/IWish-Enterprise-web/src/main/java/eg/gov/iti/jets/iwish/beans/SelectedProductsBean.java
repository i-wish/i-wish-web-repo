/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.beans;

import eg.gov.iti.jets.iwish.beans.utility.dataHoldingObject.SelectedProduct;
import eg.gov.iti.jets.iwish.delegate.ProductMapsDelegate;
import eg.gov.iti.jets.iwish.delegate.WishListDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.*;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Hossam Abdallah
 */
@ManagedBean(name = "selectedProducts")
@SessionScoped
public class SelectedProductsBean implements Serializable {

    int id = 4441;

    @ManagedProperty(value = "#{wishList}")
    WishListBean wishListBean;

    public WishListBean getWishListBean() {
        return wishListBean;
    }

    public void setWishListBean(WishListBean wishListBean) {
        this.wishListBean = wishListBean;
    }

    WishListDelegate wishListDelegate = DelegateFactory.getWishListDelegate();

    ProductMapsDelegate productMapsDelegate = DelegateFactory.getProductMapsDelegate();

    ArrayList<SelectedProduct> selectedProducts = new ArrayList<SelectedProduct>();

    ArrayList<CategoryDTO> categories = new ArrayList<CategoryDTO>();
    ArrayList<CompanyDTO> companies = new ArrayList<CompanyDTO>();
    ArrayList<ProductDTO> products = new ArrayList<ProductDTO>();

    CategoryDTO selectedCategory = new CategoryDTO();
    CategoryDTO tempCategory;
    boolean newCategorySelected = false;
    CompanyDTO selectedCompany = new CompanyDTO();

    {
        categories = (ArrayList<CategoryDTO>) productMapsDelegate.getCategories();

        if (categories.size() != 0) {
            companies = (ArrayList<CompanyDTO>) productMapsDelegate.getCompanies(categories.get(0));
            setSelectedCategory(categories.get(0));
            tempCategory = categories.get(0);
        }

        if (companies.size() != 0) {
            products = (ArrayList<ProductDTO>) productMapsDelegate.getProducts(companies.get(0));
            setSelectedCompany(companies.get(0));
        }

    }

    public CategoryDTO getSelectedCategory() {
        return selectedCategory;
    }

    public CompanyDTO getSelectedCompany() {
        return selectedCompany;
    }

    public void setSelectedCategory(CategoryDTO selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public void setSelectedCompany(CompanyDTO selectedCompany) {
        this.selectedCompany = selectedCompany;
    }

    public int getLengthOfSelectedItems() {
        return this.selectedProducts.size();
    }

    public int getPrevId() {
        return id;
    }

    public int getId() {
        int temp = id;
        id = id + 1;
        return temp;
    }

    public void updateCompanies(final CategoryDTO categoryDTO) {
        tempCategory = categoryDTO;
        newCategorySelected = true;
        companies = (ArrayList<CompanyDTO>) productMapsDelegate.getCompanies(categoryDTO);
    }

    public void updateProducts(final CompanyDTO companyDTO) {
        newCategorySelected = false;
        setSelectedCompany(companyDTO);
        setSelectedCategory(tempCategory);
        products = (ArrayList<ProductDTO>) productMapsDelegate.getProducts(companyDTO);
    }

    public void addToSelectedProducts(ProductDTO product) {
        ArrayList<ProductDTO> selectedItems = new ArrayList<ProductDTO>();
        selectedItems.add(product);

        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Object object = session.getAttribute("user");

        if(object instanceof UserDTO) {
            UserDTO user = (UserDTO) object;

            if (!wishListBean.getWishList().contains(product)) {
                wishListDelegate.addProducts(selectedItems, user);
                UserProductDTO userProductDTO = new UserProductDTO();
                userProductDTO.setProductDTO(product);
                userProductDTO.setUserDTO(user);

                wishListBean.getWishList().add(userProductDTO);
                FacesContext.getCurrentInstance().addMessage("wishListMessages", new FacesMessage(FacesMessage.SEVERITY_INFO, "Success!", "Product has been added."));
            } else {
                FacesContext.getCurrentInstance().addMessage("wishListMessages", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Product is already exist."));
            }
//        selectedProducts.add(new SelectedProduct(product, "sItemImg keep"));


        }
    }

    public void saveSelected(UserDTO userDTO) {

        //implementation used just for testing, supposed to be deleted
        ArrayList<ProductDTO> selectedItems = new ArrayList<ProductDTO>();

        for (SelectedProduct p : selectedProducts) {
            if (p.getSelected().contains("keep")) {
                System.out.println(p.getProduct().getName());
                selectedItems.add(p.getProduct());
            }
        }

        wishListDelegate.addProducts(selectedItems, userDTO);
    }

    public void setCategories(ArrayList<CategoryDTO> categories) {
        this.categories = categories;
    }

    public void setCompanies(ArrayList<CompanyDTO> companies) {
        this.companies = companies;
    }

    public void setProducts(ArrayList<ProductDTO> products) {
        this.products = products;
    }

    public void setSelectedProducts(ArrayList<SelectedProduct> selectedProducts) {
        this.selectedProducts = selectedProducts;
    }

    public ArrayList<CategoryDTO> getCategories() {
        return categories;
    }

    public ArrayList<CompanyDTO> getCompanies() {
        companies = (ArrayList<CompanyDTO>) productMapsDelegate.getCompanies(selectedCategory);
        return companies;
    }

    public ArrayList<ProductDTO> getProducts() {
        if (companies.contains(selectedCompany)) {
            products = (ArrayList<ProductDTO>) productMapsDelegate.getProducts(selectedCompany);
        }
        return products;
    }

    public ArrayList<SelectedProduct> getSelectedProducts() {
        return selectedProducts;
    }

    public String getStyleClassForCategory(CategoryDTO categoryDTO) {
        if (selectedCategory.equals(categoryDTO) && tempCategory == null) {
            return "selectedCategory";
        } else if (!selectedCategory.equals(categoryDTO) && tempCategory == null) {
            return "normalCategory";
        } else if (tempCategory.equals(categoryDTO)) {
            return "selectedCategory";
        } else {
            return "normalCategory";
        }
    }

    public String getStyleClassForCompany(CompanyDTO companyDTO) {
        if (selectedCategory != null && selectedCategory.equals(tempCategory)) {
            if (selectedCompany != null && selectedCompany.equals(companyDTO)) {
                return "selectedCategory";
            } else {
                return "normalCategory";
            }
        }
        if (newCategorySelected) {
            return "normalCategory";
        } else if (selectedCompany.equals(companyDTO)) {
            return "selectedCategory";
        } else {
            return "normalCategory";
        }
    }
}
