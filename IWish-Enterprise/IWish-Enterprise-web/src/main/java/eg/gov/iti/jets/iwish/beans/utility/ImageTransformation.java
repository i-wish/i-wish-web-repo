package eg.gov.iti.jets.iwish.beans.utility;

import org.apache.commons.codec.binary.Base64;
import org.primefaces.model.UploadedFile;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Hossam Abdallah on 4/27/2015.
 */
@ManagedBean(name="imageUtil" , eager = true)
@ApplicationScoped
public class ImageTransformation {

    public static String getImageSrc(byte[] data){
    	if(data != null) {
	        String value =new String(Base64.encodeBase64(data));
	        value = value.replace("/IWish-Enterprise-Application-web/","");
	        String tobeAdded = "data:image/jpeg;base64,";
	        value = tobeAdded+value;
	        return value; 
    	} else {
    		return null;
    	}
    }

	public static byte[] transformInputStreamToArrayOfBytes(InputStream is){
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		try {
			for (int readNum; (readNum = is.read(buf)) != -1;) {
				//Writes to this byte array output stream
				bos.write(buf, 0, readNum);
				System.out.println("read " + readNum + " bytes,");
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		byte[] bytes = bos.toByteArray();
		return bytes;
	}

	public String getImageFromFile(UploadedFile file) throws IOException {
		byte[] bytes = transformInputStreamToArrayOfBytes(file.getInputstream());
		return getImageSrc(bytes);
	}
}
