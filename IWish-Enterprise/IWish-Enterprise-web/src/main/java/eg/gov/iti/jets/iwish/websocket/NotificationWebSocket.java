/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.websocket;

import eg.gov.iti.jets.iwish.dto.NotificationDTO;
import eg.gov.iti.jets.iwish.notification.NotificationWebService;
import eg.gov.iti.jets.iwish.observer.NotificationObserver;

import javax.annotation.PostConstruct;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.*;

/**
 *
 * @author eltntawy
 */
@ServerEndpoint(
        value = "/websocket/notifications/{facebookId}/",
        encoders = {NotificationEncoder.class},
        decoders = {NotificationDecoder.class})
public class NotificationWebSocket implements NotificationWebService {

    private static final Map<String, List<NotificationDTO>> notifications = Collections.synchronizedMap(new LinkedHashMap<String, List<NotificationDTO>>());
    private static final Map<String, List<Session>> sessions = Collections.synchronizedMap(new HashMap<String, List<Session>>());

    @OnMessage
    public void onMessage(Session session, NotificationDTO notification, @PathParam("facebookId") String facebookId) {

//        for (Session openSession : sessions) {
//            try {
//                openSession.getBasicRemote().sendObject(notification);
//            } catch (IOException | EncodeException ex) {
//                sessions.remove(openSession);
//            }
//        }
    }

    @OnOpen
    public void onOpen(Session session, @PathParam("facebookId") String facebookId) throws IOException, EncodeException {
        List<Session> userSessions = sessions.get(facebookId);
        if (userSessions != null && userSessions.size() > 0) {

            userSessions.add(session);
            
        } else {

            userSessions = new ArrayList<>();
            userSessions.add(session);
            sessions.put(facebookId, userSessions);

            List<NotificationDTO> notificationList = notifications.get(facebookId);
            if (notificationList != null && notificationList.size() > 0) {
                for (NotificationDTO notification : notificationList) {
                    session.getBasicRemote().sendObject(notification);
                }
            }
        }

    }

    @OnClose
    public void onClose(Session session, @PathParam("facebookId") String facebookId) throws IOException, EncodeException {
        List<Session> userSessions = sessions.get(facebookId);
        if (userSessions != null) {
            userSessions.remove(session);
        }

    }

    @Override
    public  void pushNotification(final String facebookUserId, final NotificationDTO notificationDTO) {
        if(facebookUserId == null) throw new IllegalArgumentException("facebookUserId can not be null");

        new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                List<Session> userSessions = sessions.get(facebookUserId);
                if (userSessions != null) {
                    try {
                        for (Session session : userSessions) {
                            session.getBasicRemote().sendObject(notificationDTO);
                        }
                    } catch (IOException | EncodeException ex) {
                        sessions.remove(facebookUserId);
                    }
                } else {
                    List<NotificationDTO> userNotification = notifications.get(facebookUserId);
                    if(userNotification != null) {
                        userNotification.add(notificationDTO);
                    }
                    notifications.put(facebookUserId, userNotification);

                }
            }
        }.start();

    }

    @Override
    public  void pushNotifications(final String facebookUserId, final List<NotificationDTO> notificationList) {
        if(facebookUserId == null) throw new IllegalArgumentException("facebookUserId can not be null");

        new Thread() {
            @Override
            public void run() {

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                List<Session> userSessions = sessions.get(facebookUserId);
                if (userSessions != null) {
                    try {
                        for (Session session : userSessions) {
                            for (NotificationDTO notify : notificationList) {
                                session.getBasicRemote().sendObject(notify);
                            }
                        }
                    } catch (IOException | EncodeException ex) {
                        sessions.remove(facebookUserId);
                    }
                }else {
                    List<NotificationDTO> userNotification = notifications.get(facebookUserId);
                    if(userNotification != null) {
                        for(NotificationDTO notify : notificationList){
                            userNotification.add(notify);
                        }
                    } else {
                        userNotification = notificationList;
                    }
                    notifications.put(facebookUserId, userNotification);

                }
            }
        }.start();

    }

    @PostConstruct
    public void afterInit() {
        NotificationObserver.registerNotificationWebService(this);
    }
}
