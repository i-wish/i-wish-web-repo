package eg.gov.iti.jets.iwish.beans;

import eg.gov.iti.jets.iwish.delegate.UserDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.GenericUser;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserProductDTO;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Hossam Abdallah on 5/8/2015.
 */
@ManagedBean(name = "userdata")
@SessionScoped
public class UserBean implements Serializable {

    ArrayList<UserDTO> friends = new ArrayList<>();
    ArrayList<UserProductDTO> products = new ArrayList<>();

    private boolean editProfile = false;
    private boolean editPassword = false;


    @ManagedProperty("#{user}")
    GenericUser user;

//    @ManagedProperty("#{bundle}")
    private ResourceBundle bundle;

    private boolean isHomePageSelected = true;
    private boolean isWishListPageSelected = false;
    private boolean isEventsPageSelected = false;
    private boolean isFriendsPageSelected = false;


    UserDelegate userDelegate = DelegateFactory.getUserDelegateInstance();


    private String newPassword1;
    private String newPassword2;

    @PostConstruct
    public void init() {

        String language=FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage();
        if (!language.equals("ar")) {
            Locale defaultLocale = Locale.getDefault();
            bundle= ResourceBundle.getBundle("Bundle", defaultLocale);
        }else{
            Locale arBundle = new Locale("ar");
            bundle = ResourceBundle.getBundle("Bundle_ar", arBundle);
        }

        friends = userDelegate.getFriends((UserDTO)user);
    }


    public void setFriends(ArrayList<UserDTO> friends) {
        this.friends = friends;
    }

    public ArrayList<UserDTO> getFriends() {
        if(friends != null && friends.size() > 0) {
            return friends;
        } else {
          return userDelegate.getFriends((UserDTO) user);
        }
    }

    public void fetchProducts(UserDTO userr) {
        products = (ArrayList<UserProductDTO>) userDelegate.getUserProductsDTOsList(userr);
    }

    public ArrayList<UserProductDTO> getProducts() {
        if (products.size() != 0) {
            UserProductDTO d = products.get(0);
            String name = d.getProductDTO().getName();
        }
        return products;
    }

    public GenericUser getUser() {
        return user;
    }

    public void setUser(GenericUser user) {
        this.user = user;
    }

    public void setProducts(ArrayList<UserProductDTO> products) {
        this.products = products;
    }

    public boolean isEditProfile() {
        return editProfile;
    }

    public void setEditProfile(boolean editProfile) {
        this.editProfile = editProfile;
    }

    public void editProfile() {
        setEditProfile(!editProfile);
    }



    public String getNewPassword1() {
        return newPassword1;
    }

    public void setNewPassword1(String newPassword1) {
        this.newPassword1 = newPassword1;
    }

    public String getNewPassword2() {
        return newPassword2;
    }

    public void setNewPassword2(String newPassword2) {
        this.newPassword2 = newPassword2;
    }

    public boolean isEditPassword() {
        return editPassword;
    }

    public void setEditPassword(boolean editPassword) {
        this.editPassword = editPassword;
    }

    public void showEditPassword() {
        setEditPassword(true);
    }

    public void hideEditPassword() {
        setEditPassword(false);
    }

    public boolean isHomePageSelected() {
        return isHomePageSelected;
    }

    public void setHomePageSelected(boolean isHomePageSelected) {
        this.isHomePageSelected = isHomePageSelected;
    }

    public boolean isWishListPageSelected() {
        return isWishListPageSelected;
    }

    public void setWishListPageSelected(boolean isWishListPageSelected) {
        this.isWishListPageSelected = isWishListPageSelected;
    }

    public boolean isEventsPageSelected() {
        return isEventsPageSelected;
    }

    public void setEventsPageSelected(boolean isEventsPageSelected) {
        this.isEventsPageSelected = isEventsPageSelected;
    }

    public boolean isFriendsPageSelected() {
        return isFriendsPageSelected;
    }

    public String setHomePageSelected() {
        this.isHomePageSelected = true;
        this.isWishListPageSelected =false;
        this.isEventsPageSelected = false;
        this.isFriendsPageSelected = false;

        return "home";
    }



    public String setWishListPageSelected() {
        this.isHomePageSelected = false;
        this.isWishListPageSelected =true;
        this.isEventsPageSelected = false;
        this.isFriendsPageSelected = false;

        return "wishlist";
    }



    public String setEventsPageSelected() {
        this.isHomePageSelected = false;
        this.isWishListPageSelected =false;
        this.isEventsPageSelected = true;
        this.isFriendsPageSelected = false;

        return "events";
    }



    public String setFriendsPageSelected() {
        this.isHomePageSelected = false;
        this.isWishListPageSelected =false;
        this.isEventsPageSelected = false;
        this.isFriendsPageSelected = true;

        return "friends";
    }

    public void saveUserProfile() {
        FacesMessage message = new FacesMessage();


        if (editPassword && getNewPassword1() != null && getNewPassword1().equals(getNewPassword2())) {

                user.setPassword(getNewPassword1());
                userDelegate.updateUserProfile((UserDTO)user);
                setEditProfile(false);
                setEditPassword(false);

                message.setSeverity(FacesMessage.SEVERITY_INFO);
                String bundleMessage = bundle.getString("profileUpdatedMessage");
                message.setSummary(bundleMessage);


            } else {
            if (!editPassword) {
                if(user.getBirthday().getYear()< (new Date().getYear())) {
                    userDelegate.updateUserProfile((UserDTO) user);
//                    message.setSummary("Your Profile updated");
                    String bundleMessage = bundle.getString("profileUpdatedMessage");
                    message.setSummary(bundleMessage);
                    setEditProfile(false);
                    setEditPassword(false);
                }else{
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
//                    message.setSummary("updateProfileData_validDateRequired");
                    String bundleMessage = bundle.getString("updateProfileData_validDateRequired");
                    message.setSummary(bundleMessage);
                }

            } else {
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
//                message.setSummary("Your new password are not equals");
                String bundleMessage = bundle.getString("updateProfileData_passwordNotEqual");
                message.setSummary(bundleMessage);
            }
            }


        FacesContext.getCurrentInstance().addMessage(null,message);

    }
    public void setBundle(ResourceBundle bundle){
        this.bundle=bundle;
    }
    public ResourceBundle getBundle(){
        return  bundle;
    }
}
