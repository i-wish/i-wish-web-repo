/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.beans;

import eg.gov.iti.jets.iwish.delegate.WishListDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserProductDTO;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author radwa
 */

@ManagedBean(name="wishList")
@SessionScoped
public class WishListBean implements Serializable{
    WishListDelegate wishlistDelegate;
    
    private DataModel<UserProductDTO> wishListModel;

    public DataModel<UserProductDTO> getWishListModel() {
        return wishListModel;
    }

    public void setWishListModel(DataModel<UserProductDTO> wishListModel) {
        this.wishListModel = wishListModel;
    }

    private List<UserProductDTO> wishList;


    public List<UserProductDTO> getWishList() {
        return wishList;
    }
    
    public WishListBean(){
        wishlistDelegate=DelegateFactory.getWishListDelegate();
        wishList=wishlistDelegate.getFriendWishListWithDetails(getUser());
        wishListModel=new ListDataModel<UserProductDTO>(getWishList());
    }

    public List<UserProductDTO> refreshWishList(){
        wishList=wishlistDelegate.getFriendWishListWithDetails(getUser());
        return wishList;
    }
    
    public List<ProductDTO> getFriendWishlist(UserDTO userDTO) {
        
        return wishlistDelegate.getFriendWishList(userDTO);
        
    }

    public List<UserProductDTO> getFriendWishlistAsUserProducts(UserDTO userDTO) {

        return wishlistDelegate.getUserProductDTOs(userDTO);

    }
    
    public ProductDTO getRandomProduct(UserDTO friend) {
        
        List<ProductDTO> wishlist = wishlistDelegate.getFriendWishList(friend);
        
        if(wishlist != null && wishlist.size() > 0) {
            int index = (int) (Math.random() * wishlist.size());
            return wishlist.get(index);
        }
        
        return null;
    }
    public UserDTO getUser() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Object object = session.getAttribute("user");

        if(object instanceof UserDTO) {
            UserDTO user = (UserDTO) object;
            if (user != null) {
                return user;
            }
        }
        return null;
    }


   public boolean isDiabled(UserProductDTO userProductDTO){
       if(userProductDTO != null && userProductDTO.getReceived() != null &&  userProductDTO.getReceived() == 1)
           return true;
       else
           return false;
   }

   public void confirmReceivingGift(UserProductDTO productDto){
        ProductDTO product=productDto.getProductDTO();
        wishList.remove(product);
        wishlistDelegate.deleteGift(productDto);
    }

    public void deleteProduct(UserProductDTO product) {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Object object = session.getAttribute("user");

        if(object instanceof UserDTO) {
            UserDTO user = (UserDTO) object;
            ArrayList<ProductDTO> productList = new ArrayList<ProductDTO>();
            productList.add(product.getProductDTO());
            wishlistDelegate.deleteProducts(productList, user);
            wishList.remove(product);
            wishListModel = new ListDataModel<UserProductDTO>(getWishList());
        }
        
    }

}
