package eg.gov.iti.jets.iwish.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.Locale;

/**
 * Created by Niveen on 6/27/2015.
 */
@ManagedBean(name = "langBean")
@SessionScoped
public class Language {

    String location="LTR";
    public void changeLang(String lang) {

        FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale(lang));

    }
    public void setLocation(String location){
        this.location=location;
    }
    public String getLocation(){
        String language=FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage();
        if (language.equals("ar")){
            return "RTL";
        }else
            return "LTR";

    }
}
