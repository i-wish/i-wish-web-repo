package eg.gov.iti.jets.iwish.beans.admin;

import eg.gov.iti.jets.iwish.beans.admin.util.JsfUtil;
import eg.gov.iti.jets.iwish.beans.admin.util.JsfUtil.PersistAction;
import eg.gov.iti.jets.iwish.dao.CategoryDAO;
import eg.gov.iti.jets.iwish.dto.CategoryDTO;
import eg.gov.iti.jets.iwish.pojo.Category;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named("categoryController")
@SessionScoped
public class CategoryController implements Serializable {

    @EJB
    CategoryDAO categoryDAO;

    private List<CategoryDTO> items = null;
    private CategoryDTO selected;

    public CategoryController() {
    }

    public CategoryDTO getSelected() {
        return selected;
    }

    public void setSelected(CategoryDTO selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }



    public CategoryDTO prepareCreate() {
        selected = new CategoryDTO();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("CategoryCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("CategoryUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("CategoryDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<CategoryDTO> getItems() {
        if (items == null) {
            items = categoryDAO.getAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if(persistAction == PersistAction.UPDATE) {
                    categoryDAO.saveOrUpdate(selected);
                }else if (persistAction == PersistAction.CREATE) {
                    Category instance = PojoUtil.getPojo(selected);
                    categoryDAO.persist(instance);
                } else {
                    categoryDAO.delete(selected.getId());
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public CategoryDTO getCategory(java.lang.Integer id) {
        return categoryDAO.findById(id);
    }

    public List<CategoryDTO> getItemsAvailableSelectMany() {
        return categoryDAO.getAll();
    }

    public List<CategoryDTO> getItemsAvailableSelectOne() {
        return categoryDAO.getAll();
    }

    @FacesConverter(value = "CategoryControllerConverter")
    public static class CategoryControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CategoryController controller = (CategoryController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "categoryController");
            return controller.getCategory(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            try { key = Integer.valueOf(value);} catch (NumberFormatException ex) {key = null; ex.printStackTrace();}
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CategoryDTO) {
                CategoryDTO o = (CategoryDTO) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Category.class.getName()});
                return null;
            }
        }

    }

}
