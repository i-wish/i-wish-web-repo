package eg.gov.iti.jets.iwish.beans;

import eg.gov.iti.jets.iwish.beans.utility.ImageTransformation;
import eg.gov.iti.jets.iwish.delegate.ProductDelegate;
import eg.gov.iti.jets.iwish.delegate.ProductMapsDelegate;
import eg.gov.iti.jets.iwish.delegate.StoreOwnerDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.*;
import org.primefaces.context.RequestContext;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by eltntawy on 12/05/15.
 */
@ManagedBean
@SessionScoped
public class StoreOwnerProductBean implements Serializable  {

    final StoreOwnerDelegate storeOwnerDelegate = DelegateFactory.getStoreOwnerDelegate();
    final ProductMapsDelegate productMapsDelegate = DelegateFactory.getProductMapsDelegate();
    final ProductDelegate productDelegate = DelegateFactory.getProductDelegateInstance();

    ArrayList<CompanyDTO> companies = new ArrayList<>();
    ArrayList<CategoryDTO> categories = new ArrayList<>();
    
    ProductDTO productDTO = new ProductDTO();
    CompanyDTO selectedCompany = new CompanyDTO();
    CategoryDTO selectedCategory = new CategoryDTO();

    ProductDTO productToUpdate = new ProductDTO();
    UploadedFile fileToEdit ;

    public void setFileToEdit(UploadedFile fileToEdit) {
        this.fileToEdit = fileToEdit;
    }

    public UploadedFile getFileToEdit() {
        return fileToEdit;
    }

    public ProductDTO getProductToUpdate() {
        return productToUpdate;
    }

    public void setProductToUpdate(ProductDTO productToUpdate) {
        this.productToUpdate = productToUpdate;
    }

    @ManagedProperty("#{user}")
    GenericUser storeOwnerDTO;

    public DataModel<ProductDTO> productDTODataModel;

    private UploadedFile file;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public void upload() {
        if(file != null) {
            FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public StoreOwnerProductBean() {}

    {
        productDTO.setPrice(0);
        categories = (ArrayList<CategoryDTO>) productMapsDelegate.getAllCategories();
        companies = (ArrayList<CompanyDTO>) productMapsDelegate.getAllCompanies();
    }
    
    public ArrayList<CategoryDTO> getCategories() {
		return categories;
	}
    
    public ArrayList<CompanyDTO> getCompanies() {
		return companies;
	}
    
    public ProductDTO getProductDTO() {
		return productDTO;
	}
    
    public CategoryDTO getSelectedCategory() {
		return selectedCategory;
	}
    
    public CompanyDTO getSelectedCompany() {
		return selectedCompany;
	}
    
    public void setCategories(ArrayList<CategoryDTO> categories) {
		this.categories = categories;
	}
    
    public void setCompanies(ArrayList<CompanyDTO> companies) {
		this.companies = companies;
	}
    
    public void setProductDTO(ProductDTO productDTO) {
		this.productDTO = productDTO;
	}
    
    public void setSelectedCategory(CategoryDTO selectedCategory) {
		this.selectedCategory = selectedCategory;
	}
    
    public void setSelectedCompany(CompanyDTO selectedCompany) {
		this.selectedCompany = selectedCompany;
	}


    @PostConstruct
    public void postConstruct() {
        if(storeOwnerDTO != null)
            productDTODataModel = new ListDataModel<ProductDTO>(storeOwnerDelegate.getProducts((StoreOwnerDTO)storeOwnerDTO)) ;
    }

    public DataModel<ProductDTO> getProductDTODataModel() {
        return productDTODataModel;
    }

    public void setProductDTODataModel(DataModel<ProductDTO> productDTODataModel) {
        this.productDTODataModel = productDTODataModel;
    }

    public GenericUser getStoreOwnerDTO() {
        return storeOwnerDTO;
    }

    public void setStoreOwnerDTO(GenericUser storeOwnerDTO) {
        this.storeOwnerDTO = storeOwnerDTO;
    }

    public void editProduct() {
        try {
            this.upload();
            if(fileToEdit != null){
                byte[] bytes = ImageTransformation.transformInputStreamToArrayOfBytes(fileToEdit.getInputstream());
                getProductToUpdate().setImage(bytes);
            }

            productDelegate.updateProduct(getProductToUpdate());

            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Product Edit", "Product has been edited successfully.");
            RequestContext.getCurrentInstance().showMessageInDialog(message);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String deleteProduct() {

        ProductDTO productDTO = productDTODataModel.getRowData();

        storeOwnerDelegate.deleteProduct(productDTO);

        productDTODataModel = new ListDataModel<ProductDTO>(storeOwnerDelegate.getProducts((StoreOwnerDTO)storeOwnerDTO));

        return "";
    }

    public void addProduct() throws IOException {
         this.upload();
        byte[] bytes = ImageTransformation.transformInputStreamToArrayOfBytes(file.getInputstream());

        productDTO.setImage(bytes);
        productDTO.setStoreOwnerDTO((StoreOwnerDTO)storeOwnerDTO);
        storeOwnerDelegate.addProduct(productDTO, selectedCompany, selectedCategory);
        this.postConstruct();

        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Product Added", "Product has been added successfully.");
        RequestContext.getCurrentInstance().showMessageInDialog(message);
    }


}
