package eg.gov.iti.jets.iwish.beans.utility.dataHoldingObject;

import eg.gov.iti.jets.iwish.dto.ProductDTO;

/**
 * Created by Hossam Abdallah on 4/27/2015.
 */
public class SelectedProduct {
    ProductDTO product;
    String selected;

    public SelectedProduct(){}

    public SelectedProduct(ProductDTO product, String selected){
        this.product = product;
        this.selected = selected;
    }

    public String getImageName(){
        if(this.getSelected().equals("sItemImg keep"))
            return "filledCheckBox.png";
        else
            return "emptyCheckBox.png";
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public String getSelected() {
        return selected;
    }
}
