/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.beans;

import eg.gov.iti.jets.iwish.delegate.UserDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.GenericUser;
import eg.gov.iti.jets.iwish.dto.NotificationDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author eltntawy
 */
@ManagedBean
@SessionScoped
public class NotificationBean implements Serializable{
    
    UserDelegate userDelegate = DelegateFactory.getUserDelegateInstance();
    
    
    @ManagedProperty("#{user}")
    GenericUser user;

    public GenericUser getUser() {
        return user;
    }

    public void setUser(GenericUser user) {
        this.user = user;
    }
    
    public List<NotificationDTO> getNotification() {
        return userDelegate.getUnReadedUserNotification((UserDTO)user);
    }
    
    
}
