/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.websocket;

import eg.gov.iti.jets.iwish.beans.utility.ImageTransformation;
import eg.gov.iti.jets.iwish.dto.NotificationDTO;

import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.json.spi.JsonProvider;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import java.io.IOException;
import java.io.Writer;

/**
 *
 * @author eltntawy
 */
public class NotificationEncoder implements Encoder.TextStream<NotificationDTO> {

    @Override
    public void encode(NotificationDTO object, Writer writer) throws EncodeException, IOException {
        
        String img = "";
        if (object != null && object.getPicture() != null) {
            img = ImageTransformation.getImageSrc(object.getPicture());
        }

        JsonProvider provider = JsonProvider.provider();
        JsonObject jsonSticker = provider.createObjectBuilder()
                .add("action", "notification")
                .add("title", object.getTitle() != null ? object.getTitle() : "" )
                .add("detail", object.getDetail() != null ? object.getDetail() : "")
                .add("url", object.getUrl() != null ? object.getUrl() : "")
                .add("picture",img)
                .build();
        try (JsonWriter jsonWriter = provider.createWriter(writer)) {
            jsonWriter.write(jsonSticker);
        }
    }

    @Override
    public void init(EndpointConfig config) {

    }

    @Override
    public void destroy() {

    }

}
