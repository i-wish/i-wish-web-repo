package eg.gov.iti.jets.iwish.beans.admin;

import eg.gov.iti.jets.iwish.beans.admin.util.JsfUtil;
import eg.gov.iti.jets.iwish.beans.admin.util.JsfUtil.PersistAction;
import eg.gov.iti.jets.iwish.dao.IWishConfigDAO;
import eg.gov.iti.jets.iwish.dto.IWishConfigDTO;
import eg.gov.iti.jets.iwish.pojo.IWishConfig;
import eg.gov.iti.jets.iwish.util.PojoUtil;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named("iWishConfigController")
@SessionScoped
public class IWishConfigController implements Serializable {

    @EJB
    private IWishConfigDAO iWishConfigDAO;
    private List<IWishConfigDTO> items = null;
    private IWishConfigDTO selected;

    public IWishConfigController() {
    }

    public IWishConfigDTO getSelected() {
        return selected;
    }

    public void setSelected(IWishConfigDTO selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    

    public IWishConfigDTO prepareCreate() {
        selected = new IWishConfigDTO();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("IWishConfigCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("IWishConfigUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("IWishConfigDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<IWishConfigDTO> getItems() {
        if (items == null) {
            items = iWishConfigDAO.getAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if(persistAction == PersistAction.UPDATE) {

                    iWishConfigDAO.saveOrUpdate(selected);
                }else if (persistAction == PersistAction.CREATE)  {
                    IWishConfig iWishConfig = PojoUtil.getPojo(selected);
                    iWishConfigDAO.persist(iWishConfig);
                } else {
                    iWishConfigDAO.delete(selected.getId());
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public IWishConfigDTO getIWishConfig(java.lang.Integer id) {
        return iWishConfigDAO.findById(id);
    }

    public List<IWishConfigDTO> getItemsAvailableSelectMany() {
        return iWishConfigDAO.getAll();
    }

    public List<IWishConfigDTO> getItemsAvailableSelectOne() {
        return iWishConfigDAO.getAll();
    }

    @FacesConverter("IWishConfigControllerConverter")
    public static class IWishConfigControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            IWishConfigController controller = (IWishConfigController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "iWishConfigController");
            return controller.getIWishConfig(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            try { key = Integer.valueOf(value);} catch (NumberFormatException ex) {key = null; ex.printStackTrace();}
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof IWishConfigDTO) {
                IWishConfigDTO o = (IWishConfigDTO) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), IWishConfig.class.getName()});
                return null;
            }
        }

    }

}
