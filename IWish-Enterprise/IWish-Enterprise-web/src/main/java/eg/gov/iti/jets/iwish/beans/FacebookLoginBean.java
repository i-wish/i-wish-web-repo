/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.beans;

import eg.gov.iti.jets.iwish.delegate.EventDelegate;
import eg.gov.iti.jets.iwish.delegate.FacebookDelegate;
import eg.gov.iti.jets.iwish.delegate.UserDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.Serializable;
import java.util.Locale;

/**
 *
 * @author Nova
 */
@ManagedBean(name = "facebookLoginBean")
@RequestScoped
public class FacebookLoginBean implements Serializable {

    FacebookDelegate facebookDelegate = DelegateFactory.getFacebookDelegateInstance();

    String redirectUrl;


    UserEventDTO userEventDTO;
    UserDelegate userDelegate;
    EventDelegate eventDelegate;


    public FacebookLoginBean() {

        userDelegate = DelegateFactory.getUserDelegateInstance();
        eventDelegate=DelegateFactory.getEventDelegate();


        final ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
        String url = ectx.getRequestScheme()
                + "://" + ectx.getRequestServerName()
                + ":" + ectx.getRequestServerPort()
                + ectx.getRequestContextPath();

        url = url + "/pages/public/loginsuccess.xhtml";

        redirectUrl = url;

        System.out.println(redirectUrl);
        System.out.println(url);

    }

    @ManagedProperty(value = "#{param['code']}")
    private String verificationCode;

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        if (verificationCode != null && !verificationCode.equals(this.verificationCode)) {
            final UserDTO user = facebookDelegate.getFacebookUserData(redirectUrl, verificationCode);

            if (user != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
                session.setAttribute("user", user);

                try {
                    HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//                    response.sendRedirect("../auth/user/home.xhtml");
                    response.sendRedirect("../private/user/home.xhtml");

                    // update user friends from facebook
                    new Thread () {
                        @Override
                        public void run() {
                            facebookDelegate.updateFacebookUserFriend(user);
                        }
                    }.start();

//                    userEventDTO=new UserEventDTO();
//                    Calendar nextBirthday = Calendar.getInstance();
//                    nextBirthday.setTime(user.getBirthday());
//                    nextBirthday.add(Calendar.YEAR,(new Date().getYear()- user.getBirthday().getYear()+1));
//                    userEventDTO.setDate(nextBirthday.getTime());
//                    userEventDTO.setName("Birthday");
//                    userEventDTO.setDescription("It's My Birthday");
//                    userEventDTO.setUserDTO(user);
//                    List<EventTypeDTO> eventTypsDTO=eventDelegate.getEventsTypeByName("Birthday");
//                    for (EventTypeDTO eventTypeDTO:eventTypsDTO){
//                       userEventDTO.setEventTypeDTO(eventTypeDTO);
//                    }
//                    if(!eventDelegate.isEventExist(user,userEventDTO.getEventTypeDTO())) {
//
//                        eventDelegate.addEvent(userEventDTO);
//                    }

                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        this.verificationCode = verificationCode;
    }



    public String facebookLoginDialogUrl() {

        return facebookDelegate.getFacebookDialogUrl(redirectUrl);

    }
    
    public String checkLogin(UserDTO user) {
        if(user == null) {
            return "";
        } else {
            return "home";
        }
    }

//    public void changeLang(String lang) {
//
//        FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale(lang));
//
//    }
}
