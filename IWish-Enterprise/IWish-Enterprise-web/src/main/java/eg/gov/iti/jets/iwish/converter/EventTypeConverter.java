package eg.gov.iti.jets.iwish.converter;

import eg.gov.iti.jets.iwish.delegate.EventDelegate;
import eg.gov.iti.jets.iwish.dto.EventTypeDTO;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.ArrayList;
import java.util.List;

@FacesConverter("eventTypeConverter")
public class EventTypeConverter implements Converter {

    EventDelegate eventDelegate;
    List<EventTypeDTO> eventTypeList = new ArrayList<EventTypeDTO>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        eventTypeList = eventDelegate.getEventsType();
        for (int i = 0; i < eventTypeList.size(); i++) {
            if (eventTypeList.get(i).getName().equals(value)) {
                return eventTypeList.get(i);
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return value.toString();
    }

}
