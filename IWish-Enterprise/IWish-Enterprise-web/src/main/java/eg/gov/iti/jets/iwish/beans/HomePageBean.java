package eg.gov.iti.jets.iwish.beans;

import eg.gov.iti.jets.iwish.delegate.UserDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.GenericUser;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;

//import eg.gov.iti.jets.iwish.dto.NotificationDTO;

/**
 * Created by eltntawy on 15/05/15.
 */
@ManagedBean
@ViewScoped
public class HomePageBean  implements Serializable {

    UserDelegate userDelegate = DelegateFactory.getUserDelegateInstance();

    @ManagedProperty("#{user}")
    GenericUser userDTO;

    @PostConstruct
    public void postConstruct() {

        FacesContext context = FacesContext.getCurrentInstance();

//        List<NotificationDTO> notifications = userDelegate.getUnReadedUserNotification(userDTO);
//
//        for(NotificationDTO noti : notifications) {
//            FacesMessage message = new FacesMessage(noti.getTitle(), noti.getDetail());
//            context.addMessage(null, message);
   //     }
    }
}
