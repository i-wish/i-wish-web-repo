/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.beans;

import eg.gov.iti.jets.iwish.beans.utility.dataHoldingObject.CalculateRemainMoney;
import eg.gov.iti.jets.iwish.delegate.WishListDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.GenericUser;
import eg.gov.iti.jets.iwish.dto.ProductReserveDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserProductDTO;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 *
 * @author Niveen
 */
@ManagedBean(name = "reserveGiftBean")
@ViewScoped
public class ReserveGiftBean {

    WishListDelegate wishListDelegate = DelegateFactory.getWishListDelegate();

    private CalculateRemainMoney calculateRemainMoney = new CalculateRemainMoney();

    private ArrayList<ProductReserveDTO> productReserverDTOs;
    private UserProductDTO userProductDTO;

    private String comment;
    private Double participate=0.0;
    private Double remain=0.0;
    private Double remainPercent=0.0;

    @ManagedProperty("#{user}")
    GenericUser user;

//    @ManagedProperty("#{bundle}")
    private ResourceBundle bundle;

    private UserProductDTO userProductToReserve;

    boolean disabledReserveGiftAlonBtn;
    boolean disabledReserveGiftWithOthersBtn;
    boolean disabledCancelReserveGiftBtn;

    @PostConstruct
    public void init() {
        String language=FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage();
        if (!language.equals("ar")) {
            Locale defaultLocale = Locale.getDefault();
            bundle= ResourceBundle.getBundle("Bundle", defaultLocale);
        }else{
            Locale arBundle = new Locale("ar");
            bundle = ResourceBundle.getBundle("Bundle_ar", arBundle);
        }
        calculateRemainMoney = new CalculateRemainMoney();

        if (userProductToReserve != null) {
            this.remainPercent=calculateRemainMoney.CalcRemain(userProductToReserve);
            this.remain=(userProductToReserve.getProductDTO().getPrice()*remainPercent)/100;
            boolean isReserved = wishListDelegate.isReservedBefore((UserDTO)user, userProductToReserve);
            this.productReserverDTOs = (ArrayList<ProductReserveDTO>) wishListDelegate.getProductReservers(userProductToReserve);
            if(isReserved){
                disabledReserveGiftAlonBtn=true;
                disabledReserveGiftWithOthersBtn=true;
                disabledCancelReserveGiftBtn=false;
            }else{
                 disabledCancelReserveGiftBtn=true;
                if (remainPercent <100) {
                    disabledReserveGiftAlonBtn = true;
                    disabledReserveGiftWithOthersBtn = false;
                } else if (remainPercent == 100) {
                    disabledReserveGiftAlonBtn = false;
                    disabledReserveGiftWithOthersBtn = false;
                } else {
                    disabledReserveGiftAlonBtn = true;
                    disabledReserveGiftWithOthersBtn = true;
                }
            }

        }
    }
    public boolean getDisabledReserveGiftAlonBtn() {
        return disabledReserveGiftAlonBtn;
    }


    public void setDisabledCancelReserveGiftAlonBtn(boolean disabledCancelReserveGiftAlonBtn) {
        this.disabledCancelReserveGiftBtn = disabledCancelReserveGiftBtn;
    }
    public boolean getDisabledCancelReserveGiftBtn() {
        return disabledCancelReserveGiftBtn;
    }


    public void setDisabledReserveGiftAlonBtn(boolean disabledReserveGiftAlonBtn) {
        this.disabledReserveGiftAlonBtn = disabledReserveGiftAlonBtn;
    }
    public boolean getDisabledReserveGiftWithOthersBtn() {
        return disabledReserveGiftWithOthersBtn;
    }

    public void setDisabledReserveGiftWithOthersBtn(boolean disabledReserveGiftWithOthersBtn) {
        this.disabledReserveGiftWithOthersBtn = disabledReserveGiftAlonBtn;
    }
    public UserProductDTO getUserProductToReserve() {
        return userProductToReserve;
    }

    public void setUserProductToReserve(UserProductDTO userProductToReserve) {

        this.userProductToReserve = userProductToReserve;

        this.remainPercent=calculateRemainMoney.CalcRemain(userProductToReserve);
        this.remain=(userProductToReserve.getProductDTO().getPrice()*remainPercent)/100;
        productReserverDTOs=(ArrayList<ProductReserveDTO>) wishListDelegate.getProductReservers(userProductToReserve);
        boolean isReserved = wishListDelegate.isReservedBefore((UserDTO)user, userProductToReserve);
        if(isReserved){
            disabledReserveGiftAlonBtn=true;
            disabledReserveGiftWithOthersBtn=true;
            disabledCancelReserveGiftBtn=false;
        }else{
            disabledCancelReserveGiftBtn=true;
            if (remainPercent <100) {
                disabledReserveGiftAlonBtn = true;
                disabledReserveGiftWithOthersBtn = false;
            } else if (remainPercent == 100) {
                disabledReserveGiftAlonBtn = false;
                disabledReserveGiftWithOthersBtn = false;
            } else {
                disabledReserveGiftAlonBtn = true;
                disabledReserveGiftWithOthersBtn = true;
            }
        }
    }

    public CalculateRemainMoney getCalculateRemainMoney() {
        return calculateRemainMoney;
    }

    public void setCalculateRemainMoney(CalculateRemainMoney calculateRemainMoney) {
        this.calculateRemainMoney = calculateRemainMoney;
    }

    public ArrayList<ProductReserveDTO> getProductReserverDTOs() {
        return productReserverDTOs;
    }

    public void setProductReserverDTOs(ArrayList<ProductReserveDTO> productReserverDTOs) {
        this.productReserverDTOs = productReserverDTOs;
    }

    public UserProductDTO getUserProductDTO() {
        return userProductDTO;
    }

    public void setUserProductDTO(UserProductDTO userProductDTO) {
        this.userProductDTO = userProductDTO;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getParticipate() {
        return participate;
    }

    public void setParticipate(Double participate) {
        this.participate = participate;
    }

    public GenericUser getUser() {
        return user;
    }

    public void setUser(GenericUser user) {
        this.user = user;
    }


    public void reserveGift(UserProductDTO userProductDTO) {
        boolean isReserved=wishListDelegate.isReservedBefore((UserDTO)user, userProductDTO);
        FacesMessage resultMessage = new FacesMessage();
        remainPercent=calculateRemainMoney.CalcRemain(userProductToReserve);
        if(remainPercent==100) {
            if(comment.length()!=0) {
                if (comment.length() > 7 && comment.length() <= 50) {
                    boolean success = wishListDelegate.reserveGift((UserDTO)user, userProductDTO, comment);
                    if (success) {
                        remainPercent=0.0;
                        comment="";
                        remainPercent=calculateRemainMoney.CalcRemain(userProductToReserve);
                        this.remain=(userProductToReserve.getProductDTO().getPrice()*remainPercent)/100;
                        disabledReserveGiftAlonBtn = true;
                        disabledReserveGiftWithOthersBtn = true;
                        disabledCancelReserveGiftBtn = false;
                       this.remainPercent=0.0;
                        String bundleMessage = bundle.getString("reservationSuccess");
                        resultMessage.setSeverity(FacesMessage.SEVERITY_INFO);
//                    resultMessage.setSummary("Info");
                        resultMessage.setSummary(bundleMessage);

                    } else {
                        resultMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
//                    resultMessage.setSummary("Error");
                        String bundleMessage = bundle.getString("reservationFail");
                        resultMessage.setSummary(bundleMessage);
                    }
                } else {
                    resultMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
//                resultMessage.setSummary("Error");

                    String bundleMessage = bundle.getString("friendsValidationMessage_comment");
                    resultMessage.setSummary(bundleMessage);
                }
            }else{
                resultMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
//            resultMessage.setSummary("Error");
                String bundleMessage = bundle.getString("friendsRequiredMessage_comment");
                resultMessage.setSummary(bundleMessage);
            }
        }else{
            resultMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
//            resultMessage.setSummary("Error");
            String bundleMessage = bundle.getString("giftReservedBefore");
            resultMessage.setSummary(bundleMessage);
        }
        FacesContext.getCurrentInstance().addMessage("wishListMessages", resultMessage);
    }

    public void reserveGiftWithOthers(UserProductDTO userProductDTO) {
        FacesMessage resultMessage = new FacesMessage();
        boolean isReserved = wishListDelegate.isReservedBefore((UserDTO)user, userProductDTO);
        if (!isReserved) {
        if(comment.length()!=0) {
            if(comment.length()>=7&&comment.length()<=100) {
                    if(participate!=null && participate > 0 && participate <= remainPercent){
                        boolean success = wishListDelegate.reserveGiftWithOthers((UserDTO)user, userProductDTO, comment, participate);
                        if (success) {
                            comment="";
                            remainPercent=calculateRemainMoney.CalcRemain(userProductToReserve);
                            this.remain=(userProductToReserve.getProductDTO().getPrice()*remainPercent)/100;
                            disabledReserveGiftAlonBtn = true;
                            disabledReserveGiftWithOthersBtn = true;
                            disabledCancelReserveGiftBtn=false;
                            this.remainPercent=0.0;
//                            comment="";
                            String bundleMessage = bundle.getString("reservationSuccess");
                            resultMessage.setSeverity(FacesMessage.SEVERITY_INFO);
//                          resultMessage.setSummary("Info");
                            resultMessage.setSummary(bundleMessage);

                        } else {
                            resultMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
//                          resultMessage.setSummary("Error");
                            String bundleMessage = bundle.getString("reservationFail");
                            resultMessage.setSummary(bundleMessage);
                        }
                    } else {
                        resultMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
//                resultMessage.setSummary("Error");
                        String bundleMessage = bundle.getString("friendsValidationMessage_Participate");
                        resultMessage.setSummary(bundleMessage);
                    }

            }else{
                resultMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
//            resultMessage.setSummary("Error");
                String bundleMessage = bundle.getString("friendsValidationMessage_comment");
                resultMessage.setSummary(bundleMessage);
            }
        }else {
                resultMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
//            resultMessage.setSummary("Error");
            String bundleMessage = bundle.getString("friendsRequiredMessage_comment");
            resultMessage.setSummary(bundleMessage);
        }
        } else {
            resultMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
//            resultMessage.setSummary("Error");
            String bundleMessage = bundle.getString("giftReservedBefore");
            resultMessage.setSummary(bundleMessage);
        }
        FacesContext.getCurrentInstance().addMessage("wishListMessages" ,resultMessage);
    }


    public void setRemain(Double remain){
        this.remain=remain;
    }
    public Double getRemain(){
        return remain;
    }
    public void setRemainPercent(Double remainPercent){
        this.remainPercent=remainPercent;

    }
    public Double getRemainPercent(){
        return remainPercent;
    }

    public void cancelReservation(UserProductDTO userProductDTO){
        FacesMessage resultMessage = new FacesMessage();
        boolean isReserved = wishListDelegate.isReservedBefore((UserDTO)user, userProductDTO);
//        if (isReserved) {
            boolean success = wishListDelegate.deleteReservation((UserDTO)user, userProductDTO);
            if (success) {
//              comment="";
//                this.remainPercent=0.0;
                remainPercent=calculateRemainMoney.CalcRemain(userProductToReserve);
                this.remain=(userProductToReserve.getProductDTO().getPrice()*remainPercent)/100;
                disabledCancelReserveGiftBtn=true;
                resultMessage.setSeverity(FacesMessage.SEVERITY_INFO);
//                resultMessage.setSummary("Info");
                String bundleMessage = bundle.getString("cancelReservationSuccess");
                resultMessage.setSummary(bundleMessage);
                if(remainPercent < 100){
                    disabledReserveGiftAlonBtn=true;
                    disabledReserveGiftWithOthersBtn=false;
                }else{
                    disabledReserveGiftWithOthersBtn=false;
                    disabledReserveGiftAlonBtn=false;
                }
            } else {
                resultMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
//                resultMessage.setSummary("Error");
                String bundleMessage = bundle.getString("cancelReservationFail");
                resultMessage.setSummary(bundleMessage);
            }
//        }else{
//            resultMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
////            resultMessage.setSummary("Error");
//            resultMessage.setSummary("you didn't reserve this gift before :(");
//        }
        FacesContext.getCurrentInstance().addMessage("wishListMessages", resultMessage);

    }

    public void setBundle(ResourceBundle bundle){
        this.bundle=bundle;
    }
    public ResourceBundle getBundle(){
        return  bundle;
    }
}

