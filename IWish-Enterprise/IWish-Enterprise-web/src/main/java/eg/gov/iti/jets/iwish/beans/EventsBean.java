package eg.gov.iti.jets.iwish.beans;

import eg.gov.iti.jets.iwish.delegate.EventDelegate;
import eg.gov.iti.jets.iwish.delegate.NotificationDelegate;
import eg.gov.iti.jets.iwish.delegate.UserDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.PlaceDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

@ManagedBean(name = "eventsBean")
@ViewScoped
public class EventsBean implements Serializable {

	private DataModel<UserEventDTO> events;
	private List<UserEventDTO> eventsList;

	public static int index = -1;

	private ResourceBundle bundle;

	private static UserEventDTO editedUserEvent = new UserEventDTO();
	private EventTypeDTO eventTypeDTOToEdit;
	private PlaceDTO placeDTOToEdit;
	private NotificationDelegate notificationDelegate = DelegateFactory.getNotificationDelegateInstance();

	@PostConstruct
	public void init() {
		String language = FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage();
		if (!language.equals("ar")) {
			Locale defaultLocale = Locale.getDefault();
			bundle = ResourceBundle.getBundle("Bundle", defaultLocale);
		} else {
			Locale arBundle = new Locale("ar");
			bundle = ResourceBundle.getBundle("Bundle_ar", arBundle);
		}
	}

	public PlaceDTO getPlaceDTOToEdit() {
		return placeDTOToEdit;
	}

	public void setPlaceDTOToEdit(PlaceDTO placeDTOToEdit) {
		this.placeDTOToEdit = placeDTOToEdit;
	}

	public void setEventTypeDTOToEdit(EventTypeDTO eventTypeDTOToEdit) {
		this.eventTypeDTOToEdit = eventTypeDTOToEdit;
	}

	public EventTypeDTO getEventTypeDTOToEdit() {
		return eventTypeDTOToEdit;
	}

	public UserEventDTO getEditedUserEvent() {
		return editedUserEvent;
	}

	public void setEditedUserEvent(UserEventDTO editedUserEvent) {
		this.editedUserEvent = editedUserEvent;
	}

	private UserEventDTO selectedUserEventDTO;

	private UserEventDTO userEvent = new UserEventDTO();

	{
		userEvent.setDate(new Date());
	}

	private EventTypeDTO selectedEventType = new EventTypeDTO();
	private PlaceDTO selectedPlace = new PlaceDTO();

	EventDelegate eventDelegate;
	UserDelegate userDelegate;

	ArrayList<EventTypeDTO> eventTypes = new ArrayList<>();
	ArrayList<PlaceDTO> places = new ArrayList<>();

	public EventsBean() {
		eventDelegate = DelegateFactory.getEventDelegate();
		userDelegate = DelegateFactory.getUserDelegateInstance();

		UserDTO user = getUser();

		if (user != null) {
			eventsList = eventDelegate.getEvents(user);
			events = new ListDataModel<UserEventDTO>(getEventsList());
			eventTypes = (ArrayList<EventTypeDTO>) eventDelegate.getEventsType();
			places = (ArrayList<PlaceDTO>) eventDelegate.getPlaces();
		}
	}

	public UserEventDTO getSelectedUserEventDTO() {
		return selectedUserEventDTO;
	}

	public void setSelectedUserEventDTO(UserEventDTO selectedUserEventDTO) {
		this.selectedUserEventDTO = selectedUserEventDTO;
	}

	public void setUserEvent(UserEventDTO userEvent) {
		this.userEvent = userEvent;
	}

	public UserEventDTO getUserEvent() {
		return userEvent;
	}

	public List<UserEventDTO> getEventsList() {
		List<UserEventDTO> list = eventDelegate.getEvents(getUser());
		listOfCurrentUserEvents = (ArrayList<UserEventDTO>) list;
		return list;
	}

	ArrayList<UserEventDTO> listOfCurrentUserEvents;

	public void setListOfCurrentUserEvents(ArrayList<UserEventDTO> listOfCurrentUserEvents) {
		this.listOfCurrentUserEvents = listOfCurrentUserEvents;
	}

	public ArrayList<UserEventDTO> getListOfCurrentUserEvents() {
		return listOfCurrentUserEvents;
	}

	public DataModel<UserEventDTO> getEvents() {
		return events;
	}

	public void setEvents(DataModel<UserEventDTO> eventsModel) {
		this.events = eventsModel;
	}

	public UserDTO getUser() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);

		Object object = session.getAttribute("user");

		if (object instanceof UserDTO) {
			UserDTO user = (UserDTO) object;
			if (user != null) {
				return user;
			}
		}
		return null;
	}

	public void setEventTypes(ArrayList<EventTypeDTO> values) {
		this.eventTypes = values;
	}

	public ArrayList<EventTypeDTO> getEventTypes() {
		return eventTypes;
	}

	public void setPlaces(ArrayList<PlaceDTO> places) {
		this.places = places;
	}

	public ArrayList<PlaceDTO> getPlaces() {
		return places;
	}

	public void setSelectedEventType(EventTypeDTO event) {
		this.selectedEventType = event;
	}

	public EventTypeDTO getSelectedEventType() {
		return selectedEventType;
	}

	public void setSelectedPlace(PlaceDTO place) {
		this.selectedPlace = place;
	}

	public PlaceDTO getSelectedPlace() {
		return selectedPlace;
	}

	public List<UserEventDTO> getFriendsEvent() {
		ArrayList<UserEventDTO> userEventDTOs = (ArrayList<UserEventDTO>) eventDelegate.getFriendEvents(getUser());
		cachedFriendsEvents = userEventDTOs;
		return userEventDTOs;
	}

	private ArrayList<UserEventDTO> cachedFriendsEvents = new ArrayList<>();

	public ArrayList<UserEventDTO> getCachedFriendsEvents() {
		return cachedFriendsEvents;
	}

	public void setCachedFriendsEvents(ArrayList<UserEventDTO> cachedFriendsEvents) {
		this.cachedFriendsEvents = cachedFriendsEvents;
	}

	public void addEvent() {
		FacesMessage message = new FacesMessage();
		if (selectedPlace == null || selectedEventType == null || userEvent.getName().isEmpty() || userEvent.getDescription().isEmpty()) {
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			String bundleMessage = bundle.getString("parameterMissing");
			message.setSummary(bundleMessage);
			FacesContext.getCurrentInstance().addMessage("eventMessage", message);
			return;
		}

		if (userEvent.getDate().after(new Date()) || (userEvent.getDate().equals(new Date()))) {

			userEvent.setUserDTO(getUser());
			userEvent.setEventTypeDTO(selectedEventType);
			userEvent.setPlace(selectedPlace);
			eventsList.add(userEvent);
			events = new ListDataModel<UserEventDTO>(getEventsList());
			eventDelegate.addEvent(userEvent);
			userEvent = new UserEventDTO();
			message.setSeverity(FacesMessage.SEVERITY_INFO);
			String bundleMessage = bundle.getString("eventHasBeenSuccessfullyAdded");
			message.setSummary(bundleMessage);

		} else {

			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			// message.setSummary("Date you entered already Passed");
			String bundleMessage = bundle.getString("DateAlreadyPassedMessage");
			message.setSummary(bundleMessage);
		}
		FacesContext.getCurrentInstance().addMessage("eventMessage", message);
	}

	public void editEventData(UserEventDTO userEventObj) {
		FacesMessage message = new FacesMessage();
		eventTypeDTOToEdit = userEventObj.getEventTypeDTO();
		placeDTOToEdit = userEventObj.getPlace();
		editedUserEvent = userEventObj;
		index = eventsList.indexOf(userEventObj);
	}

	public void updateEvents() {
		FacesMessage message = new FacesMessage();
		// System.out.println(editedUserEvent.getId());
		editedUserEvent.setUserDTO(getUser());
		editedUserEvent.setEventTypeDTO(eventTypeDTOToEdit);
		editedUserEvent.setPlace(placeDTOToEdit);
		eventsList.add(index, editedUserEvent);
		if (editedUserEvent.getDate().after(new Date()) || (editedUserEvent.getDate().equals(new Date()))) {
			eventsList.remove(index);
			eventDelegate.updateEvent(editedUserEvent);
			message.setSeverity(FacesMessage.SEVERITY_INFO);
			// message.setSummary("Event has been updated.");
			String bundleMessage = bundle.getString("eventHasBeenUpdated");
			message.setSummary(bundleMessage);
		} else {
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			// message.setSummary("Date you entered already Passed");
			String bundleMessage = bundle.getString("DateAlreadyPassedMessage");
			message.setSummary(bundleMessage);
		}
		String bundleMessage = bundle.getString("eventHasBeenUpdated");
		message.setSummary(bundleMessage);
		FacesContext.getCurrentInstance().addMessage("updateEventMessage", message);// new
																					// FacesMessage(FacesMessage.SEVERITY_INFO,
																					// "Success!",
																					// "Event
																					// has
																					// been
																					// updated."));
	}

	public void delete(UserEventDTO userEventDTO) {

		eventDelegate.removeEvent(userEventDTO);
		eventsList.remove(userEventDTO);
		events = new ListDataModel<UserEventDTO>(getEventsList());
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		try {
			ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
		} catch (IOException e) {
		}
	}

	public void setBundle(ResourceBundle bundle) {
		this.bundle = bundle;
	}

	public ResourceBundle getBundle() {
		return bundle;
	}
}
