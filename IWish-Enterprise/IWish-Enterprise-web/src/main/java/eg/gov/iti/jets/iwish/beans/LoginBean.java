package eg.gov.iti.jets.iwish.beans;

import eg.gov.iti.jets.iwish.delegate.UserDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.UserDTO;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by eltntawy on 29/05/15.
 */
@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    UserDelegate userDelegate = DelegateFactory.getUserDelegateInstance();

    private String email;
    private String password;

//    @ManagedProperty("#{bundle}")
    private ResourceBundle bundle;

    private boolean loggedIn;

    @PostConstruct
    public void init() {

        String language = FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage();
        if (!language.equals("ar")) {
            Locale defaultLocale = Locale.getDefault();
            bundle = ResourceBundle.getBundle("Bundle", defaultLocale);
        } else {
            Locale arBundle = new Locale("ar");
            bundle = ResourceBundle.getBundle("Bundle_ar", arBundle);
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setIsLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String login() {

        UserDTO user = userDelegate.login(email, password);
        if (user != null) {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
            session.setAttribute("user", user);
            loggedIn = true;
            return "home";
        } else {

            FacesMessage errorMessage = new FacesMessage();
            errorMessage.setSeverity(errorMessage.SEVERITY_ERROR);
//            errorMessage.setDetail("The email or password you entered is incorrect.");
//            String bundleMessage = bundle.getString("login_emailOrPasswordIsIncorrectDetail");
//            errorMessage.setDetail(bundleMessage);
//             bundleMessage = bundle.getString("login_emailOrPasswordIsIncorrectSummary");
//            errorMessage.setSummary(bundleMessage);
//            errorMessage.setSummary("login_emailOrPasswordIsIncorrectSummary");

            FacesContext.getCurrentInstance().addMessage("loginFail", errorMessage);

            return "";
        }

    }

    public String logout() {
        HttpSession session = ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false));
        if(session != null) {
            session.setAttribute("user", null);
            session.invalidate();
        }
        return "logout";
    }
//    public void setBundle(ResourceBundle bundle){
//        this.bundle=bundle;
//    }
//    public ResourceBundle getBundle(){
//        return  bundle;
//    }
}
