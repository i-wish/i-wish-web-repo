/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.ws;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eg.gov.iti.jets.iwish.delegate.SendGreetingDelegate;
import eg.gov.iti.jets.iwish.delegate.SuggestGreetingDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.EventCommentsDTO;
import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.GreetingDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 *
 * @author Radwa Manssour
 */
@Path("/greeting")
public class GreetingWS {

    @GET
    @Path("/suggestion")
    @Produces(MediaType.APPLICATION_JSON)
    public String getSuggestions(@QueryParam("eventTypeId") String eventTypeId) {
        if (eventTypeId != null) {
            try {
                SuggestGreetingDelegate delegate = DelegateFactory.getSuggestGreetingDelegateInstance();
                EventTypeDTO type = new EventTypeDTO();
                type.setId(Integer.parseInt(eventTypeId));
                List<GreetingDTO> greetings = delegate.getGreetings(type);
                Gson json = new Gson();
                return json.toJson(greetings);

            } catch (Exception ex) {
                return null;
            }

        }
        return null;
    }

    @POST
    @Path("/send")
    @Produces(MediaType.APPLICATION_JSON)
    public String sendGreeting(String comment) {
        if (comment != null) {
            try {
                SendGreetingDelegate delegate = DelegateFactory.getSendGreetingDelegateInstance();
                Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
                EventCommentsDTO commentDTO = json.fromJson(comment, EventCommentsDTO.class);
                delegate.addGreetingToEvent(commentDTO.getContent(), commentDTO.getUserEventDTO(), commentDTO.getUser());
                Response res = new Response();
                res.isError = false;
                return json.toJson(res);

            } catch (Exception ex) {
            }

        }
        Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
        Response res = new Response();
        res.isError = true;
        return json.toJson(res);
    }
    
     @GET
    @Path("/getGreetings")
    @Produces(MediaType.APPLICATION_JSON)
    public String getGreetings(@QueryParam("eventId") String eventId) {
        if (eventId != null) {
            try {
               
               SendGreetingDelegate delegate = DelegateFactory.getSendGreetingDelegateInstance();
                UserEventDTO eventDTO = new UserEventDTO();
                eventDTO.setId(Integer.parseInt(eventId));
               List<EventCommentsDTO> comments = delegate.getCommentsOnEvent(eventDTO);
                Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
                return json.toJson(comments);

            } catch (Exception ex) {
                return null;
            }

        }
        return null;
    }

}
