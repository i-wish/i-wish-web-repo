/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.ws;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import eg.gov.iti.jets.iwish.delegate.ProductDelegate;
import eg.gov.iti.jets.iwish.delegate.ProductMapsDelegate;
import eg.gov.iti.jets.iwish.delegate.UserDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.CategoryDTO;
import eg.gov.iti.jets.iwish.dto.CompanyDTO;
import eg.gov.iti.jets.iwish.dto.ProductDTO;

import java.awt.Color;
import java.awt.Image;

import javax.imageio.ImageIO;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Radwa Manssour
 */
@Path("/products")
public class ProductWS {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getProducts() {
        ProductDelegate productDelegate = DelegateFactory
                .getProductDelegateInstance();
        List<ProductDTO> productsList = productDelegate.getAllProducts();
        Type typeOfT = new TypeToken<ArrayList<ProductDTO>>() {
        }.getType();
        Gson json = new Gson();
        return json.toJson(productsList, typeOfT);
    }

    @GET
    @Path("/wishlist")
    @Produces(MediaType.APPLICATION_JSON)
    public String getUserWishList(@QueryParam("email") String email) {
        UserDelegate userDelegate = DelegateFactory.getUserDelegateInstance();
        List<ProductDTO> productDTOs = userDelegate.getUserProductsList(email);
        Gson json = new Gson();
        return json.toJson(productDTOs);
    }

    @GET
    @Path("/categories")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCategories() {
        try {
            System.out.println("enter");
//            HashMap<CategoryDTO, HashMap<CompanyDTO, ArrayList<ProductDTO>>> catHashMap = getHashmap();
//
//            ArrayList<CategoryDTO> categoryDTOs = new ArrayList<>();
//            for (CategoryDTO categoryDTO : catHashMap.keySet()) {
//                categoryDTOs.add(categoryDTO);
//            }
            ProductMapsDelegate productMapsDelegate = DelegateFactory.getProductMapsDelegate();
            ArrayList<CategoryDTO> categoryDTOs = (ArrayList<CategoryDTO>) productMapsDelegate.getAllCategories();
            for (CategoryDTO categoryDTO : categoryDTOs){
                categoryDTO.setImage(null);
            }
            Gson json = new Gson();
            return json.toJson(categoryDTOs);
        } catch (Exception e) {
            e.printStackTrace();
            return "exception";
        }
    }

    @GET
    @Path("/category/image")
    @Produces("application/json")
    @Consumes("*/*")
    public String getCategoryImage(@QueryParam("category_id") int category_id){

        String result;
        try {
            ProductDelegate productDelegate = DelegateFactory.getProductDelegateInstance();
            CategoryDTO categoryDTO = productDelegate.getCategoryById(category_id);
            Gson gson = new Gson();
            result = gson.toJson(categoryDTO);
        }catch (Exception e){
            e.printStackTrace();
            result = "exception";
        }
        return result;
    }

    @GET
    @Path("/companies")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("*/*")
    public String getCompanies(@QueryParam("categoryId") int categoryId) {

        try {
//            HashMap<CategoryDTO, HashMap<CompanyDTO, ArrayList<ProductDTO>>> catHashMap = getHashmap();
//
//            ArrayList<CompanyDTO> companyDTOs = new ArrayList<>();
//            for (CategoryDTO categoryDTO : catHashMap.keySet()) {
//                // get id of selected category
//                if (categoryDTO.getId() == categoryId) {
//                    HashMap<CompanyDTO, ArrayList<ProductDTO>> innerHashMap = catHashMap
//                            .get(categoryDTO);
//                    for (CompanyDTO companyDTO : innerHashMap.keySet()) {
//                        companyDTOs.add(companyDTO);
//                    }
//                }
//            }

            ProductDelegate productDelegate = DelegateFactory.getProductDelegateInstance();
            CategoryDTO categoryDTO = productDelegate.getCategoryById(categoryId);
            ProductMapsDelegate productMapsDelegate = DelegateFactory.getProductMapsDelegate();
            ArrayList<CompanyDTO> companyDTOs =(ArrayList<CompanyDTO>) productMapsDelegate.getCompanies(categoryDTO);
            Gson json = new Gson();
            return json.toJson(companyDTOs);
        } catch (Exception e) {
            e.printStackTrace();
            return "exception";
        }
    }

    @GET
    @Path("/productslist")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("*/*")
    public String getProducts(@QueryParam("categoryId") int categoryId,
            @QueryParam("companyId") int companyId,
            @QueryParam("patchStart") int patchStart,
            @QueryParam("patchSize") int patchSize) {

        try {
//            HashMap<CategoryDTO, HashMap<CompanyDTO, ArrayList<ProductDTO>>> catHashMap = getHashmap();
//
//            ArrayList<ProductDTO> productDTOs = new ArrayList<>();
//            for (CategoryDTO categoryDTO : catHashMap.keySet()) {
//                if (categoryDTO.getId() == categoryId) {
//                    HashMap<CompanyDTO, ArrayList<ProductDTO>> innerHashMap = catHashMap
//                            .get(categoryDTO);
//                    for (CompanyDTO companyDTO : innerHashMap.keySet()) {
//                        if (companyDTO.getId() == companyId) {
//                            productDTOs = innerHashMap.get(companyDTO);
//                        }
//                    }
//                }
//            }

            ProductDelegate productDelegate = DelegateFactory.getProductDelegateInstance();
            CompanyDTO companyDTO = productDelegate.getCompanyById(companyId);
            ProductMapsDelegate productMapsDelegate = DelegateFactory.getProductMapsDelegate();
            ArrayList<ProductDTO> productDTOs =(ArrayList<ProductDTO>) productMapsDelegate.getProducts(companyDTO);

            if (productDTOs != null) {
                for (ProductDTO productDTO : productDTOs) {
                    productDTO.setImage(null);
                }


                Page page = new Page();
                int index = patchStart;
                page.setSize(productDTOs.size());
                ArrayList<ProductDTO> pagedProductDTOs = new ArrayList<>();

                int size;
                if ((patchSize + patchStart) < productDTOs.size()) {
                    size = patchSize;
                } else {
                    size = productDTOs.size() - patchStart;
                }
                for (int i = 0; i < size; i++) {
                    pagedProductDTOs.add(productDTOs.get(index));
                    index++;
                }
                page.setProductDTOs(pagedProductDTOs);
                page.setPatchSize(patchSize);
                page.setPatchStart(patchStart);
                Gson json = new Gson();
                return json.toJson(page);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "exception";
        }
        return null;
    }

    @GET
    @Path("/image")
    @Produces("*/*")
    @Consumes("*/*")
    public String getProductImage(@QueryParam("product_id") int product_id) {

        String result;
        try {
            ProductDTO productDTO2 = new ProductDTO();
            productDTO2.setId(product_id);
            ProductDelegate productDelegate = DelegateFactory.getProductDelegateInstance();
            ProductDTO productDTO = productDelegate.getProductImage(productDTO2);

            Gson gson = new Gson();
            result = gson.toJson(productDTO);
        } catch (Exception e) {
            e.printStackTrace();
            result = "exception";
        }
        return result;
    }

    // get hashmap of products
    public HashMap<CategoryDTO, HashMap<CompanyDTO, ArrayList<ProductDTO>>> getHashmap() {

        ProductDelegate productDelegate = DelegateFactory
                .getProductDelegateInstance();
        List<ProductDTO> productsArray = productDelegate.getAllProducts();

        HashMap<CategoryDTO, HashMap<CompanyDTO, ArrayList<ProductDTO>>> catHashMap = new HashMap<CategoryDTO, HashMap<CompanyDTO, ArrayList<ProductDTO>>>();

        for (int i = 0; i < productsArray.size(); i++) {
            // get category .. to add in hashmap

            CategoryDTO categoryDTO = productsArray.get(i).getCategoryDTO();
            String categoryName = categoryDTO.getName();

            int counter = 0;
            // check if product already exists in hashmap
            for (CategoryDTO category : catHashMap.keySet()) {
                if (!category.getName().equals(categoryName)) {
                    counter++;
                }
            }
            if (catHashMap != null && counter == catHashMap.size()) {

                // category doesn't exist in hashmap .. create a hashmap of
                // companies for this category
                HashMap<CompanyDTO, ArrayList<ProductDTO>> compHashMap = new HashMap<CompanyDTO, ArrayList<ProductDTO>>();

                for (int j = 0; j < productsArray.size(); j++) {
                    // get company

                    CompanyDTO companyDTO = productsArray.get(j)
                            .getCompanyDTO();
                    if (companyDTO != null) {
                        String companyName = companyDTO.getName();

                        int counter2 = 0;
                        // check if company already exists in hashmap
                        for (CompanyDTO company : compHashMap.keySet()) {
                            if (!company.getName().equals(companyName)) {
                                counter2++;
                            }
                        }

                        if (compHashMap != null
                                && counter2 == compHashMap.size()) {

                            // company doesn't exist in hashmap .. create
                            // arraylist
                            // of products for this company
                            ArrayList<ProductDTO> products = new ArrayList<ProductDTO>();
                            for (int k = 0; k < productsArray.size(); k++) {

                                if (productsArray.get(k).getCompanyDTO() != null
                                        && productsArray.get(k)
                                        .getCategoryDTO() != null) {
                                    String productCat = productsArray.get(k)
                                            .getCategoryDTO().getName();
                                    String productCom = productsArray.get(k)
                                            .getCompanyDTO().getName();

                                    if (categoryName.equals(productCat)
                                            && companyName.equals(productCom)) {
                                        if (productsArray.get(k).getImage() != null) {
                                            productsArray.get(k).setImage(scale(productsArray.get(k).getImage(), 100, 100));
                                        }
                                        products.add(productsArray.get(k));
                                    }
                                }

                            }

                            if (products.size() != 0) {
                                compHashMap.put(companyDTO, products);
                            }
                        }
                    }

                }
                catHashMap.put(categoryDTO, compHashMap);
            }
        }
        return catHashMap;
    }

    public byte[] scale(byte[] fileData, int width, int height) {
        ByteArrayInputStream in = new ByteArrayInputStream(fileData);
        try {
            BufferedImage img = ImageIO.read(in);
            if (height == 0) {
                height = (width * img.getHeight()) / img.getWidth();
            }
            if (width == 0) {
                width = (height * img.getWidth()) / img.getHeight();
            }
            Image scaledImage = img.getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING);
            BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0, 0, 0), null);

            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            ImageIO.write(imageBuff, "jpg", buffer);
            in.close();
            buffer.close();
            return buffer.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
