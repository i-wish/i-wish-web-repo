package eg.gov.iti.jets.iwish.ws;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/rest")
public class IWishApplication extends Application {

	@Override
	public Set<Class<?>> getClasses() {

		final Set<Class<?>> classes = new HashSet<>();
		classes.add(ProductWS.class);
		classes.add(LoginWS.class);
		classes.add(deleteFromWishList.class);
                classes.add(EventWS.class);
                classes.add(FriendWS.class);
                classes.add(WishListWS.class);
                classes.add(PlaceWs.class);
                classes.add(GreetingWS.class);
                classes.add(PushNotificationWS.class);

		return classes;
	}
}
