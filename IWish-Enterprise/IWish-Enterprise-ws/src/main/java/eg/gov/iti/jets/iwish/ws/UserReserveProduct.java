/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.ws;

import eg.gov.iti.jets.iwish.dto.UserDTO;

/**
 *
 * @author yomna
 */
public class UserReserveProduct {
    
    UserDTO userDTO;
    Double participate;
    String comment;

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getParticipate() {
        return participate;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public void setParticipate(Double participate) {
        this.participate = participate;
    }
}
