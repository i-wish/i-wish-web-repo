/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.ws;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eg.gov.iti.jets.iwish.delegate.NotificationDelegate;
import eg.gov.iti.jets.iwish.delegate.UserDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.NotificationDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

//import com.google.android.gcm.server.Message;
//import com.google.android.gcm.server.Result;
//import com.google.android.gcm.server.Sender;

/**
 *
 * @author Maram
 */
@Path("/BroadCast")
public class PushNotificationWS {

    //private static String Project_Key = "AIzaSyBtimgL7IRQmLd3zgBdTJevLRhdVyq6fSI";
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String saveRegId(@QueryParam("regId") String regId, @QueryParam("userId") int userId) {

        System.out.println("Registreration ID = " + regId + ".....");
        UserDTO userDTO = new UserDTO();
        userDTO.setId(userId);
        UserDelegate userDelegate = DelegateFactory.getUserDelegateInstance();
        userDelegate.saveRegId(userDTO, regId);
        /* try {
         Properties properties = new Properties();
         Sender sender = new Sender(Project_Key);
         Message message = new Message.Builder()
         .collapseKey("message")
         .timeToLive(3)
         .delayWhileIdle(true)
         .addData("message", "test notification") //you can get this message on client side app
         .build();
         Result result = sender.send(message, regId, 1);
         if (result.getCanonicalRegistrationId() != null) {
         System.out.println(result.toString());

         } else {
         String error = result.getErrorCodeName();
         System.out.println("Broadcast failure: " + error);

         }
         } catch (IOException ex) {
         ex.printStackTrace();
         }*/
        return regId;
    }

    @GET
    @Path("/notifications")
    @Produces("*/*")
    @Consumes("*/*")
    public String getUserNotifications(@QueryParam("user_id") int user_id) {

        String result;
        try {
            UserDTO userDTO = new UserDTO();
            userDTO.setId(user_id);
            NotificationDelegate notificationDelegate = DelegateFactory.getNotificationDelegateInstance();
            ArrayList<NotificationDTO> notificationDTOs = notificationDelegate.getUserNotifications(userDTO);

            Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
            result = gson.toJson(notificationDTOs);
        } catch (Exception e) {
            e.printStackTrace();
            result = "exception";
        }
        return result;
    }

    @GET
    @Path("/received")
    @Consumes("*/*")
    @Produces("*/*")
    public String confirmReceivingNotification(@QueryParam("notification_id") int notification_id) {

        try {
            NotificationDTO notificationDTO = new NotificationDTO();
            notificationDTO.setId(notification_id);

            NotificationDelegate notificationDelegate = DelegateFactory.getNotificationDelegateInstance();
            notificationDelegate.confirmReceivingNotification(notificationDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return "exception";
        }
        return "success";
    }
 
}
