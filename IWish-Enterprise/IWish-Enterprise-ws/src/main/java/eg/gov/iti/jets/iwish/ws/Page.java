package eg.gov.iti.jets.iwish.ws;

import eg.gov.iti.jets.iwish.dto.ProductDTO;

import java.util.ArrayList;

public class Page {

	int patchSize;
	int patchStart;
	int size;
	ArrayList<ProductDTO> productDTOs;

	public void setPatchSize(int patchSize) {
		this.patchSize = patchSize;
	}

	public void setPatchStart(int patchStart) {
		this.patchStart = patchStart;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void setProductDTOs(ArrayList<ProductDTO> productDTOs) {
		this.productDTOs = productDTOs;
	}

	public int getPatchSize() {
		return patchSize;
	}

	public int getPatchStart() {
		return patchStart;
	}

	public int getSize() {
		return size;
	}

	public ArrayList<ProductDTO> getProductDTOs() {
		return productDTOs;
	}

}
