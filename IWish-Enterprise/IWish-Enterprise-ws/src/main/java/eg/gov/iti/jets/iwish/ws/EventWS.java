package eg.gov.iti.jets.iwish.ws;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import eg.gov.iti.jets.iwish.delegate.EventDelegate;
import eg.gov.iti.jets.iwish.delegate.UserDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.PlaceDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;
import eg.gov.iti.jets.iwish.security.AESencrp;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/event")
public class EventWS {

    @Path("/get")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getEvents(@QueryParam("fb_id") String fb_id) {
        String output = null;
        if (fb_id != null) {
            UserDTO user = new UserDTO();
            user.setFacebookUserId(fb_id);
            EventDelegate delegate = DelegateFactory.getEventDelegate();
            List<UserEventDTO> eventList = delegate.getEvents(user);
            
            Calendar c = Calendar.getInstance();
            for (UserEventDTO event : eventList) {
                if (event.getTime() != null) {
                    c.setTime(event.getTime());
                    c.set(Calendar.YEAR, 3915);
                    event.setTime(c.getTime());
                }
            }
            Gson json = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm").create();
            output = json.toJson(eventList);

        }
        return output;
    }

    @Path("/getEvent")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getEvent(@QueryParam("id") String id) {
        String output = null;
        if (id != null) {
            UserEventDTO event = new UserEventDTO();
            event.setId(Integer.parseInt(id));
            EventDelegate delegate = DelegateFactory.getEventDelegate();
            UserEventDTO eventDTO = delegate.getEvent(event);
            
            Calendar c = Calendar.getInstance();
          
                
                if (eventDTO.getTime() != null) {
                    c.setTime(eventDTO.getTime());
                    c.set(Calendar.YEAR, 3915);
                    eventDTO.setTime(c.getTime());
                }
            
//
            Gson json = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm").create();
            output = json.toJson(eventDTO);

        }
        return output;
    }

    @Path("/add")
    @POST
    //@Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public String addEvent(String eventJson) {
        Response res = new Response();
        res.isError = true;

        Gson json = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm")
                .create();
        try {

            if (eventJson != null) {
                EventDelegate delegate = DelegateFactory.getEventDelegate();
                UserDelegate userDelegate = DelegateFactory.getUserDelegateInstance();

                UserEventDTO event = json.fromJson(eventJson, UserEventDTO.class);

                delegate.addEvent(event);
                res.isError = false;

            }
        } catch (Exception ex) {
            res.isError = true;

        }
        return json.toJson(res);
    }

    @Path("/test")
    @GET
    //@Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public String addEventTest(@QueryParam("event") String eventJson) {
        Response res = new Response();
        res.isError = true;

//        Gson json = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
//                .create();
        Gson json = new Gson();
        try {

            if (eventJson != null) {
                EventDelegate delegate = DelegateFactory.getEventDelegate();
                UserDelegate userDelegate = DelegateFactory.getUserDelegateInstance();

                UserEventDTO event = json.fromJson(eventJson, UserEventDTO.class);

                delegate.addEvent(event);
                res.isError = false;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            res.isError = true;

        }
        return json.toJson(res);
    }

    @Path("/edit")
    @POST
    //@Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public String editEvent(String eventJson) {
        Response res = new Response();
        res.isError = true;

        Gson json = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm")
                .create();
        try {

            if (eventJson != null) {
                EventDelegate delegate = DelegateFactory.getEventDelegate();

                UserEventDTO event = json.fromJson(eventJson, UserEventDTO.class);

                delegate.updateEvent(event);
                res.isError = false;

            }
        } catch (Exception ex) {
            res.isError = true;

        }
        return json.toJson(res);
    }

    @Path("/remove")
    @POST

    @Produces(MediaType.APPLICATION_JSON)
    public String removeEvent(String eventList) {

        Gson json = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm")
                .create();
        Response res = new Response();
        res.isError = true;
        try {

            EventDelegate delegate = DelegateFactory.getEventDelegate();

            Type listType = new TypeToken<ArrayList<UserEventDTO>>() {
            }.getType();
            List<UserEventDTO> eventsToDelete = json.fromJson(eventList, listType);
            for (UserEventDTO event : eventsToDelete) {
                delegate.removeEvent(event);
            }
            res.isError = false;
        } catch (Exception ex) {
            ex.printStackTrace();
            res.isError = true;
        }

        return json.toJson(res);
    }

    @Path("/types")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getEventTypes() {

        try {
            EventDelegate delegate = DelegateFactory.getEventDelegate();
            Gson json = new Gson();
            Type listType = new TypeToken<ArrayList<EventTypeDTO>>() {}.getType();
            String out = AESencrp.encrypt(json.toJson(delegate.getEventsType(), listType));
            return out;
        } catch (Exception ex) {
            Logger.getLogger(EventWS.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    @GET
    @Path("/print")
    public void eventJson() {
        UserEventDTO userEventDTO = new UserEventDTO();
        EventTypeDTO eventTypeDTO = new EventTypeDTO("Travel");
        eventTypeDTO.setId(1);
        UserDTO userDTO = new UserDTO();
        userDTO.setId(1);
        userEventDTO.setUserDTO(userDTO);
        userEventDTO.setEventTypeDTO(eventTypeDTO);

        PlaceDTO placeDTO = new PlaceDTO("Dubai", " ", " ", " ", " ", null);
        placeDTO.setId(1);
        userEventDTO.setPlace(placeDTO);
        userEventDTO.setDescription("I will travel :D :D");
        userEventDTO.setName("Travel");

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String dfString = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        try {
            userEventDTO.setTime(df.parse(dfString));
            userEventDTO.setDate(df.parse(dfString));
        } catch (ParseException ex) {
            Logger.getLogger(EventWS.class.getName()).log(Level.SEVERE, null, ex);
        }

//        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
//                .create();
        Gson gson = new Gson();
        System.out.println("event json " + gson.toJson(userEventDTO));
    }

    @GET
    @Path("/friends")
    @Consumes("*/*")
    @Produces("*/*")
    public String getFriendsEvents(@QueryParam("user_email") String user_email) {

        UserDTO userDTO = new UserDTO();
        userDTO.setEmail(user_email);
        try {
            EventDelegate eventDelegate = DelegateFactory.getEventDelegate();
            ArrayList<UserEventDTO> events = eventDelegate.getFriendsEvents(userDTO);
            Calendar c = Calendar.getInstance();
            for (UserEventDTO event : events) {
                
                if (event.getTime() != null) {
                    c.setTime(event.getTime());
                    c.set(Calendar.YEAR, 3915);
                    event.setTime(c.getTime());
                }
            }

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm").create();
            String result = gson.toJson(events);
            System.out.println("Result = " + result);
            return (result);

        } catch (Exception e) {
            e.printStackTrace();
            return "exception";
        }

    }

}