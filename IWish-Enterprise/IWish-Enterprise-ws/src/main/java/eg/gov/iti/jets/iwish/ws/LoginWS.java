/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eg.gov.iti.jets.iwish.ws;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eg.gov.iti.jets.iwish.delegate.FacebookDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.UserDTO;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Radwa Manssour
 */
@Path("/login")
public class LoginWS {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String login(@QueryParam("accessToken") String accessToken ){
           
        if(accessToken != null ){
            final FacebookDelegate FBDelegate = DelegateFactory.getFacebookDelegateInstance();
            
            final UserDTO user = FBDelegate. getFacebookUserData(accessToken);
            
            // update user friends from facebook
            if(user != null) {
                new Thread () {

                    @Override
                    public void run() {
                        FBDelegate.updateFacebookUserFriend(user);
                    }
                }.start();
            }
            
            
            Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
            return json.toJson(user);
        }else{
            Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
            return json.toJson(null);
        }
    }
    
}
