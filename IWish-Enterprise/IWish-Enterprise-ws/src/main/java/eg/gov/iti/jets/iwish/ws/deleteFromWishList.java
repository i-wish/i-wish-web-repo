package eg.gov.iti.jets.iwish.ws;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import eg.gov.iti.jets.iwish.delegate.WishListDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.lang.reflect.Type;
import java.util.ArrayList;


@Path ("/deletwish")
public class deleteFromWishList {
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String deleteWishes(@QueryParam("accessToken")String accessToken,String products){
		
		WishListDelegate delegate = DelegateFactory.getWishListDelegate();
		Gson json= new Gson();
		Type listType = new TypeToken<ArrayList<ProductDTO>>() {}.getType();
		ArrayList<ProductDTO> productsToDelete = json.fromJson(products,listType );
		UserDTO user = new UserDTO();
		user.setFacebookUserAccessToken(accessToken);
		delegate.deleteProducts(productsToDelete, user);

		return json.toJson(new reply("deleted", "true"));
	}
	
	class reply{
		String key;
		String reply;
		public reply(String key,String reply) {
			this.key=key;
			this.reply=reply;
		}
		
	}

}
