/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.ws;

import eg.gov.iti.jets.iwish.dto.AbstractDTO;
import java.util.List;


/**
 *
 * @author Radwa Manssour
 */
public class Response <T extends AbstractDTO>{
    public boolean isError;
    public String message;
    public List<T> list;
    
}
