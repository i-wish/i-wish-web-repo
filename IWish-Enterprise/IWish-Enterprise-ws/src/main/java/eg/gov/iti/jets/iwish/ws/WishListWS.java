package eg.gov.iti.jets.iwish.ws;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import eg.gov.iti.jets.iwish.delegate.WishListDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.ProductReserveDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserProductDTO;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.lang.reflect.Type;
import java.util.ArrayList;

@Path("/wishlist")
public class WishListWS {

    @GET
    @Path("/add")
    @Consumes("*/*")
    @Produces("*/*")
    public String createWishList(@QueryParam("ids") String id, @QueryParam("email") String email) {

        System.out.println("*****************Comming Request***************");
        System.out.println("ids are " + id + " email is " + email);
        Type type = new TypeToken<ArrayList<Integer>>() {
        }.getType();
        Gson gson = new Gson();
        ArrayList<Integer> wishListProducts = gson.fromJson(id, type);
        System.out.println("product name is " + wishListProducts.get(0));

        Response res = new Response <>();
        WishListDelegate wishListDelegate = DelegateFactory.getWishListDelegate();
        if(wishListDelegate.addProducts(wishListProducts, email)){
            res.isError = false; 
        }else{
            res.isError = true;
        }
        return gson.toJson(res);
    }

    @GET
    @Path("/giftreserve")
    @Produces("*/*")
    @Consumes("*/*")
    public String reserveGift(@QueryParam("product_id") int product_id, @QueryParam("user_id") int user_id, @QueryParam("friend_id") int friend_id) {

        System.out.println("********************* Comming Request ***************************");
        System.out.println("gift id is " + product_id + " user id is " + user_id + " friend id " + friend_id);

        try {
            UserDTO userDTO = new UserDTO();
            userDTO.setId(user_id);
            UserDTO friendDTO = new UserDTO();
            friendDTO.setId(friend_id);
            ProductDTO productDTO = new ProductDTO();
            productDTO.setId(product_id);

            WishListDelegate wishListDelegate = DelegateFactory.getWishListDelegate();
            UserProductDTO userProductDTO = wishListDelegate.getUserProductDTO(userDTO, productDTO);

            wishListDelegate.reserveGift(friendDTO, userProductDTO, " ");
            return "Successful";
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }

    @GET
    @Path("/reserve/withothers/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("*/*")
    public String reserveWithOthers(@QueryParam("product_id") int product_id, @QueryParam("user_id") int user_id, @QueryParam("friend_id") int friend_id, @QueryParam("participate") double participate, @QueryParam("comment") String comment) {

        System.out.println("********************* Comming Request ***************************");
        System.out.println("gift id is " + product_id + " user id is " + user_id + " friend id " + friend_id);

        try {
            UserDTO userDTO = new UserDTO();
            userDTO.setId(user_id);
            UserDTO friendDTO = new UserDTO();
            friendDTO.setId(friend_id);
            ProductDTO productDTO = new ProductDTO();
            productDTO.setId(product_id);

            WishListDelegate wishListDelegate = DelegateFactory.getWishListDelegate();
            UserProductDTO userProductDTO = wishListDelegate.getUserProductDTO(userDTO, productDTO);

            wishListDelegate.reserveGiftWithOthers(friendDTO, userProductDTO, comment, participate);
            return "Successful";
        } catch (Exception e) {
            e.printStackTrace();
            return "exception";
        }
    }

    @Path("/delete")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String removeEvent(String wishList) {

        Gson json = new Gson();
        Response res = new Response();
        res.isError = true;
        try {
            WishListDelegate delegate = DelegateFactory.getWishListDelegate();
            requestJson req = json.fromJson(wishList, requestJson.class);
            delegate.deleteProducts(req.products, req.user);

            res.isError = false;
        } catch (Exception ex) {
            res.isError = true;
        }

        return json.toJson(res);
    }

    @GET
    @Path("/reserving/friends")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("*/*")
    public String getFriendsReservedProduct(@QueryParam("friend_id") int friend_id, @QueryParam("product_id") int product_id) {
        System.out.println("********************************Comming Request********************************");

        try {
            UserDTO userDTO = new UserDTO();
            userDTO.setId(friend_id);
            ProductDTO productDTO = new ProductDTO();
            productDTO.setId(product_id);

            WishListDelegate wishListDelegate = DelegateFactory.getWishListDelegate();
            UserProductDTO userProductDTO = wishListDelegate.getUserProductDTO(userDTO, productDTO);

            ArrayList<ProductReserveDTO> productReserveDTOs = wishListDelegate.getProductReservers(userProductDTO);
            ArrayList<UserReserveProduct> userReserveProducts = new ArrayList<>();

            for (ProductReserveDTO productReserveDTO : productReserveDTOs) {
                UserReserveProduct userReserveProduct = new UserReserveProduct();
                userReserveProduct.setParticipate(productReserveDTO.getParticipate());

                UserDTO friendDTO = wishListDelegate.getUserReservedProduct(productReserveDTO);
                userReserveProduct.setUserDTO(friendDTO);
                userReserveProduct.setComment(productReserveDTO.getComment());
                userReserveProducts.add(userReserveProduct);
            }

            Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
            String gson_str = gson.toJson(userReserveProducts);
            return gson_str;
        } catch (Exception e) {
            e.printStackTrace();
            return "exception";
        }
    }

    @GET
    @Path("/participate")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("*/*")
    public String getParticipateForProductsForFriend(@QueryParam("friend_id") int friend_id) {

        try {
            WishListDelegate wishListDelegate = DelegateFactory.getWishListDelegate();
            UserDTO userDTO = new UserDTO();
            userDTO.setId(friend_id);
            ArrayList<UserProductDTO> userProductDTOs = wishListDelegate.getUserProductDTOs(userDTO);

            ArrayList<FriendReservedProduct> friendReservedProducts = new ArrayList<>();

            for (UserProductDTO userProductDTO : userProductDTOs) {

                FriendReservedProduct friendReservedProduct = new FriendReservedProduct();
                ArrayList<ProductReserveDTO> productReserveDTOs = wishListDelegate.getProductReservers(userProductDTO);

                ArrayList<UserDTO> userDTOs = new ArrayList<>();
                double participatePercent = 0;
                for (ProductReserveDTO productReserveDTO : productReserveDTOs) {
                    participatePercent = participatePercent + productReserveDTO.getParticipate();
                    userDTOs.add(productReserveDTO.getUserDTO());
                }

                friendReservedProduct.setParticipatePercent(participatePercent);
                friendReservedProduct.setUserDTOs(userDTOs);
                // ProductDTO productDTO = wishListDelegate.getWishlistProduct(userProductDTO);
                friendReservedProduct.setProduct_id(userProductDTO.getProductDTO().getId());
                friendReservedProducts.add(friendReservedProduct);
            }

            Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
            String gson_str = gson.toJson(friendReservedProducts);
            return gson_str;
        } catch (Exception e) {
            e.printStackTrace();
            return "exception";
        }
    }

    @GET
    @Path("/confirm")
    @Produces("*/*")
    @Consumes("*/*")
    public String confirmGiftReceived(@QueryParam("user_id") int user_id, @QueryParam("product_id") int product_id) {

        try {
            UserDTO userDTO = new UserDTO();
            userDTO.setId(user_id);
            ProductDTO productDTO = new ProductDTO();
            productDTO.setId(product_id);
            WishListDelegate wishListDelegate = DelegateFactory.getWishListDelegate();
            UserProductDTO userProductDTO = wishListDelegate.getUserProductDTO(userDTO, productDTO);
            wishListDelegate.deleteGift(userProductDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return "exception";
        }
        return "success";
    }

    @GET
    @Path("/cancel")
    @Consumes("*/*")
    @Produces("*/*")
    public String cancelReserve(@QueryParam("user_id") int user_id, @QueryParam("product_id") int product_id, @QueryParam("friend_id") int friend_id) {

        try {
            UserDTO friend = new UserDTO();
            friend.setId(friend_id);
            UserDTO userDTO = new UserDTO();
            userDTO.setId(user_id);
            ProductDTO productDTO = new ProductDTO();
            productDTO.setId(product_id);
            WishListDelegate wishListDelegate = DelegateFactory.getWishListDelegate();
            UserProductDTO userProductDTO = wishListDelegate.getUserProductDTO(userDTO, productDTO);

            if(wishListDelegate.deleteReservation(friend, userProductDTO)){
                return "success";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "exception";
        }
        return "failed";
    }

    class requestJson {

        UserDTO user;
        ArrayList<ProductDTO> products = new ArrayList<>();
    }

}
