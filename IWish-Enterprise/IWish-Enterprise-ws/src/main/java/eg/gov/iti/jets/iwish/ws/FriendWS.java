/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.ws;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eg.gov.iti.jets.iwish.delegate.UserDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.UserDTO;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

/**
 *
 * @author yomna
 */
@Path("/friend")
public class FriendWS {

    @GET
    @Path("/getfriends")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("*/*")
    public String getFriends(@QueryParam("email") String email) {

        UserDelegate userDelegate = DelegateFactory.getUserDelegateInstance();
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail(email);
        ArrayList<UserDTO> friends = userDelegate.getFriends(userDTO);
        Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
        String friendsJson = gson.toJson(friends);
        System.out.println("friends json " + friendsJson);
        return friendsJson;
    }

//    @GET
//    @Path("/reserving")
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes("*/*")
//    public String getReservingFriends(@QueryParam("friend_id") int friend_id) {
//
//        WishListDelegate wishListDelegate = DelegateFactory.getWishListDelegate();
//        UserDTO userDTO = new UserDTO();
//        userDTO.setId(friend_id);
//        ArrayList<UserProductDTO> userProductDTOs = wishListDelegate.getUserProductDTOs(userDTO);
//
//        ArrayList<FriendReservedProduct> friendReservedProducts = new ArrayList<>();
//
//        for (UserProductDTO userProductDTO : userProductDTOs) {
//
//            FriendReservedProduct friendReservedProduct = new FriendReservedProduct();
//            ArrayList<UserDTO> userDTOs = wishListDelegate.getFriendsReserved(userProductDTO);
//            
//            if (userDTOs.size() != 0) {
//                friendReservedProduct.setUserDTOs(userDTOs);
//                ProductDTO productDTO = wishListDelegate.getWishlistProduct(userProductDTO);
//                friendReservedProduct.setProduct_id(productDTO.getId());
//                friendReservedProducts.add(friendReservedProduct);
//            }
//        }
//
//        Gson gson = new Gson();
//        return gson.toJson(friendReservedProducts);
//    }

    
}
