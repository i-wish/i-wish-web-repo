package eg.gov.iti.jets.iwish.ws;

import com.google.gson.Gson;
import eg.gov.iti.jets.iwish.delegate.PlaceDelegate;
import eg.gov.iti.jets.iwish.delegate.factory.DelegateFactory;
import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.PlaceDTO;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/place")
public class PlaceWs {

    @Path("/get")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getEvents(@QueryParam("eventTypeId") String eventTypeId) {
        String output = null;
        if (eventTypeId != null) {
            try {
                int id = Integer.parseInt(eventTypeId);
                EventTypeDTO eventType = new EventTypeDTO();
                eventType.setId(id);
                PlaceDelegate delegate = DelegateFactory.getPlaceDelegate();
                List<PlaceDTO> placeList = delegate.getPlaces(eventType);
                Gson json = new Gson();
                return json.toJson(placeList);
            } catch (Exception ex) {
                return null;
            }

        } else {
            return null;
        }

    }

}
