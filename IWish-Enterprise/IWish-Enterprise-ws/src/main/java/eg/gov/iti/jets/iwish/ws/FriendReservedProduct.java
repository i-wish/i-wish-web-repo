/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.gov.iti.jets.iwish.ws;

import eg.gov.iti.jets.iwish.dto.UserDTO;

import java.util.ArrayList;

/**
 *
 * @author yomna
 */
public class FriendReservedProduct {
    
    int product_id;
    double participatePercent;
    ArrayList<UserDTO> userDTOs;

    public double getParticipatePercent() {
        return participatePercent;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public void setParticipatePercent(double participatePercent) {
        this.participatePercent = participatePercent;
    }

    public ArrayList<UserDTO> getUserDTOs() {
        return userDTOs;
    }

    public void setUserDTOs(ArrayList<UserDTO> userDTOs) {
        this.userDTOs = userDTOs;
    }
    
}
