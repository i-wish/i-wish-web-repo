# i-wish
It’s a web and mobile application that is designed to allow every user to make his own wishlist from a wide variety of elements in the system beside making his own events.
It allows friends of that user to view his wishlist and events, so they could reserve a gift for him from wishlist, send greeting based on an event or reserve suggested gift from the system based on an event.
